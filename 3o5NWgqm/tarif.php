<?php 
  	include 'header.php';

  	if (isset($_POST['add_tarif'])) {
  		add_tarif($_POST['name'], $_POST['srok'], $_POST['price']);
  	}

    if (isset($_POST['edit_tarif'])) {
      edit_tarif($_POST['name'], $_POST['srok'], $_POST['price'], $_POST['id']);
    }
    if (isset($_POST['del_tarif'])) {
      del("tarif", $_POST['id']);
    }

    

  	print_msg($msg);
?>

	<?php if ($_SESSION['role'] == 1): ?>
    <div class="page-header">
      <h2>Добавить тариф</h2>
    </div>
		<form action="" method="post">
			<div class="form-group">
        <label>Название</label>
        <input type="text" name="name" class="form-control" autocomplete="off" required>
      </div>
			<div class="form-group">
        <label>Длительность тарифа (дней)</label>
        <input type="number" name="srok" class="form-control" autocomplete="off" required>
      </div>
      <div class="form-group">
        <label>Цена</label>
        <input type="number" name="price" class="form-control" autocomplete="off" required>
      </div>
      <div class="form-group">
      	<input type="submit" class="btn btn-success" name="add_tarif" value="Добавить тариф">
      </div>
		</form>

    <div class="page-header">
      <h2>Список тарифов</h2>
    </div>
    <?php
      $tarifs = mysql_query("SELECT * FROM `tarif` ORDER BY `id`");

    while($tarifs_row = mysql_fetch_array($tarifs, MYSQL_ASSOC)){  
    ?>

    <div class="panel panel-primary">
      <div class="panel-heading">
        <h3 class="panel-title">
            <b><?php echo $tarifs_row['name']; ?></b> | <?php echo $tarifs_row['srok']; ?> дн. | <?php echo $tarifs_row['price']; ?> руб.
          </h3>
          <a  href="#editTarif<?php echo $tarifs_row['id']; ?>" class="edit_btn" data-toggle="modal">Редактировать</a>
      </div>
    </div>

    <div id="editTarif<?php echo $tarifs_row['id']; ?>" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
            <!-- Заголовок модального окна -->
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
              <h4 class="modal-title">Редактировать тариф</h4>
            </div>
            <!-- Основное содержимое модального окна -->
            <form action="" method="post">
                <div class="modal-body">
                  <fieldset>
                    
                    <div class="form-group">
                      <label>Название</label>
                      <input type="text" name="name" class="form-control" value="<?php echo $tarifs_row['name']; ?>" autocomplete="off" required>
                    </div>
                    <div class="form-group">
                      <label>Длительность тарифа (дней)</label>
                      <input type="number" name="srok" class="form-control" value="<?php echo $tarifs_row['srok']; ?>" autocomplete="off" required>
                    </div>
                    <div class="form-group">
                      <label>Цена</label>
                      <input type="number" name="price" class="form-control" value="<?php echo $tarifs_row['price']; ?>" autocomplete="off" required>
                    </div>

                  </fieldset>
                </div>
                <!-- Футер модального окна -->
                <div class="modal-footer">
                    <input type="hidden" name="id" value="<?php echo $tarifs_row['id']; ?>" required>
                    <input type="submit" class="btn btn-sucdefaultcess" name="del_tarif" value="Удалить">
                    <input type="submit" class="btn btn-success" name="edit_tarif" value="Сохранить">
                </div>
            </form>
        </div>
      </div>
    </div>

    <?php } ?>
	<?php endif ?>

<?php 
  include 'footer.php';
?>