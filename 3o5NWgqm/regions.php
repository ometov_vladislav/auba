<?php 
    include 'header.php';

    if ($_SESSION['role'] <> 1){
        header("Location: /admin/");
        exit;
    }

    if (isset($_POST['add_city'])) {
        add_city($_POST['name']);
    }
    if (isset($_POST['add_locality'])) {
        add_locality($_POST['city_id'], $_POST['type'], $_POST['name']);
    }

    if (isset($_POST['edit_locality'])) {
        edit_locality($_POST['id'], $_POST['type'], $_POST['name']);
    }

    if (isset($_POST['edit_city'])) {
        edit_city($_POST['id'], $_POST['name']);
    }
    
    print_msg($msg);
?>
    <?php
    $city_query  = "SELECT * FROM `city` ORDER BY `id`";
    $city = mysql_query($city_query);

    while($city_row = mysql_fetch_array($city, MYSQL_ASSOC)){    
        $id = stripslashes($city_row['id']);   
    ?>

	<div class="col-xs-12">
		<div class="panel panel-primary">
	      <div class="panel-heading">
	        <h3 class="panel-title">
                <?php echo $city_row['name']; ?>
            </h3>
            <a  href="#editCity<?php echo $city_row['id']; ?>" class="edit_btn" data-toggle="modal">Изменить </a><!-- href="edit.php?cut=reviews&id=<?php echo $city_row['id']; ?>" -->
            <a href="regions.php?del=city&del_id=<?php echo $city_row['id']; ?>" class="del_btn" onClick="return window.confirm('Подтверждаете удаление города?');">[x]</a> 
	      </div>
	      <div class="panel-body">
	        <div id="list" class="list">
                <ul>
                    <?php
                    $locality_query  = "SELECT * FROM `locality` WHERE `city_id` = $id  ORDER BY `type`";
                    $locality = mysql_query($locality_query);

                    while($locality_row = mysql_fetch_array($locality, MYSQL_ASSOC)){    
                        $id = stripslashes($locality_row['id']);   
                    ?>
                    <li <?php if ($locality_row['type'] == 2) {echo "style='background: #fff0b7;'";} elseif ($locality_row['type'] == 3) {echo "style='background: #d2f2b6;'";} ?>>
                        <div class="row">
                                <div class="col-xs-12 col-sm-9 col-md-10"><span><?php echo $locality_row['name']; ?></span></div>
                                <div class="col-xs-12 col-sm-3 col-md-2">
                                    <a href="#editLocality<?php echo $locality_row['id']; ?>" class="edit_btn" data-toggle="modal">Изменить </a>
                                    <a href="regions.php?del=locality&del_id=<?php echo $locality_row['id']; ?>" class="del_btn" onClick="return window.confirm('Подтверждаете удаление района?');"><b>[x]</b></a>
                                </div>
                            </div>
                    </li>

                    <div id="editLocality<?php echo $locality_row['id']; ?>" class="modal fade">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <!-- Заголовок модального окна -->
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title">Редактировать район <?php echo $locality_row['name']; ?></h4>
                          </div>
                          <!-- Основное содержимое модального окна -->
                            <form action="" method="post">
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label>Тип:</label>
                                        <select id="selest_district" name="type" class="form-control" required="">
                                            <option value="1" <?php if($locality_row['type'] == 1){echo "selected";} ?>>Район города (микрорайон города)</option>
                                            <option value="2" style="background: #fff0b7;" <?php if($locality_row['type'] == 2){echo "selected";} ?>>Пригород</option>
                                            <option value="3" style="background: #d2f2b6;" <?php if($locality_row['type'] == 3){echo "selected";} ?>>Область</option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>Название района:</label>
                                        <input type="text" name="name" value="<?php echo $locality_row['name']; ?>" class="form-control" required="">
                                    </div>

                                    <input type="hidden" name="id" value="<?php echo $locality_row['id']; ?>" class="form-control" required="">
                                </div>
                                <!-- Футер модального окна -->
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                                    <input type="submit" class="btn btn-success" name="edit_locality" value="Сохранить изменения">
                                </div>
                            </form>
                        </div>
                      </div>
                    </div>
                    <?php } ?>
                </ul>
            </div>
	      </div>
	    </div>
	</div>

    <div id="editCity<?php echo $city_row['id']; ?>" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
          <!-- Заголовок модального окна -->
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title">Редактировать город <?php echo $city_row['name']; ?></h4>
          </div>
          <!-- Основное содержимое модального окна -->
          <form action="" method="post">
              <div class="modal-body">
                    <div class="form-group">
                        <label>Название города:</label>
                        <input type="text" name="name" value="<?php echo $city_row['name']; ?>" class="form-control" required="">
                    </div>

                    <input type="hidden" name="id" value="<?php echo $city_row['id']; ?>" class="form-control" required="">
              </div>
              <!-- Футер модального окна -->
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                <input type="submit" class="btn btn-success" name="edit_city" value="Сохранить изменения">
              </div>
          </form>
        </div>
      </div>
    </div>
    <?php } ?>
<?php 
  include 'footer.php';
?>