<?php 
  include 'header.php';

  if ($_SESSION['name'] <> "") {
  	header("Location: ".$admin_dir);
  }
?>
	
	<div class="col-xs-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title">Авторизация в панели управления</h3>
			</div>
			<form method="post">
				<div class="panel-body">
					<div class="col-xs-12 login-box">
						<div class="input-group">
							<span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
							<input type="text" name="login" class="form-control" placeholder="Имя пользователя" required autofocus />
						</div>
						<div class="input-group">
							<span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
							<input type="password" name="pass" class="form-control" placeholder="Ваш пароль" required />
						</div>
					</div>
				</div>
				<div class="panel-footer">
					<div class="row">
						<div class="col-xs-12 col-sm-6">
							<p><a href="#">Забыли свой пароль?</a></p>
						</div>
						<div class="col-xs-12 col-sm-6">
							<div class="text-right">
								<input type="submit" name="login_btn" value="Войти" class="btn btn-labeled btn-success">
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>

<?php 
  include 'footer.php';
?>