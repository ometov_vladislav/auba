<?php 
    function upload_file($file) {
        $uploaddir = $_SERVER['DOCUMENT_ROOT'].'/img/upload_dir/';
        $filename = $file['name'];
        $ext = substr(strrchr($filename, '.'), 1);
        $new_name = md5(date("F j, Y, h:m s").''.$filename).".".$ext; 
        $filename = $new_name; 
        $uploadfile = $uploaddir . basename($filename);
        move_uploaded_file($file['tmp_name'], $uploadfile);
        return $filename;
    }

    function login($login, $pass) {
        $qr_result = mysql_query("select * from `users` WHERE `login`='$login'") or die(mysql_error());
        $result = mysql_fetch_array($qr_result);

        if ($result['login'] <>'') {
            if ($result['pass'] == $pass) {
                $_SESSION['name'] = $result['login'];
                $_SESSION['role'] = $result['role'];
                $_SESSION['city'] = $result['city'];
                $result = 1;
            }else{
                $result = 2;
            }
        }else{
            $result = 2;
        }
        return $result;
    }

    function logout()
    {
        unset($_SESSION['name']);
        header("Location: ".$GLOBALS['admin_dir']);
    }

    function print_msg($msg){
        if (isset($msg['good'])) {
            printf("<div class='col-xs-12'><div class='alert alert-success' role='alert'>
            <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                <span aria-hidden='true'>&times;</span>
            </button>
           ".$msg['good']."
        </div></div>");
        }
        if (isset($msg['bad'])) {
            printf("<div class='col-xs-12'><div class='alert alert-danger' role='alert'>
            <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                <span aria-hidden='true'>&times;</span>
            </button>
           ".$msg['bad']."
        </div></div>");
        }
    }

    function replace_gap($str) {
        $str = preg_replace("/(\[{1}[^\[\]]+\]{1})/", '', $str); //убираем символы
        $str = preg_replace("/[^0-9A-Za-z]/", '', $str); //оставляем толькоцифры и буквы
        return strtolower($str); //переводим текст в нижний регистр
    }

    function translit($str) {
        $rus = array('А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я', 'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я');
        $lat = array('A', 'B', 'V', 'G', 'D', 'E', 'E', 'Gh', 'Z', 'I', 'Y', 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'F', 'H', 'C', 'Ch', 'Sh', 'Sch', 'Y', 'Y', 'Y', 'E', 'Yu', 'Ya', 'a', 'b', 'v', 'g', 'd', 'e', 'e', 'gh', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h', 'c', 'ch', 'sh', 'sch', 'y', 'y', 'y', 'e', 'yu', 'ya');
        return str_replace($rus, $lat, $str);
    }

    function del($table, $id)
    {
        $result = mysql_query ("DELETE FROM $table WHERE id = $id");
        if ($result) 
            $GLOBALS['msg']['good'] = "Удалено!";
        else
            $GLOBALS['msg']['bad'] =  "Ошибка удаления!";
    }

    function select_sections(){
        $sections_query  = "SELECT * FROM `sections` ORDER BY `id`";
        $sections = mysql_query($sections_query);

        while($sections_row = mysql_fetch_array($sections, MYSQL_ASSOC)){    
            $id = stripslashes($sections_row['id']);
            print_r("<option value='".$sections_row['id']."'>".$sections_row['name']."</option>");
        }
    }

    function add_section($name){
        $url_name = replace_gap(translit($name));
        $result = mysql_query ("INSERT INTO `sections` (`name`, `url_name`) VALUES ('$name','$url_name')");
        if ($result) 
            $GLOBALS['msg']['good'] = "Раздел <strong>".$name."</strong> успешно создан!";
        else
            $GLOBALS['msg']['bad'] =  "Ошибка создания раздела!";
    }

    function add_cut($cut_name, $section_id){
        $url_name = replace_gap(translit($cut_name));

        $count = mysql_query("SELECT COUNT(1) FROM `categories` WHERE `section_id` = $section_id");
        $count = mysql_fetch_array( $count );
        $ordinal = $count[0] + 1;

        $result = mysql_query ("INSERT INTO `categories` (`ordinal`, `section_id`, `cut_name`, `url_name`) VALUES ('$ordinal','$section_id','$cut_name','$url_name')");
        if ($result) 
            $GLOBALS['msg']['good'] = "Категория <strong>".$cut_name."</strong> успешно создана!";
        else
            $GLOBALS['msg']['bad'] =  "Ошибка создания категории!";
    }

    function add_city($name){
        $url_name = replace_gap(translit($name));

        $result = mysql_query ("INSERT INTO `city` (`name`, `url_name`) VALUES ('$name','$url_name')");
        if ($result) 
            $GLOBALS['msg']['good'] = "Город <strong>".$name."</strong> успешно добавлен!";
        else
            $GLOBALS['msg']['bad'] =  "Ошибка добавления города!";
    }

    function select_city(){
        $city_query  = "SELECT * FROM `city` ORDER BY `url_name` DESC";
        $city = mysql_query($city_query);

        while($city_row = mysql_fetch_array($city, MYSQL_ASSOC)){    
            $id = stripslashes($city_row['id']);
            print_r("<option value='".$city_row['id']."'>".$city_row['name']."</option>");
        }
    }

    function add_locality($city_id, $type, $name){
        $url_name = replace_gap(translit($name));

        $result = mysql_query ("INSERT INTO `locality` (`city_id`, `name`, `type`, `url_name`) VALUES ('$city_id', '$name', '$type', '$url_name')");
        if ($result) 
            $GLOBALS['msg']['good'] = "Населенный пункт <strong>".$name."</strong> успешно добавлен!";
        else
            $GLOBALS['msg']['bad'] =  "Ошибка добавления населенного пункт!";
    }

    function edit_locality($id, $type, $name){
        $url_name = replace_gap(translit($name));

        $result = mysql_query("UPDATE `locality` SET `name`= '$name', `type`= '$type', `url_name`= '$url_name' WHERE `id` = $id") or die(mysql_error());
        if ($result) 
            $GLOBALS['msg']['good'] = "Изменения сохранены!";
        else
            $GLOBALS['msg']['bad'] =  "Ошибка сохранения зменений";
    }

    function edit_city($id, $name){
        $url_name = replace_gap(translit($name));
        
        $result = mysql_query("UPDATE `city` SET `name`= '$name', `url_name`= '$url_name' WHERE `id` = $id") or die(mysql_error());
        if ($result) 
            $GLOBALS['msg']['good'] = "Изменения сохранены!";
        else
            $GLOBALS['msg']['bad'] =  "Ошибка сохранения зменений";
    }

    function add_moder($city_id, $login, $password, $role, $fio){
        $result = mysql_query ("INSERT INTO `users` (`login`, `pass`, `role`, `city`, `fio`) VALUES ('$login', '$password', '$role', '$city_id', '$fio')");
        if ($result) 
            $GLOBALS['msg']['good'] = "Модератор <strong>".$fio."</strong> успешно добавлен!";
        else
            $GLOBALS['msg']['bad'] =  "Ошибка добавления модератора!";
    }

    function edit_moder($city_id, $login, $password, $role, $fio, $id){
        if ($password <> md5("")) {
            $result = mysql_query("UPDATE `users` SET `login`= '$login', `pass`= '$password', `role`= '$role', `city`= '$city_id', `fio`= '$fio' WHERE `id` = $id") or die(mysql_error());
        }else{
            $result = mysql_query("UPDATE `users` SET `login`= '$login', `role`= '$role', `city`= '$city_id', `fio`= '$fio' WHERE `id` = $id") or die(mysql_error());
        }
        
        if ($result) 
            $GLOBALS['msg']['good'] = "Изменения сохранены!";
        else
            $GLOBALS['msg']['bad'] =  "Ошибка сохранения зменений";
    }

    function moderate($moderate, $id, $city, $section, $category, $selest_locality, $street, $num, $etazh, $etazhnost, $mat, $size, $price, $srok, $exchange, $agent, $infra, $text, $phone, $contact_name){
        $advert_query  = "SELECT * FROM `adverts` WHERE `id` = $id";
        $advert = mysql_query($advert_query);
        $advert_row = mysql_fetch_array($advert);

        //определяем координаты адреса
        $city_result = mysql_query("select * from `city` WHERE `id`='$city'")or die(mysql_error());
        $city_name = mysql_fetch_array($city_result);
        $city_name = $city_name['name'];
        if ($district_name <> "") {
            $locality = $district_name;
        }else{
            $locality = $selest_locality;
            $locality_result = mysql_query("select * from `locality` WHERE `id`='$selest_locality'")or die(mysql_error());
            $locality_name = mysql_fetch_array($locality_result);
            $locality = $locality_name['name'];
        }
        
        //Составляем текстовый адрес
        $geocode = $city_name.", ".$locality.", ".$street.", ".$num;

        //получаем долготу и широту
        $params = array(
            'geocode' => $geocode, // адрес
            'format'  => 'json',                          // формат ответа
            'results' => 1,                               // количество выводимых результатов
            'key'     => 'AGLYD1sBAAAAzK3EYgMAeGlkXt3PSADkq3s2bEHuagIXIf4AAAAAAAAAAAB-bYWzsFD_GBmpDivWjaJJv16bcQ==',                           // ваш api key
        );
        $response = json_decode(file_get_contents('http://geocode-maps.yandex.ru/1.x/?' . http_build_query($params, '', '&')));
         
        if ($response->response->GeoObjectCollection->metaDataProperty->GeocoderResponseMetaData->found > 0) {
            $position_geo = $response->response->GeoObjectCollection->featureMember[0]->GeoObject->Point->pos;
            $position_geo_array = explode(" ", $position_geo);
        } else {
            $position_geo_array[1] = 0;
            $position_geo_array[0] = 0;
        }

        if ($advert_row['date'] == "") {
            $date = date('j ') . date('n');
        }else{
            $date = $advert_row['date'];
        }

        $kvm_price = round($price/$size);

        $result = mysql_query("UPDATE `adverts` SET `city_id` = '$city', `section_id` = '$section', `category_id` = '$category', `selest_locality` = '$selest_locality', `district_name` = '$district_name', `street` = '$street', `num` = '$num', `etazh` = '$etazh', `etazhnost` = '$etazhnost', `mat` = '$mat', `size` = '$size', `price` = '$price', `kvm_price` = '$kvm_price', `srok` = '$srok', `exchange` = '$exchange', `agent` = '$agent', `infra` = '$infra', `text` = '$text', `phone` = '$phone', `contact_name` = '$contact_name', `position_0` = '$position_geo_array[0]', `position_1` = '$position_geo_array[1]', `moderate`= '$moderate', `date`= '$date' WHERE `id` = $id") or die(mysql_error());

        if ($result){
            if ($moderate == 1) {
                $GLOBALS['msg']['good'] = "Модерация пройдена!";

            }else{
                $GLOBALS['msg']['good'] = "Объявление отклонено!";
            }
        }else{
            $GLOBALS['msg']['bad'] =  "Ошибка сохранения зменений";
        }
        header("Location: ".$GLOBALS['admin_dir']."moderate.php");
    }

    function find_ad($id){
        $advert = mysql_query("SELECT * FROM `adverts` WHERE `id`='$id'") or die(mysql_error());
        $advert_row = mysql_fetch_array($advert);

        return $advert_row;
    }

    function addNews($city, $name, $text){
        $result = mysql_query ("INSERT INTO `news` (`city_id`, `name`, `text`) VALUES ('$city', '$name', '$text')");
        if ($result) 
            $GLOBALS['msg']['good'] = "Новость размещена!";
        else
            $GLOBALS['msg']['bad'] =  "Ошибка размещена новости!";
    }

    function find_news($search){
        $news = mysql_query("SELECT * FROM `news` WHERE `id`='$search' OR `name`='$search'") or die(mysql_error());
        $news_row = mysql_fetch_array($news);

        return $news_row;
    }

    function editNews($id, $city, $name, $text){
        $result = mysql_query("UPDATE `news` SET `city_id`= '$city', `name`= '$name', `text`= '$text' WHERE `id` = $id") or die(mysql_error());
        
        if ($result) 
            $GLOBALS['msg']['good'] = "Изменения сохранены!";
        else
            $GLOBALS['msg']['bad'] =  "Ошибка сохранения зменений";
    }

    function addArt($city, $name, $text){
        $result = mysql_query ("INSERT INTO `articles` (`city_id`, `name`, `text`) VALUES ('$city', '$name', '$text')");
        if ($result) 
            $GLOBALS['msg']['good'] = "Статья размещена!";
        else
            $GLOBALS['msg']['bad'] =  "Ошибка размещена новости!";
    }

    function find_art($search){
        $news = mysql_query("SELECT * FROM `articles` WHERE `id`='$search' OR `name`='$search'") or die(mysql_error());
        $news_row = mysql_fetch_array($news);

        return $news_row;
    }

    function editArt($id, $city, $name, $text){
        $result = mysql_query("UPDATE `articles` SET `city_id`= '$city', `name`= '$name', `text`= '$text' WHERE `id` = $id") or die(mysql_error());
        
        if ($result) 
            $GLOBALS['msg']['good'] = "Изменения сохранены!";
        else
            $GLOBALS['msg']['bad'] =  "Ошибка сохранения зменений";
    }

    function update_config($sitename, $metakey, $metadeskription){
        $result = mysql_query("UPDATE config set `content` = '$sitename' WHERE name = 'sitename'") or die(mysql_error());
        if ($result) 
            $GLOBALS['msg']['good'] = "Изменения сохранены!";
        else
            $GLOBALS['msg']['bad'] =  "Ошибка сохранения зменений";

        $result = mysql_query("UPDATE `config` SET `content` = '$metakey' WHERE `name` = 'metakey'") or die(mysql_error());
        if ($result) 
            $GLOBALS['msg']['good'] = "Изменения сохранены!";
        else
            $GLOBALS['msg']['bad'] =  "Ошибка сохранения зменений";

        $result = mysql_query("UPDATE `config` SET `content` = '$metadeskription' WHERE `name` = 'metadeskription'") or die(mysql_error());
        if ($result) 
            $GLOBALS['msg']['good'] = "Изменения сохранены!";
        else
            $GLOBALS['msg']['bad'] =  "Ошибка сохранения зменений";
    }

    function can_upload($file){
      // если имя пустое, значит файл не выбран
      if($file['name'] == '')
          return 'Вы не выбрали файл.';
      
      /* если размер файла 0, значит его не пропустили настройки 
      сервера из-за того, что он слишком большой */
      if($file['size'] == 0)
          return 'Файл слишком большой.';
      
      // разбиваем имя файла по точке и получаем массив
      $getMime = explode('.', $file['name']);
      // нас интересует последний элемент массива - расширение
      $mime = strtolower(end($getMime));
      // объявим массив допустимых расширений
      $types = array('jpg', 'png', 'gif', 'bmp', 'jpeg');
      
      // если расширение не входит в список допустимых - return
      if(!in_array($mime, $types))
          return 'Недопустимый тип файла.';
      
      return true;
    }
    
    function make_upload($file){  
      $name = "h_bg.jpg";
      copy($file['tmp_name'], '../img/' . $name);
    }

    function add_tarif($name, $srok, $price){
        $result = mysql_query ("INSERT INTO `tarif` (`name`, `srok`, `price`) VALUES ('$name', '$srok', '$price')");
        if ($result) 
            $GLOBALS['msg']['good'] = "Новый тариф добавлен!";
        else
            $GLOBALS['msg']['bad'] =  "Ошибка создания тарифа!";
    }

    function edit_tarif($name, $srok, $price, $id){
        $result = mysql_query("UPDATE `tarif` SET `name`= '$name', `srok`= '$srok', `price`= '$price' WHERE `id` = $id") or die(mysql_error());
        if ($result) 
            $GLOBALS['msg']['good'] = "Изменения сохранены!";
        else
            $GLOBALS['msg']['bad'] =  "Ошибка сохранения зменений";
    }

    function generate_code(){
        $code = rand(0, 99999999);
        $code_l = strlen($code);
        if ($code_l < 8) {
            $l = 8 - $code_l;
            for ($i=0; $i < $l; $i++) { 
                $code = "0".$code;
            }
        }

        return $code;
    }

    function select_cut($categories){
        for ($i=0; $i < count($categories); $i++) { 
            if ($i == 0) {
              $cut = $categories[$i];
            }else{
              $cut .= ",".$categories[$i];
            }
        }

        return $cut;
    }

    function add_generate_code($code, $cut, $tarif){
        $start_date = date('j.').date('n').date('.Y');

        $result = mysql_query ("INSERT INTO `code` (`code`, `cut`, `start_date`, `tarif`) VALUES ('$code', '$cut', '$start_date', '$tarif')");

        if ($result) 
            $GLOBALS['msg']['good'] = "Код доступа успешно создан";
        else
            $GLOBALS['msg']['bad'] =  "Ошибка создания кода доступа";
    }
?>