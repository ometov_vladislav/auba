<?php 
    include 'header.php';

    if ($_SESSION['role'] <> 1){
        header("Location: /admin/");
        exit;
    }

    if (isset($_POST['add_section'])) {
        add_section($_POST['name']);
    }
    if (isset($_POST['add_cut'])) {
        add_cut($_POST['cut_name'], $_POST['section_id']);
    }
    
    print_msg($msg);
?>

    <script type="text/javascript">
    $(document).ready(function(){   
        function slideout(){
            setTimeout(function(){ $("#response").slideUp("slow", function () {}); }, 2000);
        }      
        $("#response").hide();       
        $(function() {
            $("#list ul").sortable({ opacity: 0.8, cursor: 'move', update: function() {
                var order = $(this).sortable("serialize") + '&update=update'; 
                $.post("updateCutList.php", order, function(theResponse){
                    $("#response").html(theResponse);
                    $("#response").slideDown('slow');
                    slideout();
                });                                                              
            }                                 
            });
        });
    }); 
    </script>

    <div id="response"></div>

    <?php
    $sections_query  = "SELECT * FROM `sections` ORDER BY `id`";
    $sections = mysql_query($sections_query);

    while($sections_row = mysql_fetch_array($sections, MYSQL_ASSOC)){    
        $id = stripslashes($sections_row['id']);   
    ?>

	<div class="col-xs-12">
		<div class="panel panel-primary">
	      <div class="panel-heading">
	        <h3 class="panel-title">
                <?php echo $sections_row['name']; ?>
            </h3>
            <a href="edit.php?cut=reviews&id=<?php echo $categories_row['id']; ?>" class="edit_btn">Изменить </a>
            <a href="cut_list.php?del=sections&del_id=<?php echo $categories_row['id']; ?>" class="del_btn" onClick="return window.confirm('Подтверждаете удаление категории?');">[x]</a> 
	      </div>
	      <div class="panel-body">
	        <div id="list" class="list">
                <ul>
                    <?php
                    $categories_query  = "SELECT * FROM `categories` WHERE `section_id` = $id  ORDER BY `ordinal`";
                    $categories = mysql_query($categories_query);

                    while($categories_row = mysql_fetch_array($categories, MYSQL_ASSOC)){    
                        $id = stripslashes($categories_row['id']);   
                    ?>
                    <li id="arrayorder_<?php echo $id ?>">
                        <div class="row">
                                <div class="col-xs-12 col-sm-9 col-md-10"><span><?php echo $categories_row['cut_name']; ?></span></div>
                                <div class="col-xs-12 col-sm-3 col-md-2">
                                    <a href="edit.php?cut=reviews&id=<?php echo $categories_row['id']; ?>" class="edit_btn">Изменить </a>
                                    <a href="cut_list.php?del=categories&del_id=<?php echo $categories_row['id']; ?>" class="del_btn" onClick="return window.confirm('Подтверждаете удаление категории?');"><b>[x]</b></a>
                                </div>
                            </div>
                    </li>
                    <?php } ?>
                </ul>
            </div>
	      </div>
	    </div>
	</div>
    <?php } ?>
<?php 
  include 'footer.php';
?>