<?php
  include 'header.php';

  $advert_id = $_GET['ad_id'];

  $get_city_url_name = $_GET['city'];
  $get_cut_url_name = $_GET['cut_name'];

  $advert_query  = "SELECT * FROM `adverts` WHERE `id` = $advert_id";
  $advert = mysql_query($advert_query);
  $advert_row = mysql_fetch_array($advert);

  $city_id = $advert_row['city_id'];
  $cut_id = $advert_row['category_id'];

  $city_result = mysql_query("select * from `city` WHERE `id`='$city_id'")or die(mysql_error());
  $city_row = mysql_fetch_array($city_result);
  $city_name = $city_row['name'];
  $city_url = $city_row['url_name'];
  $city_id = $city_row['id'];

  $cut_result = mysql_query("select * from `categories` WHERE `id`='$cut_id'")or die(mysql_error());
  $cut_row = mysql_fetch_array($cut_result);
  $cut_url = $cut_row['url_name'];
  $cut_name = $cut_row['cut_name'];
  $cut_id = $cut_row['id'];
  $section = $cut_row['section_id'];

  $locality_id = $advert_row['selest_locality'];
  $locality_result = mysql_query("select * from `locality` WHERE `id`='$locality_id' and `city_id`='$city_id'")or die(mysql_error());
  $locality_row = mysql_fetch_array($locality_result);
  $locality = $locality_row['name'];

  if (isset($_POST['check_code'])) {
    $check_code = check_code(mysql_real_escape_string($_POST['code']), $cut_id);

    if ($check_code == 0) {
      $msg = "Код не существует!";
    }elseif($check_code == 2){
      $msg = "Код не подходит для данной категории!";
    }
  }

  if (isset($_POST['edit'])) {
    $password = md5($_POST['password']);
    $check_pass = check_pass($password, $advert_id);
  }
  if (isset($_POST['stop_btn'])) {
    $password = md5($_POST['password']);
    $stop_ad = check_pass($password, $advert_id);
    if ($stop_ad == 1) {
      if ($advert_row['moderate'] == 2){
        start_ad($advert_id);
      }else{
        stop_ad($advert_id);
      }
    }
  }

  if (isset($_POST['send_edit'])) {
    $city = strip_tags($_POST['city']);
    $section = strip_tags($_POST['section']);
    $category = strip_tags($_POST['category']);
    $selest_locality = strip_tags($_POST['selest_locality']);
    $type = strip_tags($_POST['type']);
    $district_name = strip_tags($_POST['district_name']);
    $street = strip_tags($_POST['street']);
    $num = strip_tags($_POST['num']);
    $etazh = strip_tags($_POST['etazh']);
    $etazhnost = strip_tags($_POST['etazhnost']);
    $mat = strip_tags($_POST['mat']);
    $size = strip_tags($_POST['size']);
    $price = strip_tags($_POST['price']);
    $srok = strip_tags($_POST['srok']);
    $exchange = strip_tags($_POST['exchange']);
    $agent = strip_tags($_POST['agent']);
    $infra = strip_tags($_POST['infra']);
    $text = strip_tags($_POST['text']);
    $phone = strip_tags($_POST['phone']);
    $contact_name = strip_tags($_POST['contact_name']);

    if ($selest_locality == "-") {
        add_locality($city, $type, $district_name);
        $locality_id_result = mysql_query("SELECT id FROM `locality` ORDER BY id DESC LIMIT 1")or die(mysql_error());
        $locality_id_name = mysql_fetch_array($locality_id_result);
        $selest_locality = $locality_id_name['id'];
    }
    
    if ($advert_row['moderate'] == 0) {
      $moderate = 0;
    }else{
      $moderate = 3;
    }
    
    $id = $advert_id;

    edit_ad($moderate, $id, $city, $section, $category, $selest_locality, $street, $num, $etazh, $etazhnost, $mat, $size, $price, $srok, $exchange, $agent, $infra, $text, $phone, $contact_name);
  }

  $count_info_ad = mysql_num_rows($advert);

  if ($count_info_ad > 0){
    if ($get_city_url_name <> $city_url or $get_cut_url_name <> $cut_url) {
      header("Location: /".$city_url."/".$cut_url."/ad".$advert_id);
    }
  }else{
    header("Location: /");
    exit();
  }

  $adres = $locality.", ".$advert_row['street'].", ".$advert_row['num'];
  $title_page = $city_name." - ".$cut_name." - ".$adres;
?>
  <title><?php echo $site_name." ".$title_delimiter." ".$title_page; ?></title>
    <link rel="stylesheet" type="text/css" href="/slick/slick.css">
    <link rel="stylesheet" type="text/css" href="/slick/slick-theme.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.css" />

    <div id="ad_page">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <div class="page-header">
              <h2><?php echo $city_name.", ".$adres; ?> <span class="label label-info"><?php if ($section == 1) { echo "Аренда"; } else { echo "Продажа"; } ?></span></h2>
            </div>
          </div>
        </div>
      </div>

      <div class="container">
        <ol class="breadcrumb">
          <li><a href="/"><?php echo $city_name; ?></a></li>
          <li><a href="/<?php echo $_GET['city']; ?>"><?php if ($section == 1) { echo "Аренда недвижимости"; } else { echo "Продажа недвижимости"; } ?></a></li>
          <li><a href="/<?php echo $_GET['city']."/".$_GET['cut_name']; ?>"><?php echo $cut_name; ?></a></li>
          <li class="active"><?php echo $adres; ?></li>
        </ol>
      </div>
      
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            
            <div class="edit">
              <!-- <button type="button" class="btn btn-success edit_btn" data-toggle="modal" data-target="#upAd">
                <i class="glyphicon glyphicon-chevron-up"></i> Поднять в поиске
              </button> -->

              <button type="button" class="btn btn-default edit_btn" data-toggle="modal" data-target="#stopAd">
                <?php if ($advert_row['moderate'] == 2): ?>
                  <i class="glyphicon glyphicon-play"></i>
                  Включить объявление
                <?php else: ?>
                  <i class="glyphicon glyphicon-pause"></i>
                  Выключить объявление
                <?php endif ?>
              </button>
              <?php if ($advert_row['moderate'] == 1): ?>
              <button type="button" class="btn btn-default edit_btn" data-toggle="modal" <?php if ($check_pass == 1): ?> data-target="#editAdvert" <?php else: ?> data-target="#editAd" <?php endif ?>>
                <i class="glyphicon glyphicon-pencil"></i> Редактировать
              </button>
              <?php endif ?>
            </div>
 
            <!-- Поднять в поиске -->
            <div class="modal fade" id="upAd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Закрыть">
                      <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Вход в объявление №<?php echo $advert_id; ?></h4>
                  </div>
                  <div class="modal-body">
                    <p>Если вы являетесь владельцем объявления №10153984, то для получения доступа к нему введите пароль указанный при добавлении объявления.,</p>
                    <hr>
                    <form action="">
                      <div class="form-group">
                        <label for="password">Введите ваш пароль:</label>
                        <input type="password" id="password" class="form-control">
                      </div>
                    </form>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                    <button type="submit" class="btn btn-primary">Войти</button>
                  </div>
                </div>
              </div>
            </div>

            <div class="modal fade" id="stopAd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Закрыть">
                      <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Вход в объявление №<?php echo $advert_id; ?></h4>
                  </div>
                  <form action="" method="POST">
                  <div class="modal-body">
                    <p>Если вы являетесь владельцем объявления №<?php echo $advert_id; ?>, то для получения доступа к нему введите пароль указанный при добавлении объявления.</p>
                    <hr>
                    <div class="form-group">
                      <label for="password">Введите ваш пароль:</label>
                      <input type="password" id="password" name="password" class="form-control">
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                    <button type="submit" class="btn btn-primary" name="stop_btn">Войти</button>
                  </div>
                  </form>
                </div>
              </div>
            </div>

            <div class="modal fade" id="editAd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Закрыть">
                      <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Вход в объявление №<?php echo $advert_id; ?></h4>
                  </div>
                  <form action="" method="POST">
                  <div class="modal-body">
                    <p>Если вы являетесь владельцем объявления №<?php echo $advert_id; ?>, то для получения доступа к нему введите пароль указанный при добавлении объявления.</p>
                    <hr>
                    <div class="form-group">
                      <label for="password">Введите ваш пароль:</label>
                      <input type="password" id="password" name="password" class="form-control">
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                    <button type="submit" class="btn btn-primary" name="edit">Войти</button>
                  </div>
                  </form>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
      <?php if ($advert_row['moderate'] <> 1): ?>
        <?php 
        if ($advert_row['moderate'] == 0) {
          $text = "<strong>Внимание!</strong> Объявление еще не прошло проверку!";
        }elseif($advert_row['moderate'] == 2){
          $text = "<strong>Внимание!</strong> Объявление отключено собственником!";
        }elseif($advert_row['moderate'] == 3){
          $text = "<strong>Внимание!</strong> Объявление еще не прошло проверку после редактирования!";
        }
        ?>
        <div class="container">
          <div class="row">
            <div class="col-xs-12">
              <div class="alert alert-warning" role="alert">
                <?php echo $text; ?>
              </div>
            </div>
          </div>
        </div>
      <?php endif ?>

      <?php if (isset($msg)): ?>
        <div class="container">
          <div class="row">
            <div class="col-xs-12">
              <div class="alert alert-warning" role="alert">
                <?php echo $msg; ?>
              </div>
            </div>
          </div>
        </div>
      <?php endif ?>
      
      <?php if ($advert_row['moderate'] <> 2): ?>
      <div class="ads_list">
        <div class="container">
          <div class="row">
            <div class="col-xs-12">

              <div class="ad_deskr">
                <div class="row">
                  <?php if ($check_code == 1): ?>
                  <?php
                    $ad_photo_count_result = mysql_query("SELECT COUNT(*) FROM `photos` WHERE `ad_id` = $advert_id ORDER BY id")or die(mysql_error());
                    $ad_photo_count = mysql_fetch_row($ad_photo_count_result);
                    $ad_photo_count = $ad_photo_count[0];

                    $ad_photo_query  = "SELECT * FROM `photos` WHERE `ad_id` = $advert_id ORDER BY `id`";
                    $ad_photo = mysql_query($ad_photo_query) or die (mysql_error());

                    if ($ad_photo_count > 0) {
                  ?>
                  <div class="col-xs-12 col-sm-5 col-md-4 col-lg-4">
                      <div class="photo">
                          <section class="vertical slider">
                            <?php
                              while($ad_photo_row = mysql_fetch_array($ad_photo, MYSQL_ASSOC)){
                            ?>
                              <div>
                                <a data-fancybox="ad_photo" href="/watermark/watermark.php?image=<?php echo $ad_photo_row['url']; ?>&dir=addadverts">
                                  <img src="/watermark/watermark.php?image=<?php echo $ad_photo_row['url']; ?>&dir=addadverts">
                                </a>
                              </div>
                            <?php
                              }
                            ?>
                          </section>
                      </div>
                  </div>
                <?php } ?>
                <?php endif ?>
                  <div class="col-xs-12 <?php if(($ad_photo_count > 0) and ($check_code == 1)){ ?>col-sm-7 col-md-7 col-lg-8 <?php } ?>">
                    <div class="text">
                      <p><?php echo $advert_row['text']; ?></p>
                    </div>
                    <table class="table table-bordered">
                      <tbody>
                        <tr>
                          <th scope="row">Адрес</th>
                          <td><?php echo $advert_row['street'].", ".$advert_row['num']; ?></td>
                        </tr>
                        <?php if ($locality <> ""): ?>
                        <tr>
                          <th scope="row">Район</th>
                          <td><?php echo $locality; ?></td>
                        </tr>
                        <?php endif ?>
                        <?php if ($advert_row['infra'] <> ""): ?>
                        <tr>
                          <th scope="row">Инфраструктура</th>
                          <td>
                            <div class="infra">
                              <?php 
                                  $infra_array = explode(",", $advert_row['infra']);
                                  for ($i=0; $i < count($infra_array); $i++) { 
                                    echo "<span>".$infra_array[$i]."</span>";
                                  }
                              ?>
                            </div>
                          </td>
                        </tr>
                        <?php endif ?>
                        <tr>
                          <th scope="row">Цена</th>
                          <td>
                            <?php if ($advert_row['section_id'] == 1): ?>
                              <?php 
                                  if ($advert_row['srok'] == "long") {
                                    $srok = "месяц";
                                  }else{
                                    $srok = "сутки";
                                  }
                                ?>
                              <div class="price">
                                <?php echo number_format(round($advert_row['price']), 0, '.', ' '); ?> руб./<?php echo $srok; ?>
                              </div>                              
                            <?php else: ?>
                              <div class="price"><?php echo number_format(round($advert_row['price']), 0, '.', ' '); ?> руб.</div>
                              <div class="price_kvm"><?php echo number_format(round($advert_row['price']/$advert_row['size']), 0, '.', ' '); ?> руб./м²</div>
                            <?php endif ?>
                            
                          </td>
                        </tr>
                        <tr>
                          <th scope="row">Общая площадь</th>
                          <td><?php echo $advert_row['size']; ?> кв. м</td>
                        </tr>
                        <tr>
                          <th scope="row">Дата актуальности</th>
                          <?php 
                            $monthes = array(
                              1 => 'Января', 2 => 'Февраля', 3 => 'Марта', 4 => 'Апреля',
                              5 => 'Мая', 6 => 'Июня', 7 => 'Июля', 8 => 'Августа',
                              9 => 'Сентября', 10 => 'Октября', 11 => 'Ноября', 12 => 'Декабря'
                            );

                            $date = $advert_row['date'];
                            $date_withdraw = spliti (" ", $date, 2);
                          ?>
                          <td><?php echo $date_withdraw[0]." ".$monthes[$date_withdraw[1]]; ?></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <?php if ($check_code == 1): ?>
                    <?php if (($advert_row['position_1'] <> 0) or ($advert_row['position_1'] <> "") or ($advert_row['position_0'] <> 0) or ($advert_row['position_0'] <> "")): ?>
                    <div class="col-xs-12">
                      <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU&amp;load=package.full" type="text/javascript"></script>
                      <script>
                        ymaps.ready(function () {
                          var myMap = new ymaps.Map('YMapsID', {
                            center: [<?php echo $advert_row['position_1'].", ".$advert_row['position_0']; ?>],
                            zoom: 17,
                            controls: []
                          });

                          var myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
                            preset: 'islands#redDotIcon'
                          });

                          myMap.geoObjects.add(myPlacemark);
                        });
                      </script>
                      <div id="YMapsID"></div>
                    </div>
                    <?php endif ?>
                  <?php endif ?>

                  <!-- Если код не введен -->
                  <div class="col-xs-12">
                    <div class="hide_block text-center">
                      <?php if (($check_code == 0) or ($check_code == 2)): ?>
                        
                      
                      <div class="row">
                        <div class="col-xs-12 col-sm-6">
                          <h4><b>Открыть полный доступ</b></h4>
                          <p>Введите ваш код доступа к данной категории недвижимости:</p>
                          <form action="" method="post">
                            <div class="col-xs-12">
                              <div class="form-group">
                                <input type="number" name="code" class="form-control" required>
                              </div>
                            </div>
                            <div class="get_code">
                              <input type="submit" class="btn btn-success btn-lg" value="Открыть полный доступ" name="check_code">
                            </div>
                          </form>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                          <h4><b>Нет кода доступа?</b></h4>
                          <p>Вы можете открыть полный доступ к контактным данным собственников, фотографиям и геопозиции объектов недвижимости используя код доступа Получите свой код доступа для категории «<?php echo $cut_name; ?>»</p>
                          <div class="get_code">
                            <a href="#" class="btn btn-success btn-lg">Получить код</a>
                          </div>
                        </div>
                      </div>
                      <?php elseif ($check_code == 1): ?>
                      
                      <div class="phone_text">
                        <span><?php echo $advert_row['contact_name']; ?></span>
                        <span><?php echo $advert_row['phone']; ?></span>
                      </div>
                      <?php endif ?>
                    </div>

                  </div>

                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
      <?php endif ?>

    </div>

    <div id="lust_ad_list">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <div class="page-header">
              <h2>Похожие объявления</h2>
            </div>
          </div>
        </div>
      </div>

      <?php 
        $limit = 3; //СКОЛЬКО ЗАПИСЕЙ ПОДГРУЖАТЬ

        if ($advert_row['srok'] == "long") {
          $long = 1;
        } else{
          $short = 1;
        }

        $exchange = $advert_row['exchange'];

        printf('
        <input type="hidden" id="city_id" value="'.$city_id.'">
        <input type="hidden" id="cut_id" value="'.$cut_id.'">
        <input type="hidden" id="locality_id" value="'.$locality_id.'">
        <input type="hidden" id="section" value="'.$section.'">
        <input type="hidden" id="filtr_btn" value="1">
        <input type="hidden" id="sort_by" value="id">
        <input type="hidden" id="short_filtr" value="'.$short.'">
        <input type="hidden" id="long_filtr" value="'.$long.'">
        <input type="hidden" id="exchange" value="'.$exchange.'">
        <input type="hidden" id="limit_ajax" value="'.$limit.'">');
      ?>
      <script type="text/javascript" src="/ajax/js/load_ads_cut.js"></script>

      <div class="ads_list">
        <div class="container">
          <div class="row">
            <div id="ads_list" class="col-xs-12">
              <?php
                $monthes = array(
                  1 => 'Января', 2 => 'Февраля', 3 => 'Марта', 4 => 'Апреля',
                  5 => 'Мая', 6 => 'Июня', 7 => 'Июля', 8 => 'Августа',
                  9 => 'Сентября', 10 => 'Октября', 11 => 'Ноября', 12 => 'Декабря'
                );
                if ($locality_id <> ""){
                  if (isset($_POST['filtr'])) {
                    $adverts_query  = "SELECT * FROM `adverts` WHERE `city_id`='$city_id' AND `id`<>'$advert_id' AND `section_id`='$section'  AND `category_id`='$cut_id' AND `moderate`=1 AND `selest_locality` = $locality_id $filtr_query ORDER BY `$sort_by` DESC LIMIT $limit";
                  }else{
                    $adverts_query  = "SELECT * FROM `adverts` WHERE `city_id`='$city_id' AND `id`<>'$advert_id' AND `section_id`='$section'  AND `category_id`='$cut_id' AND `moderate`=1 AND `selest_locality` = $locality_id ORDER BY `id` DESC LIMIT $limit";
                  }
                }else{
                  if (isset($_POST['filtr'])) {
                    $adverts_query  = "SELECT * FROM `adverts` WHERE `city_id`='$city_id' AND `id`<>'$advert_id' AND `section_id`='$section'  AND `category_id`='$cut_id' AND `moderate`=1 $filtr_query ORDER BY `$sort_by` DESC LIMIT $limit";
                  }else{
                    $adverts_query  = "SELECT * FROM `adverts` WHERE `city_id`='$city_id' AND `id`<>'$advert_id' AND `section_id`='$section'  AND `category_id`='$cut_id' AND `moderate`=1 ORDER BY `id` DESC LIMIT $limit";
                  }
                }
                $adverts = mysql_query($adverts_query) or die (mysql_error());
                if (mysql_num_rows($adverts)>0) {
                  while($adverts_row = mysql_fetch_array($adverts, MYSQL_ASSOC)){
                    $locality_id = $adverts_row['selest_locality'];
                    $advert_id = $adverts_row['id'];
                    $locality_result = mysql_query("select * from `locality` WHERE `id`='$locality_id' and `city_id`='$city_id'")or die(mysql_error());
                    $locality_row = mysql_fetch_array($locality_result);
                    $locality = $locality_row['name'];

                    $adres = $city_name.", ".$locality.", ".$adverts_row['street'].", ".$adverts_row['num'];

                    $ad_photo_result = mysql_query("SELECT url FROM `photos` WHERE `ad_id` = $advert_id ORDER BY id DESC LIMIT 1")or die(mysql_error());
                    $ad_photo_name = mysql_fetch_array($ad_photo_result);
                    $ad_photo = $ad_photo_name['url'];

                    $ad_photo_count_result = mysql_query("SELECT url FROM `photos` WHERE `ad_id` = $advert_id ORDER BY id")or die(mysql_error());
                    $ad_photo_count = mysql_num_rows($ad_photo_count_result);

                    $date = $adverts_row['date'];
                    $date_withdraw = spliti (" ", $date, 2);

                    $current_time = date('j ') . date('n');

                    if ($date == $current_time) {
                      $date_type = 1;
                    }elseif ($date == date('j n', time()-24*3600)) {
                      $date_type = 2;
                    }else{
                      $date_type = 3;
                    }
              ?>
                <div class="ad "><!-- selected -->
                  <div class="row">
                    <?php if ($ad_photo_count > 0): ?>
                      <div class="col-xs-12 col-sm-2">
                        <a href="/<?php echo $_GET['city']."/".$_GET['cut_name']."/ad".$adverts_row['id']; ?>" target="_blank">
                          <div class="photo" style="background-image: url(/img/addadverts/<?php echo $ad_photo; ?>);"><div class="count"><?php echo $ad_photo_count; ?> фото</div></div>
                        </a>
                      </div>
                    <?php endif ?>
                    <div class="col-xs-12 <?php if ($ad_photo_count > 0): ?>col-sm-7 col-md-7<?php else: ?>col-sm-9 col-md-9<?php endif ?>">
                      <div class="deskr">
                        <div class="name"><a href="/<?php echo $_GET['city']."/".$_GET['cut_name']."/ad".$adverts_row['id']; ?>" target="_blank"><?php echo $adres; ?>, собственник</a></div>
                        <div class="text">
                          <?php if (strlen($adverts_row['text']) <= 300) {
                            echo $adverts_row['text'];
                          } else{
                            echo substr($adverts_row['text'], 0, strrpos(substr($adverts_row['text'], 0, 300), ' '))." ...";
                          } ?>
                        </div>
                        <div class="min_info">
                          <?php if ($date_type == 1): ?>
                            <div class="date today">Сегодня</div>
                          <?php elseif ($date_type == 2): ?>
                            <div class="date yesterday">Вчера</div>
                          <?php elseif ($date_type == 3): ?>
                            <div class="date"><?php echo $date_withdraw[0]." ".$monthes[$date_withdraw[1]]; ?></div>
                          <?php endif ?>
                          <div class="district"><?php echo $locality; ?></div>
                          <div class="size"><?php echo $adverts_row['size']; ?> кв. м</div>
                        </div>
                      </div>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3">
                      <?php if ($adverts_row['section_id'] == 1): ?>
                        <?php 
                          if ($adverts_row['srok'] == "long") {
                            $srok = "/месяц";
                          }else{
                            $srok = "/сутки";
                          }
                        ?>
                      <?php endif ?>
                      <div class="price"><?php echo number_format($adverts_row['price'], 0, '.', ' '); ?> руб.<?php echo $srok; ?></div>
                      <?php if ($adverts_row['section_id'] == 2): ?>
                      <div class="kvm_price"><?php echo number_format(round($adverts_row['price']/$adverts_row['size']), 0, '.', ' '); ?> тыс. руб./м²</div>
                      <?php endif ?>
                    </div>
                  </div>
                </div>
              <?php
                  }
                }else{
              ?>
                <div class="alert alert-info" role="alert">К сожаления объявлений нет!</div>
              <?php
                }
              ?>

            </div>
          </div>
        </div>
      </div>
      <div class="text-center">
        <button type="button" id="ads_list-load" class="btn btn-primary btn-lg">Загрузить еще</button>
      </div>
    </div>

    












    <?php 
      if (isset($check_pass)){
        $section = $advert_row['section_id'];

        $city_id = $advert_row['city_id'];

        $city_result = mysql_query("select * from `city` WHERE `id`='$city_id'")or die(mysql_error());
        $city_name = mysql_fetch_array($city_result);
        $city_name = $city_name['name'];

        $cut_id = $advert_row['category_id'];
        $cut_result = mysql_query("select * from `categories` WHERE `id`='$cut_id'")or die(mysql_error());
        $cut_name = mysql_fetch_array($cut_result);
        $cut_name = $cut_name['cut_name'];        

        if($advert_row['district_name'] == ""){
            $selest_locality_id = $advert_row['selest_locality'];
            $selest_locality_result = mysql_query("select * from `locality` WHERE `id`='$selest_locality_id'")or die(mysql_error());
            $selest_locality = mysql_fetch_array($selest_locality_result);
            $selest_locality = $selest_locality['name'];
        }else{
            $selest_locality = $advert_row['district_name'];
        }

        switch ($advert_row['mat']) {
            case "k":
                $mat = "Кирпичный";
                break;
            case "p":
                $mat = "Панельный";
                break;
            case "b":
                $mat = "Блочный";
                break;
            case "m":
                $mat = "Монолитный";
                break;
            case "mk":
                $mat = "Монолитно-кирпичный";
                break;
            case "d":
                $mat = "Деревянный";
                break;
        }

        if ($section == 1){
            if ($advert_row['srok'] == "long") {
                $srok = "На длительный срок";
            }else{
                $srok = "Посуточно";
            }
        } else {
            if ($advert_row['exchange'] == "0") {
                $exchange = "Нет";
            }else{
                $exchange = "Да";
            }
        }

        if ($advert_row['agent'] == 1) {
            $agent = "Да";
        }else{
            $agent = "Нет";
        }
      ?>
        <div id="editAdvert" class="modal fade">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <!-- Заголовок модального окна -->
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                  <h4 class="modal-title">Редактирование объявления #<?php echo $advert_row['id']; ?></h4>
                </div>
                <!-- Основное содержимое модального окна -->
                <form action="" method="post">
                    <div class="modal-body">
                  <fieldset>
                    <input type="hidden" name="city" value="<?php echo $city_id; ?>">
                    <input type="hidden" name="section" value="<?php echo $section; ?>">

                    <div class="form-group" name="cutegory">
                      <label>Категория объявления:</label>

                      <select class="form-control" name="category">
                        <?php
                        $sections_query  = "SELECT * FROM `sections` ORDER BY `id`";
                        $sections = mysql_query($sections_query);

                        while($sections_row = mysql_fetch_array($sections, MYSQL_ASSOC)){    
                            $id = stripslashes($sections_row['id']);   
                        ?>
                        <optgroup label="<?php echo $sections_row['name']; ?>">
                          <?php
                          $categories_query  = "SELECT * FROM `categories` WHERE `section_id` = $id ORDER BY `ordinal`";
                          $categories = mysql_query($categories_query);

                          while($categories_row = mysql_fetch_array($categories, MYSQL_ASSOC)){ 
                          ?>
                          <option value="<?php echo $categories_row['id']; ?>" <?php if ($advert_row['category_id'] == $categories_row['id']) { echo "selected"; } ?>><?php echo $categories_row['cut_name'];; ?></option>
                          <?php } ?>
                        </optgroup>
                        <?php } ?>
                      </select>
                    </div>

                    <div class="form-group">
                      <label for="name">Населенный пункт:</label>

                      <select id="selest_locality" name="selest_locality" class="form-control" required>
                        <?php
                          $type[1][1] = "Районы города";
                          $type[1][2] = "Пригород";
                          $type[1][3] = "Область";
                          $type[2][1] = 'none';
                          $type[2][2] = '#fff0b7';
                          $type[2][3] = '#d2f2b6';

                          $city_query = mysql_query("SELECT * FROM `city` WHERE `id`='$city_id'");
                          $city_query_row = mysql_fetch_array($city_query);
                        ?>
                        <?php
                            $locality_query  = "SELECT * FROM `locality` WHERE `city_id` = $city_id  ORDER BY `type`";
                            $locality = mysql_query($locality_query);

                            $i = 1;
                            while($locality_row = mysql_fetch_array($locality, MYSQL_ASSOC)){
                              $optgroup = $locality_row['type'];
                              if ($i == 1) { $last_optgroup = $locality_row['type']; }
                          ?>

                            <?php if (($i == 1) or ($optgroup <> $last_optgroup)): ?>
                              <optgroup label="<?php echo $type[1][$optgroup]; ?>" style="background: <?php echo $type[2][$optgroup]; ?>">
                              <?php $last_optgroup = $optgroup; ?>
                            <?php endif ?>

                              <option value="<?php echo $locality_row['id']; ?>" <?php if ($locality_row['id'] == $advert_row['selest_locality']) { echo "selected"; } ?>><?php echo $locality_row['name']; ?></option>

                            <?php if (($i <> 1) & ($optgroup <> $last_optgroup)): ?>
                              </optgroup>
                              <?php $last_optgroup = $last_optgroup + 1; ?>
                            <?php endif ?>

                          <?php 
                              $last_optgroup = $optgroup;
                              $i++;
                            }
                          ?>
                          </optgroup>

                        <option disabled></option>
                        <option value="-" <?php if ($advert_row['district_name'] <> ""){ echo "selected";} ?>>- Не относится ни к одному из указанных районов -</option>
                      </select>
                      <script>
                        $("#selest_locality").on('change', function(){
                          if($(this).val() == "-"){
                            $("#district_name").show();
                            $('#district_name_input').prop('required', true);
                          } else {
                            $("#district_name").hide();
                            $('#district_name_input').prop('required', false);
                          }
                        })
                      </script>
                    </div>
                    <div id="district_name" class="row" <?php if ($advert_row['district_name'] <> ""): ?>style="display: block;"<?php else: ?>style="display: none;"<?php endif ?>>
                        <div class="form-group col-xs-12 col-sm-6 col-md-4 col-lg-5">
                            <select id="selest_district" name="type" class="form-control" required="">
                                <option value="1">Район города (микрорайон города)</option>
                                <option value="2" style="background: #fff0b7;">Пригород</option>
                                <option value="3" style="background: #d2f2b6;">Область</option>
                            </select>
                        </div>
                        <div class="form-group col-xs-12 col-sm-6 col-md-8 col-lg-7">
                            <input id="district_name_input" type="text" name="district_name" class="form-control" value="<?php echo $advert_row['district_name']; ?>" placeholder="Введите имя населенного пункта">
                        </div>
                    </div>
                    
                    
                    <div class="row">
                      <div class="form-group col-xs-12 col-sm-6 col-md-8 col-lg-9">
                        <label>Улица</label>
                        <input type="text" name="street" class="form-control" value="<?php echo $advert_row['street']; ?>" required>
                      </div>

                      <div class="form-group col-xs-12 col-sm-6 col-md-4 col-lg-3">
                        <label>№ дома</label>
                        <input type="text" name="num" class="form-control" value="<?php echo $advert_row['num']; ?>" required>
                      </div>
                    </div>

                    <hr>

                    <div class="row">
                      <div class="form-group col-xs-6 col-sm-4">
                        <label for="etazh">Этаж:</label>
                        <select name="etazh" id="etazh" class="form-control" required>
                          <option value="-">-</option>
                          <option value="0" <?php if ($advert_row['etazh'] == 0) { echo "selected"; } ?>>цокольный</option>
                          <option value="1" <?php if ($advert_row['etazh'] == 1) { echo "selected"; } ?>>1</option>
                          <option value="2" <?php if ($advert_row['etazh'] == 2) { echo "selected"; } ?>>2</option>
                          <option value="3" <?php if ($advert_row['etazh'] == 3) { echo "selected"; } ?>>3</option>
                          <option value="4" <?php if ($advert_row['etazh'] == 4) { echo "selected"; } ?>>4</option>
                          <option value="5" <?php if ($advert_row['etazh'] == 5) { echo "selected"; } ?>>5</option>
                          <option value="6" <?php if ($advert_row['etazh'] == 6) { echo "selected"; } ?>>6</option>
                          <option value="7" <?php if ($advert_row['etazh'] == 7) { echo "selected"; } ?>>7</option>
                          <option value="8" <?php if ($advert_row['etazh'] == 8) { echo "selected"; } ?>>8</option>
                          <option value="9" <?php if ($advert_row['etazh'] == 9) { echo "selected"; } ?>>9</option>
                          <option value="10" <?php if ($advert_row['etazh'] == 10) { echo "selected"; } ?>>10</option>
                          <option value="11" <?php if ($advert_row['etazh'] == 11) { echo "selected"; } ?>>11</option>
                          <option value="12" <?php if ($advert_row['etazh'] == 12) { echo "selected"; } ?>>12</option>
                          <option value="13" <?php if ($advert_row['etazh'] == 13) { echo "selected"; } ?>>13</option>
                          <option value="14" <?php if ($advert_row['etazh'] == 14) { echo "selected"; } ?>>14</option>
                          <option value="15" <?php if ($advert_row['etazh'] == 15) { echo "selected"; } ?>>15</option>
                          <option value="16" <?php if ($advert_row['etazh'] == 16) { echo "selected"; } ?>>16</option>
                          <option value="17" <?php if ($advert_row['etazh'] == 17) { echo "selected"; } ?>>17</option>
                          <option value="18" <?php if ($advert_row['etazh'] == 18) { echo "selected"; } ?>>18</option>
                          <option value="19" <?php if ($advert_row['etazh'] == 19) { echo "selected"; } ?>>19</option>
                          <option value="20" <?php if ($advert_row['etazh'] == 20) { echo "selected"; } ?>>20</option>
                          <option value="21" <?php if ($advert_row['etazh'] == 21) { echo "selected"; } ?>>21</option>
                          <option value="22" <?php if ($advert_row['etazh'] == 22) { echo "selected"; } ?>>22</option>
                          <option value="23" <?php if ($advert_row['etazh'] == 23) { echo "selected"; } ?>>23</option>
                          <option value="24" <?php if ($advert_row['etazh'] == 24) { echo "selected"; } ?>>24</option>
                          <option value="25" <?php if ($advert_row['etazh'] == 25) { echo "selected"; } ?>>25</option>
                          <option value="26" <?php if ($advert_row['etazh'] == 26) { echo "selected"; } ?>>26</option>
                          <option value="27" <?php if ($advert_row['etazh'] == 27) { echo "selected"; } ?>>27</option>
                          <option value="28" <?php if ($advert_row['etazh'] == 28) { echo "selected"; } ?>>28</option>
                          <option value="29" <?php if ($advert_row['etazh'] == 29) { echo "selected"; } ?>>29</option>
                          <option value="30" <?php if ($advert_row['etazh'] == 30) { echo "selected"; } ?>>30</option>
                          <option value="31" <?php if ($advert_row['etazh'] == 31) { echo "selected"; } ?>>31</option>
                          <option value="32" <?php if ($advert_row['etazh'] == 32) { echo "selected"; } ?>>32</option>
                          <option value="33" <?php if ($advert_row['etazh'] == 33) { echo "selected"; } ?>>33</option>
                          <option value="34" <?php if ($advert_row['etazh'] == 34) { echo "selected"; } ?>>34</option>
                          <option value="35" <?php if ($advert_row['etazh'] == 35) { echo "selected"; } ?>>35</option>
                          <option value="36" <?php if ($advert_row['etazh'] == 36) { echo "selected"; } ?>>36</option>
                          <option value="37" <?php if ($advert_row['etazh'] == 37) { echo "selected"; } ?>>37</option>
                          <option value="38" <?php if ($advert_row['etazh'] == 38) { echo "selected"; } ?>>38</option>
                          <option value="39" <?php if ($advert_row['etazh'] == 39) { echo "selected"; } ?>>39</option>
                          <option value="40" <?php if ($advert_row['etazh'] == 40) { echo "selected"; } ?>>40</option>
                          <option value="41" <?php if ($advert_row['etazh'] == 41) { echo "selected"; } ?>>41</option>
                          <option value="42" <?php if ($advert_row['etazh'] == 42) { echo "selected"; } ?>>42</option>
                          <option value="43" <?php if ($advert_row['etazh'] == 43) { echo "selected"; } ?>>43</option>
                          <option value="44" <?php if ($advert_row['etazh'] == 44) { echo "selected"; } ?>>44</option>
                          <option value="45" <?php if ($advert_row['etazh'] == 45) { echo "selected"; } ?>>45</option>
                          <option value="46" <?php if ($advert_row['etazh'] == 46) { echo "selected"; } ?>>46</option>
                          <option value="47" <?php if ($advert_row['etazh'] == 47) { echo "selected"; } ?>>47</option>
                          <option value="48" <?php if ($advert_row['etazh'] == 48) { echo "selected"; } ?>>48</option>
                          <option value="49" <?php if ($advert_row['etazh'] == 49) { echo "selected"; } ?>>49</option>
                          <option value="50" <?php if ($advert_row['etazh'] == 50) { echo "selected"; } ?>>50</option>
                          <option value="51" <?php if ($advert_row['etazh'] == 51) { echo "selected"; } ?>>51</option>
                          <option value="52" <?php if ($advert_row['etazh'] == 52) { echo "selected"; } ?>>52</option>
                          <option value="53" <?php if ($advert_row['etazh'] == 53) { echo "selected"; } ?>>53</option>
                          <option value="54" <?php if ($advert_row['etazh'] == 54) { echo "selected"; } ?>>54</option>
                          <option value="55" <?php if ($advert_row['etazh'] == 55) { echo "selected"; } ?>>55</option>
                          <option value="56" <?php if ($advert_row['etazh'] == 56) { echo "selected"; } ?>>56</option>
                          <option value="57" <?php if ($advert_row['etazh'] == 57) { echo "selected"; } ?>>57</option>
                          <option value="58" <?php if ($advert_row['etazh'] == 58) { echo "selected"; } ?>>58</option>
                          <option value="59" <?php if ($advert_row['etazh'] == 59) { echo "selected"; } ?>>59</option>
                          <option value="60" <?php if ($advert_row['etazh'] == 60) { echo "selected"; } ?>>60</option>
                          <option value="61" <?php if ($advert_row['etazh'] == 61) { echo "selected"; } ?>>61</option>
                          <option value="62" <?php if ($advert_row['etazh'] == 62) { echo "selected"; } ?>>62</option>
                          <option value="63" <?php if ($advert_row['etazh'] == 63) { echo "selected"; } ?>>63</option>
                          <option value="64" <?php if ($advert_row['etazh'] == 64) { echo "selected"; } ?>>64</option>
                          <option value="65" <?php if ($advert_row['etazh'] == 65) { echo "selected"; } ?>>65</option>
                          <option value="66" <?php if ($advert_row['etazh'] == 66) { echo "selected"; } ?>>66</option>
                          <option value="67" <?php if ($advert_row['etazh'] == 67) { echo "selected"; } ?>>67</option>
                          <option value="68" <?php if ($advert_row['etazh'] == 68) { echo "selected"; } ?>>68</option>
                          <option value="69" <?php if ($advert_row['etazh'] == 69) { echo "selected"; } ?>>69</option>
                          <option value="70" <?php if ($advert_row['etazh'] == 70) { echo "selected"; } ?>>70</option>
                          <option value="71" <?php if ($advert_row['etazh'] == 71) { echo "selected"; } ?>>71</option>
                          <option value="72" <?php if ($advert_row['etazh'] == 72) { echo "selected"; } ?>>72</option>
                          <option value="73" <?php if ($advert_row['etazh'] == 73) { echo "selected"; } ?>>73</option>
                          <option value="74" <?php if ($advert_row['etazh'] == 74) { echo "selected"; } ?>>74</option>
                          <option value="75" <?php if ($advert_row['etazh'] == 75) { echo "selected"; } ?>>75</option>
                          <option value="76" <?php if ($advert_row['etazh'] == 76) { echo "selected"; } ?>>76</option>
                          <option value="77" <?php if ($advert_row['etazh'] == 77) { echo "selected"; } ?>>77</option>
                          <option value="78" <?php if ($advert_row['etazh'] == 78) { echo "selected"; } ?>>78</option>
                          <option value="79" <?php if ($advert_row['etazh'] == 79) { echo "selected"; } ?>>79</option>
                          <option value="80" <?php if ($advert_row['etazh'] == 80) { echo "selected"; } ?>>80</option>
                          <option value="81" <?php if ($advert_row['etazh'] == 81) { echo "selected"; } ?>>81</option>
                          <option value="82" <?php if ($advert_row['etazh'] == 82) { echo "selected"; } ?>>82</option>
                          <option value="83" <?php if ($advert_row['etazh'] == 83) { echo "selected"; } ?>>83</option>
                          <option value="84" <?php if ($advert_row['etazh'] == 84) { echo "selected"; } ?>>84</option>
                          <option value="85" <?php if ($advert_row['etazh'] == 85) { echo "selected"; } ?>>85</option>
                          <option value="86" <?php if ($advert_row['etazh'] == 86) { echo "selected"; } ?>>86</option>
                          <option value="87" <?php if ($advert_row['etazh'] == 87) { echo "selected"; } ?>>87</option>
                          <option value="88" <?php if ($advert_row['etazh'] == 88) { echo "selected"; } ?>>88</option>
                          <option value="89" <?php if ($advert_row['etazh'] == 89) { echo "selected"; } ?>>89</option>
                          <option value="90" <?php if ($advert_row['etazh'] == 90) { echo "selected"; } ?>>90</option>
                          <option value="91" <?php if ($advert_row['etazh'] == 91) { echo "selected"; } ?>>91</option>
                          <option value="92" <?php if ($advert_row['etazh'] == 92) { echo "selected"; } ?>>92</option>
                          <option value="93" <?php if ($advert_row['etazh'] == 93) { echo "selected"; } ?>>93</option>
                          <option value="94" <?php if ($advert_row['etazh'] == 94) { echo "selected"; } ?>>94</option>
                          <option value="95" <?php if ($advert_row['etazh'] == 95) { echo "selected"; } ?>>95</option>
                          <option value="96" <?php if ($advert_row['etazh'] == 96) { echo "selected"; } ?>>96</option>
                          <option value="97" <?php if ($advert_row['etazh'] == 97) { echo "selected"; } ?>>97</option>
                          <option value="98" <?php if ($advert_row['etazh'] == 98) { echo "selected"; } ?>>98</option>
                          <option value="99" <?php if ($advert_row['etazh'] == 99) { echo "selected"; } ?>>99</option>
                        </select>
                      </div>

                      <div class="form-group col-xs-6 col-sm-4">
                        <label for="etazhnost">Этажность:</label>
                        <select name="etazhnost" id="etazhnost" class="form-control" required>
                          <option value="-">-</option>
                          <option value="1" <?php if ($advert_row['etazhnost'] == 1) { echo "selected"; } ?>>1</option>
                          <option value="2" <?php if ($advert_row['etazhnost'] == 2) { echo "selected"; } ?>>2</option>
                          <option value="3" <?php if ($advert_row['etazhnost'] == 3) { echo "selected"; } ?>>3</option>
                          <option value="4" <?php if ($advert_row['etazhnost'] == 4) { echo "selected"; } ?>>4</option>
                          <option value="5" <?php if ($advert_row['etazhnost'] == 5) { echo "selected"; } ?>>5</option>
                          <option value="6" <?php if ($advert_row['etazhnost'] == 6) { echo "selected"; } ?>>6</option>
                          <option value="7" <?php if ($advert_row['etazhnost'] == 7) { echo "selected"; } ?>>7</option>
                          <option value="8" <?php if ($advert_row['etazhnost'] == 8) { echo "selected"; } ?>>8</option>
                          <option value="9" <?php if ($advert_row['etazhnost'] == 9) { echo "selected"; } ?>>9</option>
                          <option value="10" <?php if ($advert_row['etazhnost'] == 10) { echo "selected"; } ?>>10</option>
                          <option value="11" <?php if ($advert_row['etazhnost'] == 11) { echo "selected"; } ?>>11</option>
                          <option value="12" <?php if ($advert_row['etazhnost'] == 12) { echo "selected"; } ?>>12</option>
                          <option value="13" <?php if ($advert_row['etazhnost'] == 13) { echo "selected"; } ?>>13</option>
                          <option value="14" <?php if ($advert_row['etazhnost'] == 14) { echo "selected"; } ?>>14</option>
                          <option value="15" <?php if ($advert_row['etazhnost'] == 15) { echo "selected"; } ?>>15</option>
                          <option value="16" <?php if ($advert_row['etazhnost'] == 16) { echo "selected"; } ?>>16</option>
                          <option value="17" <?php if ($advert_row['etazhnost'] == 17) { echo "selected"; } ?>>17</option>
                          <option value="18" <?php if ($advert_row['etazhnost'] == 18) { echo "selected"; } ?>>18</option>
                          <option value="19" <?php if ($advert_row['etazhnost'] == 19) { echo "selected"; } ?>>19</option>
                          <option value="20" <?php if ($advert_row['etazhnost'] == 20) { echo "selected"; } ?>>20</option>
                          <option value="21" <?php if ($advert_row['etazhnost'] == 21) { echo "selected"; } ?>>21</option>
                          <option value="22" <?php if ($advert_row['etazhnost'] == 22) { echo "selected"; } ?>>22</option>
                          <option value="23" <?php if ($advert_row['etazhnost'] == 23) { echo "selected"; } ?>>23</option>
                          <option value="24" <?php if ($advert_row['etazhnost'] == 24) { echo "selected"; } ?>>24</option>
                          <option value="25" <?php if ($advert_row['etazhnost'] == 25) { echo "selected"; } ?>>25</option>
                          <option value="26" <?php if ($advert_row['etazhnost'] == 26) { echo "selected"; } ?>>26</option>
                          <option value="27" <?php if ($advert_row['etazhnost'] == 27) { echo "selected"; } ?>>27</option>
                          <option value="28" <?php if ($advert_row['etazhnost'] == 28) { echo "selected"; } ?>>28</option>
                          <option value="29" <?php if ($advert_row['etazhnost'] == 29) { echo "selected"; } ?>>29</option>
                          <option value="30" <?php if ($advert_row['etazhnost'] == 30) { echo "selected"; } ?>>30</option>
                          <option value="31" <?php if ($advert_row['etazhnost'] == 31) { echo "selected"; } ?>>31</option>
                          <option value="32" <?php if ($advert_row['etazhnost'] == 32) { echo "selected"; } ?>>32</option>
                          <option value="33" <?php if ($advert_row['etazhnost'] == 33) { echo "selected"; } ?>>33</option>
                          <option value="34" <?php if ($advert_row['etazhnost'] == 34) { echo "selected"; } ?>>34</option>
                          <option value="35" <?php if ($advert_row['etazhnost'] == 35) { echo "selected"; } ?>>35</option>
                          <option value="36" <?php if ($advert_row['etazhnost'] == 36) { echo "selected"; } ?>>36</option>
                          <option value="37" <?php if ($advert_row['etazhnost'] == 37) { echo "selected"; } ?>>37</option>
                          <option value="38" <?php if ($advert_row['etazhnost'] == 38) { echo "selected"; } ?>>38</option>
                          <option value="39" <?php if ($advert_row['etazhnost'] == 39) { echo "selected"; } ?>>39</option>
                          <option value="40" <?php if ($advert_row['etazhnost'] == 40) { echo "selected"; } ?>>40</option>
                          <option value="41" <?php if ($advert_row['etazhnost'] == 41) { echo "selected"; } ?>>41</option>
                          <option value="42" <?php if ($advert_row['etazhnost'] == 42) { echo "selected"; } ?>>42</option>
                          <option value="43" <?php if ($advert_row['etazhnost'] == 43) { echo "selected"; } ?>>43</option>
                          <option value="44" <?php if ($advert_row['etazhnost'] == 44) { echo "selected"; } ?>>44</option>
                          <option value="45" <?php if ($advert_row['etazhnost'] == 45) { echo "selected"; } ?>>45</option>
                          <option value="46" <?php if ($advert_row['etazhnost'] == 46) { echo "selected"; } ?>>46</option>
                          <option value="47" <?php if ($advert_row['etazhnost'] == 47) { echo "selected"; } ?>>47</option>
                          <option value="48" <?php if ($advert_row['etazhnost'] == 48) { echo "selected"; } ?>>48</option>
                          <option value="49" <?php if ($advert_row['etazhnost'] == 49) { echo "selected"; } ?>>49</option>
                          <option value="50" <?php if ($advert_row['etazhnost'] == 50) { echo "selected"; } ?>>50</option>
                          <option value="51" <?php if ($advert_row['etazhnost'] == 51) { echo "selected"; } ?>>51</option>
                          <option value="52" <?php if ($advert_row['etazhnost'] == 52) { echo "selected"; } ?>>52</option>
                          <option value="53" <?php if ($advert_row['etazhnost'] == 53) { echo "selected"; } ?>>53</option>
                          <option value="54" <?php if ($advert_row['etazhnost'] == 54) { echo "selected"; } ?>>54</option>
                          <option value="55" <?php if ($advert_row['etazhnost'] == 55) { echo "selected"; } ?>>55</option>
                          <option value="56" <?php if ($advert_row['etazhnost'] == 56) { echo "selected"; } ?>>56</option>
                          <option value="57" <?php if ($advert_row['etazhnost'] == 57) { echo "selected"; } ?>>57</option>
                          <option value="58" <?php if ($advert_row['etazhnost'] == 58) { echo "selected"; } ?>>58</option>
                          <option value="59" <?php if ($advert_row['etazhnost'] == 59) { echo "selected"; } ?>>59</option>
                          <option value="60" <?php if ($advert_row['etazhnost'] == 60) { echo "selected"; } ?>>60</option>
                          <option value="61" <?php if ($advert_row['etazhnost'] == 61) { echo "selected"; } ?>>61</option>
                          <option value="62" <?php if ($advert_row['etazhnost'] == 62) { echo "selected"; } ?>>62</option>
                          <option value="63" <?php if ($advert_row['etazhnost'] == 63) { echo "selected"; } ?>>63</option>
                          <option value="64" <?php if ($advert_row['etazhnost'] == 64) { echo "selected"; } ?>>64</option>
                          <option value="65" <?php if ($advert_row['etazhnost'] == 65) { echo "selected"; } ?>>65</option>
                          <option value="66" <?php if ($advert_row['etazhnost'] == 66) { echo "selected"; } ?>>66</option>
                          <option value="67" <?php if ($advert_row['etazhnost'] == 67) { echo "selected"; } ?>>67</option>
                          <option value="68" <?php if ($advert_row['etazhnost'] == 68) { echo "selected"; } ?>>68</option>
                          <option value="69" <?php if ($advert_row['etazhnost'] == 69) { echo "selected"; } ?>>69</option>
                          <option value="70" <?php if ($advert_row['etazhnost'] == 70) { echo "selected"; } ?>>70</option>
                          <option value="71" <?php if ($advert_row['etazhnost'] == 71) { echo "selected"; } ?>>71</option>
                          <option value="72" <?php if ($advert_row['etazhnost'] == 72) { echo "selected"; } ?>>72</option>
                          <option value="73" <?php if ($advert_row['etazhnost'] == 73) { echo "selected"; } ?>>73</option>
                          <option value="74" <?php if ($advert_row['etazhnost'] == 74) { echo "selected"; } ?>>74</option>
                          <option value="75" <?php if ($advert_row['etazhnost'] == 75) { echo "selected"; } ?>>75</option>
                          <option value="76" <?php if ($advert_row['etazhnost'] == 76) { echo "selected"; } ?>>76</option>
                          <option value="77" <?php if ($advert_row['etazhnost'] == 77) { echo "selected"; } ?>>77</option>
                          <option value="78" <?php if ($advert_row['etazhnost'] == 78) { echo "selected"; } ?>>78</option>
                          <option value="79" <?php if ($advert_row['etazhnost'] == 79) { echo "selected"; } ?>>79</option>
                          <option value="80" <?php if ($advert_row['etazhnost'] == 80) { echo "selected"; } ?>>80</option>
                          <option value="81" <?php if ($advert_row['etazhnost'] == 81) { echo "selected"; } ?>>81</option>
                          <option value="82" <?php if ($advert_row['etazhnost'] == 82) { echo "selected"; } ?>>82</option>
                          <option value="83" <?php if ($advert_row['etazhnost'] == 83) { echo "selected"; } ?>>83</option>
                          <option value="84" <?php if ($advert_row['etazhnost'] == 84) { echo "selected"; } ?>>84</option>
                          <option value="85" <?php if ($advert_row['etazhnost'] == 85) { echo "selected"; } ?>>85</option>
                          <option value="86" <?php if ($advert_row['etazhnost'] == 86) { echo "selected"; } ?>>86</option>
                          <option value="87" <?php if ($advert_row['etazhnost'] == 87) { echo "selected"; } ?>>87</option>
                          <option value="88" <?php if ($advert_row['etazhnost'] == 88) { echo "selected"; } ?>>88</option>
                          <option value="89" <?php if ($advert_row['etazhnost'] == 89) { echo "selected"; } ?>>89</option>
                          <option value="90" <?php if ($advert_row['etazhnost'] == 90) { echo "selected"; } ?>>90</option>
                          <option value="91" <?php if ($advert_row['etazhnost'] == 91) { echo "selected"; } ?>>91</option>
                          <option value="92" <?php if ($advert_row['etazhnost'] == 92) { echo "selected"; } ?>>92</option>
                          <option value="93" <?php if ($advert_row['etazhnost'] == 93) { echo "selected"; } ?>>93</option>
                          <option value="94" <?php if ($advert_row['etazhnost'] == 94) { echo "selected"; } ?>>94</option>
                          <option value="95" <?php if ($advert_row['etazhnost'] == 95) { echo "selected"; } ?>>95</option>
                          <option value="96" <?php if ($advert_row['etazhnost'] == 96) { echo "selected"; } ?>>96</option>
                          <option value="97" <?php if ($advert_row['etazhnost'] == 97) { echo "selected"; } ?>>97</option>
                          <option value="98" <?php if ($advert_row['etazhnost'] == 98) { echo "selected"; } ?>>98</option>
                          <option value="99" <?php if ($advert_row['etazhnost'] == 99) { echo "selected"; } ?>>99</option>
                        </select>
                      </div>

                      <div class="form-group col-xs-12 col-sm-4">
                        <label for="etazhnost">Тип дома:</label>
                        <select name="mat" id="mat" class="form-control" required>
                          <option value="k" <?php if ($advert_row['etazhnost'] == "k") { echo "selected"; } ?>>Кирпичный</option>
                          <option value="p" <?php if ($advert_row['etazhnost'] == "p") { echo "selected"; } ?>>Панельный</option>
                          <option value="b" <?php if ($advert_row['etazhnost'] == "b") { echo "selected"; } ?>>Блочный</option>
                          <option value="m" <?php if ($advert_row['etazhnost'] == "m") { echo "selected"; } ?>>Монолитный</option>
                          <option value="mk" <?php if ($advert_row['etazhnost'] == "mk") { echo "selected"; } ?>>Монолитно-кирпичный</option>
                          <option value="d" <?php if ($advert_row['etazhnost'] == "d") { echo "selected"; } ?>>Деревянный</option>
                        </select>
                      </div>
                    </div>
                    
                    <div class="row">
                      
                      <div class="form-group col-xs-12 col-sm-6">
                        <label for="size">Общая площадь:</label>
                        <input type="text" name="size" id="size" class="form-control only_num" value="<?php echo $advert_row['size']; ?>" required>
                        <span class="kvm"></span>
                      </div>

                      <div class="form-group col-xs-12 col-sm-6">
                        <label for="price">Стоимость:</label>
                        <input type="text" name="price" id="price" class="form-control only_num" value="<?php echo $advert_row['price']; ?>" required>
                        <span class="price_kvm"></span>
                      </div>
                      <?php if ($section == 2): ?>
                      <script>
                        $('.only_num').bind("change keyup input click", function() {
                          if (this.value.match(/[^0-9]/g)) {
                            this.value = this.value.replace(/[^0-9]/g, '');
                          }
                        });

                        $('#size').bind('keyup keydown paste',function(e) {
                          var $size=$('#size');
                          var $price_no_space = $('#price').val().replace(/\s+/g,'');

                          setTimeout(function(){
                            $('.kvm').text($size.val() + " кв.м");
                            $price_kvm = $price_no_space / $size.val();
                            $('.price_kvm').text($price_kvm.toFixed(0) + " руб./кв.м");
                          },0);
                        });

                        $('#price').bind('keyup keydown paste',function(e) {
                          var $price = $(this);
                          var $price_no_space = $price.val().replace(/\s+/g,'');
                          var $size = $('#size');

                          setTimeout(function(){
                            $price_kvm = $price_no_space / $size.val();
                            $('.price_kvm').text($price_kvm.toFixed(0) + " руб./кв.м");
                          },0);
                        });
                      </script>
                      <?php endif ?>
                    </div>

                    <hr>

                    <div class="row">
                      <?php if ($section == 1): ?>
                      <div class="form-group col-xs-12 col-sm-6">
                        <label for="srok">Срок аренды:</label>
                        <select name="srok" id="srok" class="form-control" required>
                          <option value="long" <?php if ($advert_row['srok'] == "long") { echo "selected"; } ?>>На длительный срок</option>
                          <option value="short" <?php if ($advert_row['srok'] == "short") { echo "selected"; } ?>>Посуточно</option>
                        </select>

                        <script>
                          $("#srok").on('change', function(){
                            $("#long").hide();
                            $("#short").hide();
                            if($(this).val() == "long"){
                              $("#long").show();
                            } else {
                              $("#short").show();
                            }
                          })
                        </script>
                        <span id="long" class="kvm" <?php if ($advert_row['srok'] == "short") { echo "style='display: none;'"; } ?>>Указана цена за месяц</span>
                        <span id="short" class="kvm" <?php if ($advert_row['srok'] == "long") { echo "style='display: none;'"; } ?>>Указана цена за сутки</span>
                      </div>
                      <?php else: ?>
                      <div class="form-group col-xs-12 col-sm-6">
                        <label>Возможность обмена:</label>
                        <div class="label_deskr">
                          Рассматриваете ли вы возможность обмена недвижимости?
                        </div>
                        <label class="radio_check"><input type="radio" name="exchange" value="1" <?php if ($advert_row['exchange'] == "1") { echo "checked"; } ?>> Да</label>
                        <label class="radio_check"><input type="radio" name="exchange" value="0" <?php if ($advert_row['exchange'] == "0") { echo "checked"; } ?>> Нет</label>
                      </div>
                      <?php endif ?>

                      <div class="form-group col-xs-12 col-sm-6">
                        <label>Принимаете услуги риэлторов?</label>
                        <div class="label_deskr">
                          Готовы ли вы к сотрудничеству с агентствами недвижимости
                        </div>
                        <label class="radio_check"><input type="radio" name="agent" value="1" <?php if ($advert_row['agent'] == "1") { echo "checked"; } ?>> Да</label>
                        <label class="radio_check"><input type="radio" name="agent" value="0" <?php if ($advert_row['agent'] == "0") { echo "checked"; } ?>> Нет</label>
                      </div>
                    </div>

                    <hr>

                    <div class="form-group">
                      <label for="infra">Инфраструктура</label>
                      <input type="text" name="infra" id="infra" class="form-control" value="<?php echo $advert_row['infra']; ?>" placeholder="Перечислите через запятую">
                    </div>

                    <hr>

                    <div class="form-group">
                      <label for="text">Текст объявления</label>
                      <textarea name="text" id="text" class="form-control" rows="8" style="resize: none;" required><?php echo $advert_row['text']; ?></textarea>
                    </div>
                    
                    <hr>

                    <div class="form-group">
                      <label for="photos">Фотографии</label>
                      
                    </div>

                    <hr>

                    <div class="row">                  
                      <div class="form-group col-xs-12 col-sm-6">
                        <label for="phone">Номер телефона:</label>
                        <input type="text" name="phone" id="phone" class="form-control" placeholder="+7 (___) ___-____" value="<?php echo $advert_row['phone']; ?>" required>
                      </div>
                      <script type="text/javascript" src="/js/input_mask.js"></script>
                      <script>
                        $(document).ready(function() {
                          $("#phone").mask("+7 (999) 999-9999");
                        });
                      </script>

                      <div class="form-group col-xs-12 col-sm-6">
                        <label for="contact_name">Контактное лицо:</label>
                        <input type="text" name="contact_name" id="contact_name" class="form-control" placeholder="Например, имя и отчество собственника" value="<?php echo $advert_row['contact_name']; ?>" required>
                      </div>
                    </div>

                  </fieldset>

                    </div>
                    <!-- Футер модального окна -->
                    <div class="modal-footer">
                        <input type="hidden" name="id" value="<?php echo $advert_row['id']; ?>" required>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                        <input type="submit" class="btn btn-success" name="send_edit" value="Внести изменения">
                    </div>
                </form>
            </div>
          </div>
        </div>
      <?php
      } 
      ?>

    

<?php
  include 'footer.php';
?>