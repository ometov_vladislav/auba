<?php
  include 'header.php';
  $title_page = "Главная";
?>
  <title><?php echo $site_name." ".$title_delimiter." ".$title_page; ?></title>

  <div id="city_list" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog ">
      <div class="modal-content">
        <div class="modal-content">

          <div class="modal-header">
            <h4 class="modal-title" id="mySmallModalLabel">Выберите город для быстрого и удобного<br>поиска недвижимости от собственников</h4>
          </div>
          <div class="modal-body">
            <div class="city_list_modal">
              <a href="/addadvert/" class="btn btn-primary">Разместить объявление <i class="glyphicon glyphicon-plus"></i></a>
              <ul>
                <?php
                $city_query  = "SELECT * FROM `city` ORDER BY `name`";
                $city = mysql_query($city_query);
                while($city_row = mysql_fetch_array($city, MYSQL_ASSOC)){
                ?>
                <li><a href="/<?php echo $city_row['url_name']; ?>"><?php echo $city_row['name']; ?></a></li>
                <?php } ?>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

<?php
  include 'footer.php';
?>