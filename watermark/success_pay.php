<?php 
include 'header.php'; 

/*
==========================================
    Страница, на которую будет переведён
    пользователь после успешной оплаты.
    Данная страница не обязательна, вместо
    неё можно переводить пользователя на
    главную страницу сайта, но лучше так.
==========================================
*/

$mrh_pass1 = "tzB7HhxZ2VjZ9D0NpWz5"; // пароль #1

// чтение параметров
$out_summ = $_REQUEST["OutSum"]; // по умолчанию (не трогать)
$inv_id = $_REQUEST["InvId"]; // по умолчанию (не трогать)
$shp_item = $_REQUEST["Shp_item"]; // по умолчанию (не трогать)
$crc = $_REQUEST["SignatureValue"]; // по умолчанию (не трогать)

$shp_mulo = $_REQUEST["shp_mulo"]; // если нужно принимаем дополнительный параметр
$shp_names = $_REQUEST["shp_names"]; // если нужно принимаем дополнительный параметр
$shp_phone = $_REQUEST["shp_phone"]; // если нужно принимаем дополнительный параметр

$crc = strtoupper($crc); // переводим ключ в верхний регистр

$my_crc = strtoupper(md5("$out_summ:$inv_id:$mrh_pass1:Shp_item=$shp_item:shp_mulo=$shp_mulo:shp_names=$shp_names:shp_phone=$shp_phone")); // формируем новый ключ

if ($my_crc != $crc) // проверка корректности подписи
{
  echo "bad sign\n";
  exit(); // останавливаем выполнение скрипта, если подписи не совпадают
}


$code = $shp_item;
$selected_cut = urldecode(base64_decode($shp_names));
$tarif = urldecode(base64_decode($shp_mulo));

// echo $code;
find_code($code);

if (find_code($code) == 0) {
	add_generate_code($code, $selected_cut, $tarif);
	$code_status = 1;
}else{
	$msg = "Код уже активирован!";
}

$title_page = "Оплата прошла успешно";
?>
<title><?php echo $site_name." ".$title_delimiter." ".$title_page; ?></title>

<div class="container">
    <div class="col-xs-12">
        <div class="by_code">
			<?php if ($code_status <> 1): ?>
				<div class="alert alert-warning" role="alert">
                	<?php echo $msg; ?>              
            	</div>
            	<h1 class="text-center"><?php echo $code; ?></h1>
            <?php else: ?>
				<div class="alert alert-success" role="alert">
                	Ваш код для получения доступа к выбранным вами категориям             
            	</div>
            	<h1 class="text-center"><?php echo $code; ?></h1>
			<?php endif ?>
        </div>
   	</div>
</div> 

<?php include 'footer.php'; ?>