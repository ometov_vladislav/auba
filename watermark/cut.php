<?php
  include 'header.php';

  if (isset($_GET['locality_id'])) {
    $locality_id = $_GET['locality_id'];

    $select_locality_query  = "SELECT * FROM `locality` WHERE `id` = $locality_id  ORDER BY `type`";
    $select_locality = mysql_query($select_locality_query);
    $select_locality_row = mysql_fetch_array($select_locality);
    $select_locality_name = $select_locality_row['name'];
  }

  $city_url_name = $_GET['city'];
  $city_result = mysql_query("select * from `city` WHERE `url_name`='$city_url_name'")or die(mysql_error());
  $city_row = mysql_fetch_array($city_result);
  $city_name = $city_row['name'];
  $city_id = $city_row['id'];
  
  $cut_url_name = $_GET['cut_name'];
  $cut_result = mysql_query("select * from `categories` WHERE `url_name`='$cut_url_name'")or die(mysql_error());
  $cut_row = mysql_fetch_array($cut_result);
  $cut_name = $cut_row['cut_name'];
  $cut_id = $cut_row['id'];
  $section = $cut_row['section_id'];

  if ($city_name == "" or $cut_name == "") {
    header("Location: /");
  }

  $filtr_btn = 0;
  if (isset($_POST['filtr'])) {
    $filtr_btn = 1;
    $sort_by = $_POST['sort_by'];
    if ($sort_by == "date") {
      $sort_by = "id";
    }

    if ($section == 1) {
      $short = $_POST['short'];
      $long = $_POST['long'];
      if ($short <> "") {
        $filtr_query =" AND `srok` = 'short'";
      }
      if ($long <> "") {
        $filtr_query =" AND `srok` = 'long'";
      }
      if (($short == "" and $long == "") or ($short <> "" and $long <> "")) {
        $filtr_query = "";
      }
    }else{
      $exchange = $_POST['exchange'];
      $filtr_query = "AND `exchange` = $exchange";
    }
  }

  $limit = 15; //СКОЛЬКО ЗАПИСЕЙ ПОДГРУЖАТЬ

  printf('
    <input type="hidden" id="city_id" value="'.$city_id.'">
    <input type="hidden" id="cut_id" value="'.$cut_id.'">
    <input type="hidden" id="locality_id" value="'.$locality_id.'">
    <input type="hidden" id="section" value="'.$section.'">
    <input type="hidden" id="filtr_btn" value="'.$filtr_btn.'">
    <input type="hidden" id="sort_by" value="'.$sort_by.'">
    <input type="hidden" id="short_filtr" value="'.$short.'">
    <input type="hidden" id="long_filtr" value="'.$long.'">
    <input type="hidden" id="exchange" value="'.$exchange.'">
    <input type="hidden" id="limit_ajax" value="'.$limit.'">');

  $title_page = $city_name." - ".$cut_name;
?>
  <title><?php echo $site_name." ".$title_delimiter." ".$title_page; ?></title>
  
  <script type="text/javascript" src="/ajax/js/load_ads_cut.js"></script>

    <div id="cut_list">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <div class="page-header">
              <h2><?php echo $cut_name; ?> в <?php echo $city_name; ?>е от собственника</h2>
            </div>
          </div>

          <div class="col-xs-12">
            <button type="button" id="filtr" class="btn btn-default edit_btn visible-xs">
              Фильтр <i class="glyphicon glyphicon-cog"></i>
            </button>
            <div class="filtr">
              <div class="row">

                <div class="col-xs-12 col-sm-5 col-md-3 col-lg-3">
                  <div id="raion">
                    <span class="filtr_name">Район:</span>
                    <ul class="nav nav-pills">
                      <li class="dropdown">
                        <a href="#" data-toggle="dropdown" class="btn btn-success dropdown-toggle">
                          <?php if ($locality_id <> ""): ?>
                            <?php echo $select_locality_name; ?>
                          <?php else: ?>
                            Все районы
                          <?php endif ?>
                          <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu">
                          <?php if ($locality_id <> ""): ?>
                            <li><a href="<?php echo "/".$city_url_name."/".$cut_url_name; ?>">Все районы</a></li>
                            <li class="divider"></li>
                          <?php endif ?>
                          <?php
                            $type[1][1] = "Районы города";
                            $type[1][2] = "Пригород";
                            $type[1][3] = "Область";
                            $type[2][1] = 'none';
                            $type[2][2] = '#fff0b7';
                            $type[2][3] = '#d2f2b6';

                            $locality_query  = "SELECT * FROM `locality` WHERE `city_id` = $city_id  ORDER BY `type`";
                            $locality = mysql_query($locality_query);

                            $i = 1;
                            while($locality_row = mysql_fetch_array($locality, MYSQL_ASSOC)){
                              $optgroup = $locality_row['type'];
                          ?>
                            <?php if (($i <> 1) & ($optgroup <> $last_optgroup)): ?>
                              <li class="divider"></li>
                            <?php endif ?>
                              <li <?php if ($locality_id == $locality_row['id']) { echo "class='active'"; } ?>><a href="<?php echo "/".$city_url_name."/".$cut_url_name."/locality".$locality_row['id']; ?>"><?php echo $locality_row['name']; ?></a></li>
                          <?php 
                            $last_optgroup = $locality_row['type'];
                            $i++;
                            } 
                          ?>
                        </ul>
                      </li>
                    </ul>
                  </div>
                </div>
                <form action="" method="POST">
                <div class="col-xs-12 col-sm-7 col-md-4 col-lg-3">
                  <div id="sort">
                    <span class="filtr_name">Сортировать по:</span>
                    <div class="filtr_group">
                      <div class="filtr_group-block">
                        <input id="sort_by_date" type="radio" name="sort_by" value="date" <?php if ($sort_by == "id") {echo "checked";} if (!isset($_POST['filtr'])) {echo "checked";} ?>>
                        <label for="sort_by_date">Дате</label>
                      </div>
                      <div class="filtr_group-block">
                        <input id="sort_by_size" type="radio" name="sort_by" value="size" <?php if ($sort_by == "size") {echo "checked";} ?>>
                        <label for="sort_by_size">Площади</label>
                      </div>
                      <?php if ($section == 2): ?>
                      <div class="filtr_group-block">
                        <input id="sort_by_kvm_price" type="radio" name="sort_by" value="kvm_price" <?php if ($sort_by == "kvm_price") {echo "checked";} ?>>
                        <label for="sort_by_kvm_price">Цене 1 м²</label>
                      </div>
                      <?php endif ?>
                    </div>
                  </div>
                </div>

                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                  <div id="trade">
                    <span class="filtr_name">Дополнительно:</span>

                    <div class="filtr_group">
                      <?php if ($section == 1): ?>
                      <div class="filtr_group-block">
                        <input id="short" type="checkbox" name="short" value="1" <?php if ($short <> "") {echo "checked";}  if (!isset($_POST['filtr'])) {echo "checked";} ?>>
                        <label for="short">Посуточно</label>
                      </div>
                      <div class="filtr_group-block">
                        <input id="long" type="checkbox" name="long" value="1" <?php if ($long <> "") {echo "checked";}  if (!isset($_POST['filtr'])) {echo "checked";} ?>>
                        <label for="long">Длительный срок</label>
                      </div>
                      <?php else: ?>
                      <div class="filtr_group-block">
                        <input id="exchange1" type="checkbox" name="exchange" value="1" <?php if ($exchange <> "") {echo "checked";} ?>>
                        <label for="exchange1">Только обмен</label>
                      </div>
                      <?php endif ?>
                    </div>
                  </div>
                </div>

                <div class="col-xs-12 col-sm-6 col-md-2 col-lg-3">
                  <div id="filtr_btn">
                    <span class="filtr_name"> </span>
                    <input type="submit" name="filtr" value="Применить" class="btn btn-primary btn-md">
                  </div>
                </div>
                </form>

              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="container">
        <ol class="breadcrumb">
          <li><a href="/"><?php echo $city_name; ?></a></li>
          <?php if (isset($_GET['locality'])): ?>
            <li><a href="/<?php echo $_GET['city']."/".$_GET['locality']; ?>"><?php if ($section == 1) { echo "Аренда недвижимости"; } else { echo "Продажа недвижимости"; } ?></a></li>
          <?php else: ?>
          <li><a href="/<?php echo $_GET['city']; ?>"><?php if ($section == 1) { echo "Аренда недвижимости"; } else { echo "Продажа недвижимости"; } ?></a></li>
          <?php endif ?>
          <li class="active"><?php echo $cut_name; ?></li>
        </ol>
      </div>

      <div class="ads_list">
        <div class="container">
          <div class="row">
            <div id="ads_list" class="col-xs-12">
              <?php
                $monthes = array(
                  1 => 'Января', 2 => 'Февраля', 3 => 'Марта', 4 => 'Апреля',
                  5 => 'Мая', 6 => 'Июня', 7 => 'Июля', 8 => 'Августа',
                  9 => 'Сентября', 10 => 'Октября', 11 => 'Ноября', 12 => 'Декабря'
                );
                if ($locality_id <> ""){
                  if (isset($_POST['filtr'])) {
                    $adverts_query  = "SELECT * FROM `adverts` WHERE `city_id`='$city_id' AND `section_id`='$section'  AND `category_id`='$cut_id' AND `moderate`=1 AND `selest_locality` = $locality_id $filtr_query ORDER BY `$sort_by` DESC LIMIT $limit";
                  }else{
                    $adverts_query  = "SELECT * FROM `adverts` WHERE `city_id`='$city_id' AND `section_id`='$section'  AND `category_id`='$cut_id' AND `moderate`=1 AND `selest_locality` = $locality_id ORDER BY `id` DESC LIMIT $limit";
                  }
                }else{
                  if (isset($_POST['filtr'])) {
                    $adverts_query  = "SELECT * FROM `adverts` WHERE `city_id`='$city_id' AND `section_id`='$section'  AND `category_id`='$cut_id' AND `moderate`=1 $filtr_query ORDER BY `$sort_by` DESC LIMIT $limit";
                  }else{
                    $adverts_query  = "SELECT * FROM `adverts` WHERE `city_id`='$city_id' AND `section_id`='$section'  AND `category_id`='$cut_id' AND `moderate`=1 ORDER BY `id` DESC LIMIT $limit";
                  }
                }
                $adverts = mysql_query($adverts_query) or die (mysql_error());
                if (mysql_num_rows($adverts)>0) {
                  while($adverts_row = mysql_fetch_array($adverts, MYSQL_ASSOC)){
                    $locality_id = $adverts_row['selest_locality'];
                    $advert_id = $adverts_row['id'];
                    $locality_result = mysql_query("select * from `locality` WHERE `id`='$locality_id' and `city_id`='$city_id'")or die(mysql_error());
                    $locality_row = mysql_fetch_array($locality_result);
                    $locality = $locality_row['name'];

                    $adres = $city_name.", ".$locality.", ".$adverts_row['street'].", ".$adverts_row['num'];

                    $ad_photo_result = mysql_query("SELECT url FROM `photos` WHERE `ad_id` = $advert_id ORDER BY id DESC LIMIT 1")or die(mysql_error());
                    $ad_photo_name = mysql_fetch_array($ad_photo_result);
                    $ad_photo = $ad_photo_name['url'];

                    $ad_photo_count_result = mysql_query("SELECT url FROM `photos` WHERE `ad_id` = $advert_id ORDER BY id")or die(mysql_error());
                    $ad_photo_count = mysql_num_rows($ad_photo_count_result);

                    $date = $adverts_row['date'];
                    $date_withdraw = spliti (" ", $date, 2);

                    $current_time = date('j ') . date('n');

                    if ($date == $current_time) {
                      $date_type = 1;
                    }elseif ($date == date('j n', time()-24*3600)) {
                      $date_type = 2;
                    }else{
                      $date_type = 3;
                    }
              ?>
                <div class="ad "><!-- selected -->
                  <div class="row">
                    <?php if ($ad_photo_count > 0): ?>
                      <div class="col-xs-12 col-sm-2">
                        <a href="/<?php echo $_GET['city']."/".$cut_url_name."/ad".$adverts_row['id']; ?>" target="_blank">
                          <div class="photo" style="background-image: url(../img/addadverts/<?php echo $ad_photo; ?>);"><div class="count"><?php echo $ad_photo_count; ?> фото</div></div>
                        </a>
                      </div>
                    <?php endif ?>
                    <div class="col-xs-12 <?php if ($ad_photo_count > 0): ?>col-sm-7 col-md-7<?php else: ?>col-sm-9 col-md-9<?php endif ?>">
                      <div class="deskr">
                        <div class="name"><a href="/<?php echo $_GET['city']."/".$cut_url_name."/ad".$adverts_row['id']; ?>" target="_blank"><?php echo $adres; ?>, собственник</a></div>
                        <div class="text">
                          <?php if (strlen($adverts_row['text']) <= 300) {
                            echo $adverts_row['text'];
                          } else{
                            echo substr($adverts_row['text'], 0, strrpos(substr($adverts_row['text'], 0, 300), ' '))." ...";
                          } ?>
                        </div>
                        <div class="min_info">
                          <?php if ($date_type == 1): ?>
                            <div class="date today">Сегодня</div>
                          <?php elseif ($date_type == 2): ?>
                            <div class="date yesterday">Вчера</div>
                          <?php elseif ($date_type == 3): ?>
                            <div class="date"><?php echo $date_withdraw[0]." ".$monthes[$date_withdraw[1]]; ?></div>
                          <?php endif ?>
                          <div class="district"><?php echo $locality; ?></div>
                          <div class="size"><?php echo $adverts_row['size']; ?> кв. м</div>
                        </div>
                      </div>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3">
                      <?php if ($adverts_row['section_id'] == 1): ?>
                        <?php 
                          if ($adverts_row['srok'] == "long") {
                            $srok = "/месяц";
                          }else{
                            $srok = "/сутки";
                          }
                        ?>
                      <?php endif ?>
                      <div class="price"><?php echo number_format($adverts_row['price'], 0, '.', ' '); ?> руб.<?php echo $srok; ?></div>
                      <?php if ($adverts_row['section_id'] == 2): ?>
                      <div class="kvm_price"><?php echo number_format(round($adverts_row['price']/$adverts_row['size']), 0, '.', ' '); ?> тыс. руб./м²</div>
                      <?php endif ?>
                    </div>
                  </div>
                </div>
              <?php
                  }
                }else{
              ?>
                <div class="alert alert-info" role="alert">К сожаления объявлений нет!</div>
              <?php
                }
              ?>

            </div>
          </div>
        </div>
      </div>
      <div class="text-center">
        <button type="button" id="ads_list-load" class="btn btn-primary btn-lg">Загрузить еще</button>
      </div>

    </div>
<?php
  include 'footer.php';
?>