<?php
    include 'header.php';

    $limit = 12; //СКОЛЬКО ЗАПИСЕЙ ПОДГРУЖАТЬ
  
    $city_url_name = $_GET['city'];
    $city_result = mysql_query("select * from `city` WHERE `url_name`='$city_url_name'")or die(mysql_error());
    $city_row = mysql_fetch_array($city_result);
    $city_name = $city_row['name'];
    $city_id = $city_row['id'];
    
    if ($city_name == "") {
        header("Location: /");
    }

    $news_query  = "SELECT * FROM `articles` WHERE `city_id` = $city_id or `city_id` = 0  ORDER BY `id` DESC LIMIT $limit";
    $news = mysql_query($news_query);

  printf('
    <input type="hidden" id="city_id" value="'.$city_id.'">
    <input type="hidden" id="limit_ajax" value="'.$limit.'">');

  $title_page = $city_name." - Новости";
?>
    <title><?php echo $site_name." ".$title_delimiter." ".$title_page; ?></title>
    <script type="text/javascript" src="/ajax/js/load_news_page.js"></script>

    <div id="articles_news_list">
        <div class="container">
    
          <ol class="breadcrumb">
            <li><a href="/">Главная</a></li>
            <li class="active">Статьи</li>
          </ol>

          <div class="row">
            <div class="col-xs-12">
              <div class="page-header">
                <h2>Наши cтатьи</h2>
              </div>
            </div>
          </div>
          <div id="news_list" class="row-flex">

            <?php 
            if (mysql_num_rows($news)>0) {
              while($news_row = mysql_fetch_array($news, MYSQL_ASSOC)){ 
            ?>
              <div class="col-sm-6 col-md-3 col-lg-3">
                <a href="/articles/<?php echo $city_url_name; ?>/art<?php echo $news_row['id']; ?>">
                  <div class="thumbnail">
                    <?php 
                      preg_match_all('/<img[^>]+>/i',$news_row['text'], $poster); 
                      print_r($poster[0][0]);
                    ?>

                      <div class="caption">
                        <h3><?php echo $news_row['name']; ?></h3>
                        <p><?php if (strlen($news_row['text']) <= 200) {
                              echo strip_tags($news_row['text']);
                            } else{
                              echo substr(strip_tags($news_row['text']), 0, strrpos(substr(strip_tags($news_row['text']), 0, 200), ' '))." ...";
                            } ?></p>
                      </div>
                  </div>
                  </a>
              </div>
            <?php 
              }
            } else{ 
            ?>
          <div class="alert alert-info" role="alert">К сожаления статей нет!</div>
          <?php } ?>

          </div>

          <div class="text-center">
          <button type="button" id="news_list-load" class="btn btn-primary btn-lg">Загрузить еще</button>
      </div>

        </div>
    </div>
<?php
  include 'footer.php';
?>