<?php 
    function upload_boofer($file, $i) {
        $uploaddir = $_SERVER['DOCUMENT_ROOT'].'/img/boofer/';
        $filename = $file['name'][$i];
        $ext = substr(strrchr($filename, '.'), 1);
        $new_name = md5(date("F j, Y, h:m s").''.$filename).".".$ext; 
        $filename = $new_name; 
        $uploadfile = $uploaddir . basename($filename);
        move_uploaded_file($file['tmp_name'][$i], $uploadfile);

        return $filename;
    }

    function is_image($filename) {
        $is = @getimagesize($filename);
        if ( !$is ) {
            return 0;
        } elseif ( !in_array($is[2], array(1,2,3)) ) {
            return 0;
        } else {
            return 1;
        }
    }

    function crop_image($filename_boofer, $dir){
        $boofer_file = $_SERVER['DOCUMENT_ROOT'].'/img/boofer/'.$filename_boofer;
        $file_name = $boofer_file;

        //проверяем на изображение
        $is_image = is_image($file_name);

        if ($is_image == 1){
            //ресайзим и обрезаем 
            require_once 'AcImage/AcImage.php';
            $ext = substr(strrchr($filename_boofer, '.'), 1);
            $new_name = md5(date("F j, Y, h:m s").''.$file_name).".".$ext;

            $savePath = $_SERVER['DOCUMENT_ROOT'].'/img/'.$dir.'/'.$new_name;
            $filePath = $boofer_file;        
            $image = AcImage::createImage($filePath);
            
            AcImage::setQuality(100);

            $new_width = 1200;

            list($width, $height) = getimagesize($file_name);
            
            $image->resizeByWidth($new_width);

            $image
                ->save($savePath);
        }

        unlink($boofer_file);

        return $new_name;
    }

    function add_advert($city, $section, $category, $selest_locality, $district_name, $street, $num, $etazh, $etazhnost, $mat, $size, $price, $srok, $exchange, $agent, $infra, $text, $photos, $phone, $contact_name, $pass){
        //ЗАНОСИМ ВСЕ ДАННЫЕ В БД
        $result = mysql_query ("INSERT INTO `adverts` (`city_id`, `section_id`, `category_id`, `selest_locality`, `district_name`, `street`, `num`, `etazh`, `etazhnost`, `mat`, `size`, `price`, `srok`, `exchange`, `agent`, `infra`, `text`, `phone`, `contact_name`, `pass`, `moderate`) VALUES ('$city', '$section', '$category', '$selest_locality', '$district_name', '$street', '$num', '$etazh', '$etazhnost', '$mat', '$size', '$price', '$srok', '$exchange', '$agent', '$infra', '$text', '$phone', '$contact_name', '$pass', '0')");

        //ЗАНОСИМ ФОТОГРАФИИ
        $ad_id_result = mysql_query("SELECT id FROM `adverts` ORDER BY id DESC LIMIT 1")or die(mysql_error());
        $ad_id_name = mysql_fetch_array($ad_id_result);
        $ad_id = $ad_id_name['id'];

        if ($photos <> 0) {
            for ($i = 0; $i < count($photos['name']); $i++) {
                $upload_file = $photos;
                $dir = 'addadverts';
                $new_name_file_boofer = upload_boofer($upload_file, $i);
                $new_file = crop_image($new_name_file_boofer, $dir);

                $result = mysql_query ("INSERT INTO `photos` (`ad_id`, `url`) VALUES ('$ad_id', '$new_file')");
            }
        }
        
        $city_result = mysql_query("select * from `city` WHERE `id`='$city'")or die(mysql_error());
        $city_name = mysql_fetch_array($city_result);
        $city_url_name = $city_name['url_name'];

        $cut_result = mysql_query("select * from `categories` WHERE `id`='$category'")or die(mysql_error());
        $cut_name = mysql_fetch_array($cut_result);
        $cut_url_name = $cut_name['url_name'];

        header("Location: /".$city_url_name."/".$cut_url_name."/ad".$ad_id);
    }

    function check_pass($password, $advert_id){
        $advert_query  = "SELECT * FROM `adverts` WHERE `id` = $advert_id";
        $advert = mysql_query($advert_query);
        $advert_row = mysql_fetch_array($advert);
        
        if ($advert_row['pass'] == $password) {
            $result = 1;
        }else{
            $result = 0;
        }
        return $result;
    }

    function stop_ad($id){
        $url = $_SERVER['REQUEST_URI'];
        $result = mysql_query("UPDATE `adverts` SET `moderate`= '2' WHERE `id` = $id") or die(mysql_error());

        header("Location: ".$url);
    }

    function start_ad($id){
        $url = $_SERVER['REQUEST_URI'];
        $result = mysql_query("UPDATE `adverts` SET `moderate`= '3' WHERE `id` = $id") or die(mysql_error());

        header("Location: ".$url);
    }

    function edit_ad($moderate, $id, $city, $section, $category, $selest_locality, $street, $num, $etazh, $etazhnost, $mat, $size, $price, $srok, $exchange, $agent, $infra, $text, $phone, $contact_name){
        //определяем координаты адреса
        $city_result = mysql_query("select * from `city` WHERE `id`='$city'")or die(mysql_error());
        $city_name = mysql_fetch_array($city_result);
        $city_name = $city_name['name'];
        if ($district_name <> "") {
            $locality = $district_name;
        }else{
            $locality = $selest_locality;
            $locality_result = mysql_query("select * from `locality` WHERE `id`='$selest_locality'")or die(mysql_error());
            $locality_name = mysql_fetch_array($locality_result);
            $locality = $locality_name['name'];
        }
        
        //Составляем текстовый адрес
        $geocode = $city_name.", ".$locality.", ".$street.", ".$num;

        //получаем долготу и широту
        $params = array(
            'geocode' => $geocode, // адрес
            'format'  => 'json',                          // формат ответа
            'results' => 1,                               // количество выводимых результатов
            'key'     => 'AGLYD1sBAAAAzK3EYgMAeGlkXt3PSADkq3s2bEHuagIXIf4AAAAAAAAAAAB-bYWzsFD_GBmpDivWjaJJv16bcQ==',                           // ваш api key
        );
        $response = json_decode(file_get_contents('http://geocode-maps.yandex.ru/1.x/?' . http_build_query($params, '', '&')));
         
        if ($response->response->GeoObjectCollection->metaDataProperty->GeocoderResponseMetaData->found > 0) {
            $position_geo = $response->response->GeoObjectCollection->featureMember[0]->GeoObject->Point->pos;
            $position_geo_array = explode(" ", $position_geo);
        } else {
            $position_geo_array[1] = 0;
            $position_geo_array[0] = 0;
        }

        $kvm_price = round($price/$size);

        $result = mysql_query("UPDATE `adverts` SET `city_id` = '$city', `section_id` = '$section', `category_id` = '$category', `selest_locality` = '$selest_locality', `district_name` = '$district_name', `street` = '$street', `num` = '$num', `etazh` = '$etazh', `etazhnost` = '$etazhnost', `mat` = '$mat', `size` = '$size', `price` = '$price', `kvm_price` = '$kvm_price', `srok` = '$srok', `exchange` = '$exchange', `agent` = '$agent', `infra` = '$infra', `text` = '$text', `phone` = '$phone', `contact_name` = '$contact_name', `position_0` = '$position_geo_array[0]', `position_1` = '$position_geo_array[1]', `moderate`= '$moderate' WHERE `id` = $id") or die(mysql_error());

        $url = $_SERVER['REQUEST_URI'];
        header("Location: ".$url);
    }

    function check_code($code, $cut_id){
        $code_query  = "SELECT * FROM `code` WHERE `code` = '$code' LIMIT 1";
        $code = mysql_query($code_query);

        $code_row = mysql_fetch_array($code);

        $start_date = $code_row['start_date'];
        $tarif_id = $code_row['tarif'];


        $tarif = mysql_query("SELECT * FROM `tarif` WHERE `id` = $tarif_id");
        $tarif_row = mysql_fetch_array($tarif);
        $srok = (int)$tarif_row['srok'] * 24 * 3600;

        $end_date = date('d-n-Y',(strtotime($start_date)+$srok));
        $reat_date = date('j-n-Y');

        // echo date('d n Y',strtotime($end_date))." - ".date('d n Y',strtotime($reat_date));
        if ((strtotime($end_date) > strtotime($reat_date))) {
            if ($code_row['code'] <> "") {
                $cuts = explode(",", $code_row['cut']);

                for ($i=0; $i < count($cuts); $i++) { 
                    if ($cuts[$i] == $cut_id) {
                        $result = 1;
                    }
                }

                if ($result <> 1) {
                    $result = 2;
                }
            }else{
                $result = 0;
            }
        }else{
           $del_code = mysql_query ("DELETE FROM `code` WHERE code = $code"); 
           $result = 3;
        }

        return $result;
    }

    function generate_code(){
        $code = rand(0, 99999999);
        $code_l = strlen($code);
        if ($code_l < 8) {
            $l = 8 - $code_l;
            for ($i=0; $i < $l; $i++) { 
                $code = "0".$code;
            }
        }

        return $code;
    }

    function select_cut($categories){
        for ($i=0; $i < count($categories); $i++) { 
            if ($i == 0) {
              $cut = $categories[$i];
            }else{
              $cut .= ",".$categories[$i];
            }
        }

        return $cut;
    }

    function add_generate_code($code, $cut, $tarif){
        $start_date = date('j.').date('n').date('.Y');

        $result = mysql_query ("INSERT INTO `code` (`code`, `cut`, `start_date`, `tarif`) VALUES ('$code', '$cut', '$start_date', '$tarif')");
    }

    function find_code($code){
        $code_query  = "SELECT COUNT(1) FROM `code` WHERE `code` = '$code'";
        $code = mysql_query($code_query);
        $code_row = mysql_fetch_array($code);

        return $code_row[0];
    }
?>