<?php 
  include 'config.php';
  include 'functions.php';

  if (isset($_GET['city'])) {
    $select_city = $_GET['city'];

    $city_result = mysql_query("select * from `city` WHERE `url_name`='$select_city'")or die(mysql_error());
    $city_row = mysql_fetch_array($city_result);
    $city_name = $city_row['name'];
  }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name = "description" content = "<?php echo $metadeskription; ?>"/>
    <meta name = "keywords" content = "<?php echo $metakey; ?>" />
    <meta name = "robots" content = "index,follow"/>

    <!-- Bootstrap -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/main.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

    <noscript>
      <style type="text/css">
        body *{
          display: none!important;
        }
        #js-off, #js-off *{
          display: block!important;
        }
      </style>
      <div id="js-off">
        <div class="container">
          <div class="row">
            <div class="col-xs-12">
              <div class="alert alert-danger">Включите JavaScript в настройках Вашего браузера!</div>
            </div>
          </div>
        </div>
      </div>
    </noscript>
  </head>
  <body>
    <?php if (($_SERVER['REQUEST_URI']=="") or ($_SERVER['REQUEST_URI']=="/") or ($_SERVER['REQUEST_URI']=="/index.php")): ?>
    <?php else: ?>
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">

          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-main" aria-expanded="false">
            <span class=sr-only>Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/">
            <?php if (($_SERVER['REQUEST_URI']=="") or ($_SERVER['REQUEST_URI']=="/") or ($_SERVER['REQUEST_URI']=="/index.php")): ?>
              AuBa
            <?php else: ?>
              <span class="name">AuBa</span>
              <?php if (($_SERVER['REQUEST_URI']=="/buy_code") or ($_SERVER['REQUEST_URI']=="/success_pay.php") or ($_SERVER['REQUEST_URI']=="/fail_pay.php")): ?>
              <span class="city">Оплата</span>
              <?php else: ?>
              <span class="city"><?php echo $city_name; ?></span>
              <?php endif ?>
            <?php endif ?>
          </a>
          <div class="visible-xs deskr_logo">
            <span>Продажа и аренда</span>
            <span>недвижимости</span>
          </div>
        </div>

        <div class="collapse navbar-collapse" id="navbar-main">
          <ul class="nav navbar-nav">
            <li>
              <a href="/<?php echo $select_city; ?>">Аренда и продажа недвижимост</a>
            </li>
            <li>
              <a href="/news/<?php echo $select_city; ?>">Новости</a>
            </li>
            <li>
              <a href="/articles/<?php echo $select_city; ?>">Статьи</a>
            </li>
          </ul>
          <?php if (($_SERVER['REQUEST_URI']=="") or ($_SERVER['REQUEST_URI']=="/") or ($_SERVER['REQUEST_URI']=="/index.php")): ?>
          <?php else: ?>
          <div class="navbar-right">
            <a href="/addadvert/" class="btn btn-primary navbar-btn">Разместить объявление <i class="glyphicon glyphicon-plus"></i></a>
          </div>
          <?php endif ?>
        </div>

      </div>
    </nav>
    <?php endif ?>

    <?php if (($_SERVER['REQUEST_URI']=="") or ($_SERVER['REQUEST_URI']=="/") or ($_SERVER['REQUEST_URI']=="/index.php")): ?>
    <?php else: ?>
      <div class="container-fluid">
        <div class="row">
          <div id="add_mobile" class="visible-xs add_mobile">
            <a href="/addadvert/">
              <div>
                Разместить объявление <span class="glyphicon glyphicon-plus"></span>
              </div>
            </a>
          </div>
          <div id="search_mobile" class="visible-xs visible-sm search_mobile">
            <div>
              Поиск <span class="glyphicon glyphicon-search"></span>
            </div>
          </div>
        </div>
      </div>
    <?php endif ?>

    <div id="header_search" <?php if (($_SERVER['REQUEST_URI']=="") or ($_SERVER['REQUEST_URI']=="/") or ($_SERVER['REQUEST_URI']=="/index.php")){echo "class=''";} else {echo "class='no_index'";} ?>>
      <div class="form-group">
        <div class="container">
          <div class="header_form">

            <div class="row">
              <form action="/search/" method="POST">
                <div class="col-xs-12 col-sm-6 col-md-2">
                  <label for="">Город/регион</label>
                  <select class="form-control" name="city_id">
                    <?php
                    $city_query  = "SELECT * FROM `city` ORDER BY `id`";
                    $city = mysql_query($city_query);

                    while($city_row = mysql_fetch_array($city, MYSQL_ASSOC)){    
                        $id = stripslashes($city_row['id']);   
                    ?>
                    <option value="<?php echo $city_row['id']; ?>"><?php echo $city_row['name']; ?></option>
                    <?php } ?>
                  </select>
                </div>

                <div class="col-xs-12 col-sm-6 col-md-4">
                  <label for="selest_district">Категория</label>
                  <select id="selest_district" class="form-control" name="cut_id">
                    <?php
                    $sections_query  = "SELECT * FROM `sections` ORDER BY `id`";
                    $sections = mysql_query($sections_query);

                    while($sections_row = mysql_fetch_array($sections, MYSQL_ASSOC)){    
                        $id = stripslashes($sections_row['id']);   
                    ?>
                    <optgroup label="<?php echo $sections_row['name']; ?>">
                      <?php
                      $categories_query  = "SELECT * FROM `categories` WHERE `section_id` = $id ORDER BY `ordinal`";
                      $categories = mysql_query($categories_query);

                      while($categories_row = mysql_fetch_array($categories, MYSQL_ASSOC)){ 
                      ?>
                      <option value="<?php echo $categories_row['id']; ?>"><?php echo $categories_row['cut_name'];; ?></option>
                      <?php } ?>
                    </optgroup>
                    <?php } ?>
                  </select>
                </div>

                <div class="col-xs-12 col-sm-6 col-md-2">
                  <label for="">Цена</label>
                  <div class="input-group">
                    <span class="input-group-addon">₽</span>
                    <input type="text" class="form-control only_num" placeholder="Цена от" name="min_price">
                  </div>
                </div>

                <div class="col-xs-12 col-sm-6 col-md-2">
                  <label for=""></label>
                  <div class="input-group">
                    <span class="input-group-addon">₽</span>
                    <input type="text" class="form-control only_num" placeholder="до" name="max_price">
                  </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-2">
                  <label for=""></label>
                  <button type="submit" class="btn btn-success">Поиск</button>
                </div>
              </form>
            </div>
          </div>
        </div>
        
      </div>
      <script>
        $('.only_num').bind("change keyup input click", function() {
          if (this.value.match(/[^0-9]/g)) {
            this.value = this.value.replace(/[^0-9]/g, '');
          }
        });
      </script>
    </div>