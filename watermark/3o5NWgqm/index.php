<?php 
  	include 'header.php';

  	if (isset($_POST['save_config'])) {
  		$sitename = $_POST['sitename'];
  		$metakey = $_POST['metakey'];
  		$metadeskription = $_POST['metadeskription'];

  		update_config($sitename, $metakey, $metadeskription);

  		// проверяем, можно ли загружать изображение
	    $check = can_upload($_FILES['file']);
	    
	    if($check === true){
	    	// загружаем изображение на сервер
	    	make_upload($_FILES['file']);
	    }
  	}

    if (isset($_POST['add_key'])) {
      $code = generate_code();
      $selected_cut = select_cut($_POST['categories']);
      $tarif = $_POST['tarif'];

      add_generate_code($code, $selected_cut, $tarif);
    }
    

  	print_msg($msg);
?>

	<?php if ($_SESSION['role'] == 1): ?>
    
    <?php if (isset($code)): ?>
      <h1>Код доступа - <span class="label label-default"><?php echo $code; ?></span></h1>
    <?php endif ?>

    <div class="page-header">
      <h2>Сгенерировать код</h2>
    </div>
    <form action="" enctype="multipart/form-data" method="post">
      <div class="form-group">
        <div class="checkbox_group_scroll">
          
            <?php
            $sections_query  = "SELECT * FROM `sections` ORDER BY `id`";
            $sections = mysql_query($sections_query);

            while($sections_row = mysql_fetch_array($sections, MYSQL_ASSOC)){    
                $id = stripslashes($sections_row['id']);   
            ?>
            <div class="col-xs-12 col-sm-6">
              <label><?php echo $sections_row['name']; ?></label>
              <?php
              $categories_query  = "SELECT * FROM `categories` WHERE `section_id` = $id ORDER BY `ordinal`";
              $categories = mysql_query($categories_query);

              while($categories_row = mysql_fetch_array($categories, MYSQL_ASSOC)){ 
              ?>
                <div class="checkbox">
                  <label><input type="checkbox" name="categories[]" value="<?php echo $categories_row['id']; ?>"><?php echo $categories_row['cut_name'];; ?></label>
                </div>
              <?php } ?>
            </div>
            <?php } ?>
        </div>
      </div>
      <div class="form-group">
        <label>Тариф</label>
        <select class="form-control" name="tarif">
          <?php
            $tarifs = mysql_query("SELECT * FROM `tarif` ORDER BY `id`");

            while($tarifs_row = mysql_fetch_array($tarifs, MYSQL_ASSOC)){  
          ?>
            <option value="<?php echo $tarifs_row['id']; ?>"><?php echo $tarifs_row['name']; ?></option>
          <?php } ?>
        </select>
      </div>
      <div class="form-group">
        <input type="submit" class="btn btn-success" name="add_key" value="Сгенирировать ключ доступа">
      </div>
    </form>

		<?php
		    $sitename = mysql_query("SELECT * FROM `config` WHERE `name` = 'sitename'");
		    $sitename_row = mysql_fetch_array($sitename);
		    $sitename_name = $sitename_row['content'];

		    $metakey = mysql_query("SELECT * FROM `config` WHERE `name` = 'metakey'");
		    $metakey_row = mysql_fetch_array($metakey);
		    $metakey_name = $metakey_row['content'];

		    $metadeskription = mysql_query("SELECT * FROM `config` WHERE `name` = 'metadeskription'");
		    $metadeskription_row = mysql_fetch_array($metadeskription);
		    $metadeskription_name = $metadeskription_row['content'];
		?>
    <div class="page-header">
      <h2>Общие настройки</h2>
    </div>
		<form action="" enctype="multipart/form-data" method="post">
			<div class="form-group">
        <label>Site name</label>
        <input type="text" name="sitename" class="form-control" value="<?php echo $sitename_name; ?>">
      </div>
			<div class="form-group">
        <label>Meta key</label>
        <input type="text" name="metakey" class="form-control" value="<?php echo $metakey_name; ?>">
      </div>
      <div class="form-group">
        <label>Meta deskription</label>
        <input type="text" name="metadeskription" class="form-control" value="<?php echo $metadeskription_name; ?>">
      </div>
      <div class="form-group">
      	<label>Фоновое изображение</label>
      	<input type="file" class="form-control" name="file">
      </div>
      <div class="form-group">
      	<input type="submit" class="btn btn-success" name="save_config" value="Сохранить настройки">
      </div>
		</form>
	<?php endif ?>

<?php 
  include 'footer.php';
?>