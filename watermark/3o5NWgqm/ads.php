<?php 
  	include 'header.php';

  	if (isset($_POST['mod1'])) {
        $city = strip_tags($_POST['city']);
        $section = strip_tags($_POST['section']);
        $category = strip_tags($_POST['category']);
        $selest_locality = strip_tags($_POST['selest_locality']);
        $type = strip_tags($_POST['type']);
        $district_name = strip_tags($_POST['district_name']);
        $street = strip_tags($_POST['street']);
        $num = strip_tags($_POST['num']);
        $etazh = strip_tags($_POST['etazh']);
        $etazhnost = strip_tags($_POST['etazhnost']);
        $mat = strip_tags($_POST['mat']);
        $size = strip_tags($_POST['size']);
        $price = strip_tags($_POST['price']);
        $srok = strip_tags($_POST['srok']);
        $exchange = strip_tags($_POST['exchange']);
        $agent = strip_tags($_POST['agent']);
        $infra = strip_tags($_POST['infra']);
        $text = strip_tags($_POST['text']);
        $phone = strip_tags($_POST['phone']);
        $contact_name = strip_tags($_POST['contact_name']);

        if ($selest_locality == "-") {
            add_locality($city, $type, $district_name);
            $locality_id_result = mysql_query("SELECT id FROM `locality` ORDER BY id DESC LIMIT 1")or die(mysql_error());
            $locality_id_name = mysql_fetch_array($locality_id_result);
            $selest_locality = $locality_id_name['id'];
        }

        $moderate = 1;
        $id = $_POST['id'];

        moderate($moderate, $id, $city, $section, $category, $selest_locality, $street, $num, $etazh, $etazhnost, $mat, $size, $price, $srok, $exchange, $agent, $infra, $text, $phone, $contact_name);
    }

  	if (isset($_POST['find_ad'])) {
	  	$ad_num = $_POST['ad_num'];
	  	$find_ad = find_ad($ad_num);

	  	if ((($_SESSION['city'] == 0) or ($find_ad['city_id'] == $_SESSION['city'])) and ($find_ad['city_id'] <> "")) {
	  		$result = 1;
	  		$city_id = $find_ad['city_id'];
	  		$section = $find_ad['section_id'];
	  	}else{
	  		$msg['bad'] = "<strong>Внимание!</strong> Объявление не найдено. Возможно оно размещено не в вашем регионе.";
	  	}
  	}

  print_msg($msg);
?>
	<div class="page-header">
    <h2>Найти объявление</h2>
  </div>
	<form action="" method="POST">
		<div class="col-xs-12 col-md-9">
	      	<div class="form-group">
	        	<input type="text" class="form-control" name="ad_num" placeholder="Введите номер объявления">
	      	</div>
	    </div>
	    <div class="col-xs-12 col-md-3">
	      	<input type="submit" class="btn btn-primary" value="Найти" name="find_ad">
	    </div>
	</form>

	<?php if ($result == 1): ?>

	<div class="col-xs-12">
		<div class="panel panel-primary">
		    <div class="panel-heading">
		      <h3 class="panel-title">
	            <b># <?php echo $find_ad['id']; ?></b>
	          </h3>
	          <a  href="#editAdvert<?php echo $find_ad['id']; ?>" class="edit_btn" data-toggle="modal">Перейти к проверке</a>
		    </div>
		 </div>
	</div>
	
	

    <div id="editAdvert<?php echo $find_ad['id']; ?>" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
            <!-- Заголовок модального окна -->
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
              <h4 class="modal-title">Просмотр объявления #<?php echo $find_ad['id']; ?></h4>
            </div>
            <!-- Основное содержимое модального окна -->
            <form action="" method="post">
                <div class="modal-body">
              <fieldset>
                <input type="hidden" name="city" value="<?php echo $city_id; ?>">
                <input type="hidden" name="section" value="<?php echo $section; ?>">

                <div class="form-group" name="cutegory">
                  <label>Категория объявления:</label>

                  <select class="form-control" name="category">
                    <?php
                    $sections_query  = "SELECT * FROM `sections` ORDER BY `id`";
                    $sections = mysql_query($sections_query);

                    while($sections_row = mysql_fetch_array($sections, MYSQL_ASSOC)){    
                        $id = stripslashes($sections_row['id']);   
                    ?>
                    <optgroup label="<?php echo $sections_row['name']; ?>">
                      <?php
                      $categories_query  = "SELECT * FROM `categories` WHERE `section_id` = $id ORDER BY `ordinal`";
                      $categories = mysql_query($categories_query);

                      while($categories_row = mysql_fetch_array($categories, MYSQL_ASSOC)){ 
                      ?>
                      <option value="<?php echo $categories_row['id']; ?>" <?php if ($find_ad['category_id'] == $categories_row['id']) { echo "selected"; } ?>><?php echo $categories_row['cut_name'];; ?></option>
                      <?php } ?>
                    </optgroup>
                    <?php } ?>
                  </select>
                </div>

                <div class="form-group">
                  <label for="name">Населенный пункт:</label>

                  <select id="selest_locality" name="selest_locality" class="form-control" required>
                    <?php
                      $type[1][1] = "Районы города";
                      $type[1][2] = "Пригород";
                      $type[1][3] = "Область";
                      $type[2][1] = 'none';
                      $type[2][2] = '#fff0b7';
                      $type[2][3] = '#d2f2b6';

                      $city_query = mysql_query("SELECT * FROM `city` WHERE `id`='$city_id'");
                      $city_query_row = mysql_fetch_array($city_query);
                    ?>
                    <?php
                        $locality_query  = "SELECT * FROM `locality` WHERE `city_id` = $city_id  ORDER BY `type`";
                        $locality = mysql_query($locality_query);

                        $i = 1;
                        while($locality_row = mysql_fetch_array($locality, MYSQL_ASSOC)){
                          $optgroup = $locality_row['type'];
                          if ($i == 1) { $last_optgroup = $locality_row['type']; }
                      ?>

                        <?php if (($i == 1) or ($optgroup <> $last_optgroup)): ?>
                          <optgroup label="<?php echo $type[1][$optgroup]; ?>" style="background: <?php echo $type[2][$optgroup]; ?>">
                          <?php $last_optgroup = $optgroup; ?>
                        <?php endif ?>

                          <option value="<?php echo $locality_row['id']; ?>" <?php if ($locality_row['id'] == $find_ad['selest_locality']) { echo "selected"; } ?>><?php echo $locality_row['name']; ?></option>

                        <?php if (($i <> 1) & ($optgroup <> $last_optgroup)): ?>
                          </optgroup>
                          <?php $last_optgroup = $last_optgroup + 1; ?>
                        <?php endif ?>

                      <?php 
                          $last_optgroup = $optgroup;
                          $i++;
                        }
                      ?>
                      </optgroup>

                    <option disabled></option>
                    <option value="-" <?php if ($find_ad['district_name'] <> ""){ echo "selected";} ?>>- Не относится ни к одному из указанных районов -</option>
                  </select>
                  <script>
                    $("#selest_locality").on('change', function(){
                      if($(this).val() == "-"){
                        $("#district_name").show();
                        $('#district_name_input').prop('required', true);
                      } else {
                        $("#district_name").hide();
                        $('#district_name_input').prop('required', false);
                      }
                    })
                  </script>
                </div>
                <div id="district_name" class="row" <?php if ($find_ad['district_name'] <> ""): ?>style="display: block;"<?php else: ?>style="display: none;"<?php endif ?>>
                    <div class="form-group col-xs-12 col-sm-6 col-md-4 col-lg-5">
                        <select id="selest_district" name="type" class="form-control" required="">
                            <option value="1">Район города (микрорайон города)</option>
                            <option value="2" style="background: #fff0b7;">Пригород</option>
                            <option value="3" style="background: #d2f2b6;">Область</option>
                        </select>
                    </div>
                    <div class="form-group col-xs-12 col-sm-6 col-md-8 col-lg-7">
                        <input id="district_name_input" type="text" name="district_name" class="form-control" value="<?php echo $find_ad['district_name']; ?>" placeholder="Введите имя населенного пункта">
                    </div>
                </div>
                
                
                <div class="row">
                  <div class="form-group col-xs-12 col-sm-6 col-md-8 col-lg-9">
                    <label>Улица</label>
                    <input type="text" name="street" class="form-control" value="<?php echo $find_ad['street']; ?>" required>
                  </div>

                  <div class="form-group col-xs-12 col-sm-6 col-md-4 col-lg-3">
                    <label>№ дома</label>
                    <input type="text" name="num" class="form-control" value="<?php echo $find_ad['num']; ?>" required>
                  </div>
                </div>

                <hr>

                <div class="row">
                  <div class="form-group col-xs-6 col-sm-4">
                    <label for="etazh">Этаж:</label>
                    <select name="etazh" id="etazh" class="form-control" required>
                      <option value="-">-</option>
                      <option value="0" <?php if ($find_ad['etazh'] == 0) { echo "selected"; } ?>>цокольный</option>
                      <option value="1" <?php if ($find_ad['etazh'] == 1) { echo "selected"; } ?>>1</option>
                      <option value="2" <?php if ($find_ad['etazh'] == 2) { echo "selected"; } ?>>2</option>
                      <option value="3" <?php if ($find_ad['etazh'] == 3) { echo "selected"; } ?>>3</option>
                      <option value="4" <?php if ($find_ad['etazh'] == 4) { echo "selected"; } ?>>4</option>
                      <option value="5" <?php if ($find_ad['etazh'] == 5) { echo "selected"; } ?>>5</option>
                      <option value="6" <?php if ($find_ad['etazh'] == 6) { echo "selected"; } ?>>6</option>
                      <option value="7" <?php if ($find_ad['etazh'] == 7) { echo "selected"; } ?>>7</option>
                      <option value="8" <?php if ($find_ad['etazh'] == 8) { echo "selected"; } ?>>8</option>
                      <option value="9" <?php if ($find_ad['etazh'] == 9) { echo "selected"; } ?>>9</option>
                      <option value="10" <?php if ($find_ad['etazh'] == 10) { echo "selected"; } ?>>10</option>
                      <option value="11" <?php if ($find_ad['etazh'] == 11) { echo "selected"; } ?>>11</option>
                      <option value="12" <?php if ($find_ad['etazh'] == 12) { echo "selected"; } ?>>12</option>
                      <option value="13" <?php if ($find_ad['etazh'] == 13) { echo "selected"; } ?>>13</option>
                      <option value="14" <?php if ($find_ad['etazh'] == 14) { echo "selected"; } ?>>14</option>
                      <option value="15" <?php if ($find_ad['etazh'] == 15) { echo "selected"; } ?>>15</option>
                      <option value="16" <?php if ($find_ad['etazh'] == 16) { echo "selected"; } ?>>16</option>
                      <option value="17" <?php if ($find_ad['etazh'] == 17) { echo "selected"; } ?>>17</option>
                      <option value="18" <?php if ($find_ad['etazh'] == 18) { echo "selected"; } ?>>18</option>
                      <option value="19" <?php if ($find_ad['etazh'] == 19) { echo "selected"; } ?>>19</option>
                      <option value="20" <?php if ($find_ad['etazh'] == 20) { echo "selected"; } ?>>20</option>
                      <option value="21" <?php if ($find_ad['etazh'] == 21) { echo "selected"; } ?>>21</option>
                      <option value="22" <?php if ($find_ad['etazh'] == 22) { echo "selected"; } ?>>22</option>
                      <option value="23" <?php if ($find_ad['etazh'] == 23) { echo "selected"; } ?>>23</option>
                      <option value="24" <?php if ($find_ad['etazh'] == 24) { echo "selected"; } ?>>24</option>
                      <option value="25" <?php if ($find_ad['etazh'] == 25) { echo "selected"; } ?>>25</option>
                      <option value="26" <?php if ($find_ad['etazh'] == 26) { echo "selected"; } ?>>26</option>
                      <option value="27" <?php if ($find_ad['etazh'] == 27) { echo "selected"; } ?>>27</option>
                      <option value="28" <?php if ($find_ad['etazh'] == 28) { echo "selected"; } ?>>28</option>
                      <option value="29" <?php if ($find_ad['etazh'] == 29) { echo "selected"; } ?>>29</option>
                      <option value="30" <?php if ($find_ad['etazh'] == 30) { echo "selected"; } ?>>30</option>
                      <option value="31" <?php if ($find_ad['etazh'] == 31) { echo "selected"; } ?>>31</option>
                      <option value="32" <?php if ($find_ad['etazh'] == 32) { echo "selected"; } ?>>32</option>
                      <option value="33" <?php if ($find_ad['etazh'] == 33) { echo "selected"; } ?>>33</option>
                      <option value="34" <?php if ($find_ad['etazh'] == 34) { echo "selected"; } ?>>34</option>
                      <option value="35" <?php if ($find_ad['etazh'] == 35) { echo "selected"; } ?>>35</option>
                      <option value="36" <?php if ($find_ad['etazh'] == 36) { echo "selected"; } ?>>36</option>
                      <option value="37" <?php if ($find_ad['etazh'] == 37) { echo "selected"; } ?>>37</option>
                      <option value="38" <?php if ($find_ad['etazh'] == 38) { echo "selected"; } ?>>38</option>
                      <option value="39" <?php if ($find_ad['etazh'] == 39) { echo "selected"; } ?>>39</option>
                      <option value="40" <?php if ($find_ad['etazh'] == 40) { echo "selected"; } ?>>40</option>
                      <option value="41" <?php if ($find_ad['etazh'] == 41) { echo "selected"; } ?>>41</option>
                      <option value="42" <?php if ($find_ad['etazh'] == 42) { echo "selected"; } ?>>42</option>
                      <option value="43" <?php if ($find_ad['etazh'] == 43) { echo "selected"; } ?>>43</option>
                      <option value="44" <?php if ($find_ad['etazh'] == 44) { echo "selected"; } ?>>44</option>
                      <option value="45" <?php if ($find_ad['etazh'] == 45) { echo "selected"; } ?>>45</option>
                      <option value="46" <?php if ($find_ad['etazh'] == 46) { echo "selected"; } ?>>46</option>
                      <option value="47" <?php if ($find_ad['etazh'] == 47) { echo "selected"; } ?>>47</option>
                      <option value="48" <?php if ($find_ad['etazh'] == 48) { echo "selected"; } ?>>48</option>
                      <option value="49" <?php if ($find_ad['etazh'] == 49) { echo "selected"; } ?>>49</option>
                      <option value="50" <?php if ($find_ad['etazh'] == 50) { echo "selected"; } ?>>50</option>
                      <option value="51" <?php if ($find_ad['etazh'] == 51) { echo "selected"; } ?>>51</option>
                      <option value="52" <?php if ($find_ad['etazh'] == 52) { echo "selected"; } ?>>52</option>
                      <option value="53" <?php if ($find_ad['etazh'] == 53) { echo "selected"; } ?>>53</option>
                      <option value="54" <?php if ($find_ad['etazh'] == 54) { echo "selected"; } ?>>54</option>
                      <option value="55" <?php if ($find_ad['etazh'] == 55) { echo "selected"; } ?>>55</option>
                      <option value="56" <?php if ($find_ad['etazh'] == 56) { echo "selected"; } ?>>56</option>
                      <option value="57" <?php if ($find_ad['etazh'] == 57) { echo "selected"; } ?>>57</option>
                      <option value="58" <?php if ($find_ad['etazh'] == 58) { echo "selected"; } ?>>58</option>
                      <option value="59" <?php if ($find_ad['etazh'] == 59) { echo "selected"; } ?>>59</option>
                      <option value="60" <?php if ($find_ad['etazh'] == 60) { echo "selected"; } ?>>60</option>
                      <option value="61" <?php if ($find_ad['etazh'] == 61) { echo "selected"; } ?>>61</option>
                      <option value="62" <?php if ($find_ad['etazh'] == 62) { echo "selected"; } ?>>62</option>
                      <option value="63" <?php if ($find_ad['etazh'] == 63) { echo "selected"; } ?>>63</option>
                      <option value="64" <?php if ($find_ad['etazh'] == 64) { echo "selected"; } ?>>64</option>
                      <option value="65" <?php if ($find_ad['etazh'] == 65) { echo "selected"; } ?>>65</option>
                      <option value="66" <?php if ($find_ad['etazh'] == 66) { echo "selected"; } ?>>66</option>
                      <option value="67" <?php if ($find_ad['etazh'] == 67) { echo "selected"; } ?>>67</option>
                      <option value="68" <?php if ($find_ad['etazh'] == 68) { echo "selected"; } ?>>68</option>
                      <option value="69" <?php if ($find_ad['etazh'] == 69) { echo "selected"; } ?>>69</option>
                      <option value="70" <?php if ($find_ad['etazh'] == 70) { echo "selected"; } ?>>70</option>
                      <option value="71" <?php if ($find_ad['etazh'] == 71) { echo "selected"; } ?>>71</option>
                      <option value="72" <?php if ($find_ad['etazh'] == 72) { echo "selected"; } ?>>72</option>
                      <option value="73" <?php if ($find_ad['etazh'] == 73) { echo "selected"; } ?>>73</option>
                      <option value="74" <?php if ($find_ad['etazh'] == 74) { echo "selected"; } ?>>74</option>
                      <option value="75" <?php if ($find_ad['etazh'] == 75) { echo "selected"; } ?>>75</option>
                      <option value="76" <?php if ($find_ad['etazh'] == 76) { echo "selected"; } ?>>76</option>
                      <option value="77" <?php if ($find_ad['etazh'] == 77) { echo "selected"; } ?>>77</option>
                      <option value="78" <?php if ($find_ad['etazh'] == 78) { echo "selected"; } ?>>78</option>
                      <option value="79" <?php if ($find_ad['etazh'] == 79) { echo "selected"; } ?>>79</option>
                      <option value="80" <?php if ($find_ad['etazh'] == 80) { echo "selected"; } ?>>80</option>
                      <option value="81" <?php if ($find_ad['etazh'] == 81) { echo "selected"; } ?>>81</option>
                      <option value="82" <?php if ($find_ad['etazh'] == 82) { echo "selected"; } ?>>82</option>
                      <option value="83" <?php if ($find_ad['etazh'] == 83) { echo "selected"; } ?>>83</option>
                      <option value="84" <?php if ($find_ad['etazh'] == 84) { echo "selected"; } ?>>84</option>
                      <option value="85" <?php if ($find_ad['etazh'] == 85) { echo "selected"; } ?>>85</option>
                      <option value="86" <?php if ($find_ad['etazh'] == 86) { echo "selected"; } ?>>86</option>
                      <option value="87" <?php if ($find_ad['etazh'] == 87) { echo "selected"; } ?>>87</option>
                      <option value="88" <?php if ($find_ad['etazh'] == 88) { echo "selected"; } ?>>88</option>
                      <option value="89" <?php if ($find_ad['etazh'] == 89) { echo "selected"; } ?>>89</option>
                      <option value="90" <?php if ($find_ad['etazh'] == 90) { echo "selected"; } ?>>90</option>
                      <option value="91" <?php if ($find_ad['etazh'] == 91) { echo "selected"; } ?>>91</option>
                      <option value="92" <?php if ($find_ad['etazh'] == 92) { echo "selected"; } ?>>92</option>
                      <option value="93" <?php if ($find_ad['etazh'] == 93) { echo "selected"; } ?>>93</option>
                      <option value="94" <?php if ($find_ad['etazh'] == 94) { echo "selected"; } ?>>94</option>
                      <option value="95" <?php if ($find_ad['etazh'] == 95) { echo "selected"; } ?>>95</option>
                      <option value="96" <?php if ($find_ad['etazh'] == 96) { echo "selected"; } ?>>96</option>
                      <option value="97" <?php if ($find_ad['etazh'] == 97) { echo "selected"; } ?>>97</option>
                      <option value="98" <?php if ($find_ad['etazh'] == 98) { echo "selected"; } ?>>98</option>
                      <option value="99" <?php if ($find_ad['etazh'] == 99) { echo "selected"; } ?>>99</option>
                    </select>
                  </div>

                  <div class="form-group col-xs-6 col-sm-4">
                    <label for="etazhnost">Этажность:</label>
                    <select name="etazhnost" id="etazhnost" class="form-control" required>
                      <option value="-">-</option>
                      <option value="1" <?php if ($find_ad['etazhnost'] == 1) { echo "selected"; } ?>>1</option>
                      <option value="2" <?php if ($find_ad['etazhnost'] == 2) { echo "selected"; } ?>>2</option>
                      <option value="3" <?php if ($find_ad['etazhnost'] == 3) { echo "selected"; } ?>>3</option>
                      <option value="4" <?php if ($find_ad['etazhnost'] == 4) { echo "selected"; } ?>>4</option>
                      <option value="5" <?php if ($find_ad['etazhnost'] == 5) { echo "selected"; } ?>>5</option>
                      <option value="6" <?php if ($find_ad['etazhnost'] == 6) { echo "selected"; } ?>>6</option>
                      <option value="7" <?php if ($find_ad['etazhnost'] == 7) { echo "selected"; } ?>>7</option>
                      <option value="8" <?php if ($find_ad['etazhnost'] == 8) { echo "selected"; } ?>>8</option>
                      <option value="9" <?php if ($find_ad['etazhnost'] == 9) { echo "selected"; } ?>>9</option>
                      <option value="10" <?php if ($find_ad['etazhnost'] == 10) { echo "selected"; } ?>>10</option>
                      <option value="11" <?php if ($find_ad['etazhnost'] == 11) { echo "selected"; } ?>>11</option>
                      <option value="12" <?php if ($find_ad['etazhnost'] == 12) { echo "selected"; } ?>>12</option>
                      <option value="13" <?php if ($find_ad['etazhnost'] == 13) { echo "selected"; } ?>>13</option>
                      <option value="14" <?php if ($find_ad['etazhnost'] == 14) { echo "selected"; } ?>>14</option>
                      <option value="15" <?php if ($find_ad['etazhnost'] == 15) { echo "selected"; } ?>>15</option>
                      <option value="16" <?php if ($find_ad['etazhnost'] == 16) { echo "selected"; } ?>>16</option>
                      <option value="17" <?php if ($find_ad['etazhnost'] == 17) { echo "selected"; } ?>>17</option>
                      <option value="18" <?php if ($find_ad['etazhnost'] == 18) { echo "selected"; } ?>>18</option>
                      <option value="19" <?php if ($find_ad['etazhnost'] == 19) { echo "selected"; } ?>>19</option>
                      <option value="20" <?php if ($find_ad['etazhnost'] == 20) { echo "selected"; } ?>>20</option>
                      <option value="21" <?php if ($find_ad['etazhnost'] == 21) { echo "selected"; } ?>>21</option>
                      <option value="22" <?php if ($find_ad['etazhnost'] == 22) { echo "selected"; } ?>>22</option>
                      <option value="23" <?php if ($find_ad['etazhnost'] == 23) { echo "selected"; } ?>>23</option>
                      <option value="24" <?php if ($find_ad['etazhnost'] == 24) { echo "selected"; } ?>>24</option>
                      <option value="25" <?php if ($find_ad['etazhnost'] == 25) { echo "selected"; } ?>>25</option>
                      <option value="26" <?php if ($find_ad['etazhnost'] == 26) { echo "selected"; } ?>>26</option>
                      <option value="27" <?php if ($find_ad['etazhnost'] == 27) { echo "selected"; } ?>>27</option>
                      <option value="28" <?php if ($find_ad['etazhnost'] == 28) { echo "selected"; } ?>>28</option>
                      <option value="29" <?php if ($find_ad['etazhnost'] == 29) { echo "selected"; } ?>>29</option>
                      <option value="30" <?php if ($find_ad['etazhnost'] == 30) { echo "selected"; } ?>>30</option>
                      <option value="31" <?php if ($find_ad['etazhnost'] == 31) { echo "selected"; } ?>>31</option>
                      <option value="32" <?php if ($find_ad['etazhnost'] == 32) { echo "selected"; } ?>>32</option>
                      <option value="33" <?php if ($find_ad['etazhnost'] == 33) { echo "selected"; } ?>>33</option>
                      <option value="34" <?php if ($find_ad['etazhnost'] == 34) { echo "selected"; } ?>>34</option>
                      <option value="35" <?php if ($find_ad['etazhnost'] == 35) { echo "selected"; } ?>>35</option>
                      <option value="36" <?php if ($find_ad['etazhnost'] == 36) { echo "selected"; } ?>>36</option>
                      <option value="37" <?php if ($find_ad['etazhnost'] == 37) { echo "selected"; } ?>>37</option>
                      <option value="38" <?php if ($find_ad['etazhnost'] == 38) { echo "selected"; } ?>>38</option>
                      <option value="39" <?php if ($find_ad['etazhnost'] == 39) { echo "selected"; } ?>>39</option>
                      <option value="40" <?php if ($find_ad['etazhnost'] == 40) { echo "selected"; } ?>>40</option>
                      <option value="41" <?php if ($find_ad['etazhnost'] == 41) { echo "selected"; } ?>>41</option>
                      <option value="42" <?php if ($find_ad['etazhnost'] == 42) { echo "selected"; } ?>>42</option>
                      <option value="43" <?php if ($find_ad['etazhnost'] == 43) { echo "selected"; } ?>>43</option>
                      <option value="44" <?php if ($find_ad['etazhnost'] == 44) { echo "selected"; } ?>>44</option>
                      <option value="45" <?php if ($find_ad['etazhnost'] == 45) { echo "selected"; } ?>>45</option>
                      <option value="46" <?php if ($find_ad['etazhnost'] == 46) { echo "selected"; } ?>>46</option>
                      <option value="47" <?php if ($find_ad['etazhnost'] == 47) { echo "selected"; } ?>>47</option>
                      <option value="48" <?php if ($find_ad['etazhnost'] == 48) { echo "selected"; } ?>>48</option>
                      <option value="49" <?php if ($find_ad['etazhnost'] == 49) { echo "selected"; } ?>>49</option>
                      <option value="50" <?php if ($find_ad['etazhnost'] == 50) { echo "selected"; } ?>>50</option>
                      <option value="51" <?php if ($find_ad['etazhnost'] == 51) { echo "selected"; } ?>>51</option>
                      <option value="52" <?php if ($find_ad['etazhnost'] == 52) { echo "selected"; } ?>>52</option>
                      <option value="53" <?php if ($find_ad['etazhnost'] == 53) { echo "selected"; } ?>>53</option>
                      <option value="54" <?php if ($find_ad['etazhnost'] == 54) { echo "selected"; } ?>>54</option>
                      <option value="55" <?php if ($find_ad['etazhnost'] == 55) { echo "selected"; } ?>>55</option>
                      <option value="56" <?php if ($find_ad['etazhnost'] == 56) { echo "selected"; } ?>>56</option>
                      <option value="57" <?php if ($find_ad['etazhnost'] == 57) { echo "selected"; } ?>>57</option>
                      <option value="58" <?php if ($find_ad['etazhnost'] == 58) { echo "selected"; } ?>>58</option>
                      <option value="59" <?php if ($find_ad['etazhnost'] == 59) { echo "selected"; } ?>>59</option>
                      <option value="60" <?php if ($find_ad['etazhnost'] == 60) { echo "selected"; } ?>>60</option>
                      <option value="61" <?php if ($find_ad['etazhnost'] == 61) { echo "selected"; } ?>>61</option>
                      <option value="62" <?php if ($find_ad['etazhnost'] == 62) { echo "selected"; } ?>>62</option>
                      <option value="63" <?php if ($find_ad['etazhnost'] == 63) { echo "selected"; } ?>>63</option>
                      <option value="64" <?php if ($find_ad['etazhnost'] == 64) { echo "selected"; } ?>>64</option>
                      <option value="65" <?php if ($find_ad['etazhnost'] == 65) { echo "selected"; } ?>>65</option>
                      <option value="66" <?php if ($find_ad['etazhnost'] == 66) { echo "selected"; } ?>>66</option>
                      <option value="67" <?php if ($find_ad['etazhnost'] == 67) { echo "selected"; } ?>>67</option>
                      <option value="68" <?php if ($find_ad['etazhnost'] == 68) { echo "selected"; } ?>>68</option>
                      <option value="69" <?php if ($find_ad['etazhnost'] == 69) { echo "selected"; } ?>>69</option>
                      <option value="70" <?php if ($find_ad['etazhnost'] == 70) { echo "selected"; } ?>>70</option>
                      <option value="71" <?php if ($find_ad['etazhnost'] == 71) { echo "selected"; } ?>>71</option>
                      <option value="72" <?php if ($find_ad['etazhnost'] == 72) { echo "selected"; } ?>>72</option>
                      <option value="73" <?php if ($find_ad['etazhnost'] == 73) { echo "selected"; } ?>>73</option>
                      <option value="74" <?php if ($find_ad['etazhnost'] == 74) { echo "selected"; } ?>>74</option>
                      <option value="75" <?php if ($find_ad['etazhnost'] == 75) { echo "selected"; } ?>>75</option>
                      <option value="76" <?php if ($find_ad['etazhnost'] == 76) { echo "selected"; } ?>>76</option>
                      <option value="77" <?php if ($find_ad['etazhnost'] == 77) { echo "selected"; } ?>>77</option>
                      <option value="78" <?php if ($find_ad['etazhnost'] == 78) { echo "selected"; } ?>>78</option>
                      <option value="79" <?php if ($find_ad['etazhnost'] == 79) { echo "selected"; } ?>>79</option>
                      <option value="80" <?php if ($find_ad['etazhnost'] == 80) { echo "selected"; } ?>>80</option>
                      <option value="81" <?php if ($find_ad['etazhnost'] == 81) { echo "selected"; } ?>>81</option>
                      <option value="82" <?php if ($find_ad['etazhnost'] == 82) { echo "selected"; } ?>>82</option>
                      <option value="83" <?php if ($find_ad['etazhnost'] == 83) { echo "selected"; } ?>>83</option>
                      <option value="84" <?php if ($find_ad['etazhnost'] == 84) { echo "selected"; } ?>>84</option>
                      <option value="85" <?php if ($find_ad['etazhnost'] == 85) { echo "selected"; } ?>>85</option>
                      <option value="86" <?php if ($find_ad['etazhnost'] == 86) { echo "selected"; } ?>>86</option>
                      <option value="87" <?php if ($find_ad['etazhnost'] == 87) { echo "selected"; } ?>>87</option>
                      <option value="88" <?php if ($find_ad['etazhnost'] == 88) { echo "selected"; } ?>>88</option>
                      <option value="89" <?php if ($find_ad['etazhnost'] == 89) { echo "selected"; } ?>>89</option>
                      <option value="90" <?php if ($find_ad['etazhnost'] == 90) { echo "selected"; } ?>>90</option>
                      <option value="91" <?php if ($find_ad['etazhnost'] == 91) { echo "selected"; } ?>>91</option>
                      <option value="92" <?php if ($find_ad['etazhnost'] == 92) { echo "selected"; } ?>>92</option>
                      <option value="93" <?php if ($find_ad['etazhnost'] == 93) { echo "selected"; } ?>>93</option>
                      <option value="94" <?php if ($find_ad['etazhnost'] == 94) { echo "selected"; } ?>>94</option>
                      <option value="95" <?php if ($find_ad['etazhnost'] == 95) { echo "selected"; } ?>>95</option>
                      <option value="96" <?php if ($find_ad['etazhnost'] == 96) { echo "selected"; } ?>>96</option>
                      <option value="97" <?php if ($find_ad['etazhnost'] == 97) { echo "selected"; } ?>>97</option>
                      <option value="98" <?php if ($find_ad['etazhnost'] == 98) { echo "selected"; } ?>>98</option>
                      <option value="99" <?php if ($find_ad['etazhnost'] == 99) { echo "selected"; } ?>>99</option>
                    </select>
                  </div>

                  <div class="form-group col-xs-12 col-sm-4">
                    <label for="etazhnost">Тип дома:</label>
                    <select name="mat" id="mat" class="form-control" required>
                      <option value="k" <?php if ($find_ad['etazhnost'] == "k") { echo "selected"; } ?>>Кирпичный</option>
                      <option value="p" <?php if ($find_ad['etazhnost'] == "p") { echo "selected"; } ?>>Панельный</option>
                      <option value="b" <?php if ($find_ad['etazhnost'] == "b") { echo "selected"; } ?>>Блочный</option>
                      <option value="m" <?php if ($find_ad['etazhnost'] == "m") { echo "selected"; } ?>>Монолитный</option>
                      <option value="mk" <?php if ($find_ad['etazhnost'] == "mk") { echo "selected"; } ?>>Монолитно-кирпичный</option>
                      <option value="d" <?php if ($find_ad['etazhnost'] == "d") { echo "selected"; } ?>>Деревянный</option>
                    </select>
                  </div>
                </div>
                
                <div class="row">
                  
                  <div class="form-group col-xs-12 col-sm-6">
                    <label for="size">Общая площадь:</label>
                    <input type="text" name="size" id="size" class="form-control only_num" value="<?php echo $find_ad['size']; ?>" required>
                    <span class="kvm"></span>
                  </div>

                  <div class="form-group col-xs-12 col-sm-6">
                    <label for="price">Стоимость:</label>
                    <input type="text" name="price" id="price" class="form-control only_num" value="<?php echo $find_ad['price']; ?>" required>
                    <span class="price_kvm"></span>
                  </div>
                  <?php if ($section == 2): ?>
                  <script>
                    $('.only_num').bind("change keyup input click", function() {
                      if (this.value.match(/[^0-9]/g)) {
                        this.value = this.value.replace(/[^0-9]/g, '');
                      }
                    });

                    $('#size').bind('keyup keydown paste',function(e) {
                      var $size=$('#size');
                      var $price_no_space = $('#price').val().replace(/\s+/g,'');

                      setTimeout(function(){
                        $('.kvm').text($size.val() + " кв.м");
                        $price_kvm = $price_no_space / $size.val();
                        $('.price_kvm').text($price_kvm.toFixed(0) + " руб./кв.м");
                      },0);
                    });

                    $('#price').bind('keyup keydown paste',function(e) {
                      var $price = $(this);
                      var $price_no_space = $price.val().replace(/\s+/g,'');
                      var $size = $('#size');

                      setTimeout(function(){
                        $price_kvm = $price_no_space / $size.val();
                        $('.price_kvm').text($price_kvm.toFixed(0) + " руб./кв.м");
                      },0);
                    });
                  </script>
                  <?php endif ?>
                </div>

                <hr>

                <div class="row">
                  <?php if ($section == 1): ?>
                  <div class="form-group col-xs-12 col-sm-6">
                    <label for="srok">Срок аренды:</label>
                    <select name="srok" id="srok" class="form-control" required>
                      <option value="long" <?php if ($find_ad['srok'] == "long") { echo "selected"; } ?>>На длительный срок</option>
                      <option value="short" <?php if ($find_ad['srok'] == "short") { echo "selected"; } ?>>Посуточно</option>
                    </select>

                    <script>
                      $("#srok").on('change', function(){
                        $("#long").hide();
                        $("#short").hide();
                        if($(this).val() == "long"){
                          $("#long").show();
                        } else {
                          $("#short").show();
                        }
                      })
                    </script>
                    <span id="long" class="kvm" <?php if ($find_ad['srok'] == "short") { echo "style='display: none;'"; } ?>>Указана цена за месяц</span>
                    <span id="short" class="kvm" <?php if ($find_ad['srok'] == "long") { echo "style='display: none;'"; } ?>>Указана цена за сутки</span>
                  </div>
                  <?php else: ?>
                  <div class="form-group col-xs-12 col-sm-6">
                    <label>Возможность обмена:</label>
                    <div class="label_deskr">
                      Рассматриваете ли вы возможность обмена недвижимости?
                    </div>
                    <label class="radio_check"><input type="radio" name="exchange" value="1" <?php if ($find_ad['exchange'] == "1") { echo "checked"; } ?>> Да</label>
                    <label class="radio_check"><input type="radio" name="exchange" value="0" <?php if ($find_ad['exchange'] == "0") { echo "checked"; } ?>> Нет</label>
                  </div>
                  <?php endif ?>

                  <div class="form-group col-xs-12 col-sm-6">
                    <label>Принимаете услуги риэлторов?</label>
                    <div class="label_deskr">
                      Готовы ли вы к сотрудничеству с агентствами недвижимости
                    </div>
                    <label class="radio_check"><input type="radio" name="agent" value="1" <?php if ($find_ad['agent'] == "1") { echo "checked"; } ?>> Да</label>
                    <label class="radio_check"><input type="radio" name="agent" value="0" <?php if ($find_ad['agent'] == "0") { echo "checked"; } ?>> Нет</label>
                  </div>
                </div>

                <hr>

                <div class="form-group">
                  <label for="infra">Инфраструктура</label>
                  <input type="text" name="infra" id="infra" class="form-control" value="<?php echo $find_ad['infra']; ?>" placeholder="Перечислите через запятую">
                </div>

                <hr>

                <div class="form-group">
                  <label for="text">Текст объявления</label>
                  <textarea name="text" id="text" class="form-control" rows="8" style="resize: none;" required><?php echo $find_ad['text']; ?></textarea>
                </div>
                
                <hr>

                <div class="form-group">
                  <label for="photos">Фотографии</label>
                  <div class="form-group">
                    <a href="<?php echo $admin_dir; ?>photos.php?id=<?php echo $find_ad['id']; ?>" target="_blank" class="form-control">Просмотреть</a>
                  </div>
                </div>

                <hr>

                <div class="row">                  
                  <div class="form-group col-xs-12 col-sm-6">
                    <label for="phone">Номер телефона:</label>
                    <input type="text" name="phone" id="phone" class="form-control" placeholder="+7 (___) ___-____" value="<?php echo $find_ad['phone']; ?>" required>
                  </div>
                  <script type="text/javascript" src="/js/input_mask.js"></script>
                  <script>
                    $(document).ready(function() {
                      $("#phone").mask("+7 (999) 999-9999");
                    });
                  </script>

                  <div class="form-group col-xs-12 col-sm-6">
                    <label for="contact_name">Контактное лицо:</label>
                    <input type="text" name="contact_name" id="contact_name" class="form-control" placeholder="Например, имя и отчество собственника" value="<?php echo $find_ad['contact_name']; ?>" required>
                  </div>
                </div>

              </fieldset>

                </div>
                <!-- Футер модального окна -->
                <div class="modal-footer">
                    <input type="hidden" name="id" value="<?php echo $find_ad['id']; ?>" required>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                    <input type="submit" class="btn btn-success" name="mod1" value="Изменить">
                </div>
            </form>
        </div>
      </div>
    </div>


	<?php endif ?>

<?php 
  include 'footer.php';
?>