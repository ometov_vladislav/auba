<?php 
  	include 'header.php';

	if (isset($_POST['addNews'])) {
		
		if ($moderate_city == 0) {
			$city = $_POST['city'];
		}else{
			if ($_POST['all_city'] == 1) {
	  			$city = 0;
		  	}else{
		  		$city = $moderate_city;
		  	}
		}
		$name = $_POST['name'];
		$text = $_POST['text'];

		addArt($city, $name, $text);
	}

	if (isset($_POST['delNews'])){
		$table = "articles";
		$id = $_POST['id'];
		del($table, $id);
	}

	if (isset($_POST['editNews'])) {
  		if ($moderate_city == 0) {
			$city = $_POST['city'];
		}else{
			if ($_POST['all_city'] == 1) {
	  			$city = 0;
		  	}else{
		  		$city = $moderate_city;
		  	}
		}

  		$id = $_POST['id'];
	  	$name = $_POST['name'];
		$text = $_POST['text'];

		editArt($id, $city, $name, $text);
  	}

  	if (isset($_POST['find_news'])) {
	  	$search = $_POST['search'];
	  	$find_news = find_art($search);

	  	$city_id = $find_news['city_id'];

	  	if ((($moderate_city == 0) or ($city_id == $moderate_city)) and ($city_id <> "")) {
	  		$result = 1;
	  	}else{
	  		$msg['bad'] = "<strong>Внимание!</strong> Статья не найдена. Возможно она размещено не в вашем регионе.";
	  	}
  	}

  	print_msg($msg);
?>
	<script src="//cdn.ckeditor.com/4.8.0/standard/ckeditor.js"></script>
	<div class="page-header">
      <h2>Статьи</h2>
    </div>

    <form action="" method="POST">
    	<div class="row">
    		<div class="col-xs-12 col-md-9">
		      	<div class="form-group">
		        	<input type="text" class="form-control" name="search" placeholder="Введите номер или название статьи">
		      	</div>
		    </div>
		    <div class="col-xs-12 col-md-3">
		      	<input type="submit" class="btn btn-primary" value="Найти статью" name="find_news">
		    </div>
    	</div>
	</form>

	<a href="#addNews" class="btn btn-labeled btn-success" data-toggle="modal">
        <span class="btn-label" ><i class="glyphicon glyphicon-plus"></i></span>Добавить статью
    </a>
    <?php if ($result == 1): ?>
    <a href="#editNews" class="btn btn-labeled btn-warning" data-toggle="modal">
        <span class="btn-label" ><i class="glyphicon glyphicon-edit"></i></span>Редактировать статью
    </a>
	<?php endif ?>

    <div id="addNews" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
            <!-- Заголовок модального окна -->
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
              <h4 class="modal-title">Добавить статью</h4>
            </div>
            <!-- Основное содержимое модального окна -->
            <form action="" method="post">
                <div class="modal-body">
	            	<fieldset>
	            		<?php if ($moderate_city == 0): ?>
	            		<div class="form-group">
		                	<label for="name">Выберите регион:</label>

		                	<select class="form-control" name="city">
		                		<option value="0">Все города</option>
			                	<?php
			                	$city_query  = "SELECT * FROM `city` ORDER BY `id`";
			                	$city = mysql_query($city_query);

		                	  	while($city_row = mysql_fetch_array($city, MYSQL_ASSOC)){   
		                	  	?>
		                	  	<option value="<?php echo $city_row['id']; ?>"><?php echo $city_row['name']; ?></option>
		                	  	<?php } ?>
		                	</select>
		                </div>
						<?php else: ?>
						<div class="form-group">
		                	<label>Разместить статью во всех городах?</label>
		                	<div class="label_deskr">Нажмите "Да", если нужно разместить статью для всех городов</div>
		                	<label class="radio_check"><input type="radio" name="all_city" value="1"> Да</label>
		                	<label class="radio_check"><input type="radio" name="all_city" value="0" checked> Нет</label>
		                </div>
	            		<?php endif ?>

		                <hr>

		                <div class="form-group">
		                  	<label for="name">Название статьи</label>
		                  	<input type="text" name="name" id="name" class="form-control" value="" placeholder="Название объявления">
		                </div>

		                <hr>

		                <div class="form-group">
		                  <label for="text">Текст статьи</label>
		                  <textarea name="text" id="text" class="form-control" style="resize: none;" required></textarea>
		                </div>
	              	</fieldset>
                </div>
                <!-- Футер модального окна -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                    <input type="submit" class="btn btn-success" name="addNews" value="Добавить статью">
                </div>
            </form>
        </div>
      </div>
    </div>

    <?php if ($result == 1): ?>
	<div id="editNews" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
            <!-- Заголовок модального окна -->
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
              <h4 class="modal-title">Редактировать статью</h4>
            </div>
            <!-- Основное содержимое модального окна -->
            <form action="" method="post">
                <div class="modal-body">
	            	<fieldset>
	            		<?php if ($moderate_city == 0): ?>
	            		<div class="form-group">
		                	<label for="name">Выберите регион:</label>

		                	<select class="form-control" name="city">
		                		<option value="0">Все города</option>
			                	<?php
			                	$city_query  = "SELECT * FROM `city` ORDER BY `id`";
			                	$city = mysql_query($city_query);

		                	  	while($city_row = mysql_fetch_array($city, MYSQL_ASSOC)){   
		                	  	?>
		                	  	<option value="<?php echo $city_row['id']; ?>" <?php if ($find_news['city_id'] == $city_row['id']) { echo "selected"; } ?>><?php echo $city_row['name']; ?></option>
		                	  	<?php } ?>
		                	</select>
		                </div>
						<?php else: ?>
						<div class="form-group">
		                	<label>Разместить статью во всех городах?</label>
		                	<div class="label_deskr">Нажмите "Да", если нужно разместить статью для всех городов</div>
		                	<label class="radio_check"><input type="radio" name="all_city" value="1" <?php if ($find_news['city_id'] == 0) { echo "selected"; } ?>> Да</label>
		                	<label class="radio_check"><input type="radio" name="all_city" value="0" <?php if ($find_news['city_id'] <> 0) { echo "selected"; } ?>> Нет</label>
		                </div>
	            		<?php endif ?>

		                <hr>

		                <div class="form-group">
		                  	<label for="name">Название статьи</label>
		                  	<input type="text" name="name" id="name" class="form-control" value="<?php echo $find_news['name']; ?>" placeholder="Название объявления">
		                </div>

		                <hr>

		                <div class="form-group">
		                  <label for="text">Текст статьи</label>
		                  <textarea name="text" id="edit_text" class="form-control" style="resize: none;" required><?php echo $find_news['text']; ?></textarea>
		                </div>
	              	</fieldset>
                </div>
                <!-- Футер модального окна -->
                <div class="modal-footer">
                    <input type="hidden" name="id" value="<?php echo $find_news['id']; ?>" required>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                    <input type="submit" class="btn btn-danger" name="delNews" value="Удалить статью">
                    <input type="submit" class="btn btn-success" name="editNews" value="Сохранить изменения">
                </div>
            </form>
        </div>
      </div>
    </div>
   	<?php endif ?>

    <script>
        CKEDITOR.replace('text');
        CKEDITOR.replace('edit_text');
    </script>

<?php 
  include 'footer.php';
?>