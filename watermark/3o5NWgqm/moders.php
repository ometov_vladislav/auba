<?php 
    include 'header.php';

    if ($_SESSION['role'] <> 1){
        header("Location: /admin/");
        exit;
    }

    if (isset($_POST['add_moder'])) {
        add_moder($_POST['city_id'], $_POST['name'], md5($_POST['password']), 2, $_POST['fio']);
    }
    if (isset($_POST['edit_moder'])) {
        edit_moder($_POST['city_id'], $_POST['name'], md5($_POST['password']), 2, $_POST['fio'], $_POST['id']);
    }
    
    print_msg($msg);
?>


    <?php
        $city_list_query  = "SELECT * FROM `city` ORDER BY `id`";
        $city_list = mysql_query($city_list_query);

        while($city_list_row = mysql_fetch_array($city_list, MYSQL_ASSOC)){ 
            $city_id = $city_list_row['id']; 
    ?>
    <div class="col-xs-12">
        <div class="panel panel-primary">
          <div class="panel-heading">
            <h3 class="panel-title">Модераторы города <?php echo $city_list_row['name']; ?></h3>
          </div>
          <div class="panel-body">
            <div id="list" class="list">
                <ul>
                    <?php
                    $users_city_query  = "SELECT * FROM `users` WHERE `city` = '$city_id' ORDER BY `id`";
                    $users_city = mysql_query($users_city_query);
                    while($users_city_row = mysql_fetch_array($users_city, MYSQL_ASSOC)){  
                    ?>
                    <li>
                        <div class="row">
                            <div class="col-xs-12 col-sm-9 col-md-10"><span><?php echo $users_city_row['login']; ?></span></div>
                            <div class="col-xs-12 col-sm-3 col-md-2">
                                <a href="#editUser<?php echo $users_city_row['id']; ?>" class="edit_btn" data-toggle="modal">Изменить</a>
                                <a href="moders.php?del=users&del_id=<?php echo $users_city_row['id']; ?>" class="del_btn" onClick="return window.confirm('Подтверждаете удаление модератора?');"><b>[x]</b></a>
                            </div>
                        </div>
                    </li>
                    <?php } ?>
                </ul>
            </div>
	      </div>
	    </div>
	</div>
    <?php } ?>

    <?php
        $users_query  = "SELECT * FROM `users` ORDER BY `id`";
        $users = mysql_query($users_query);
        while($users_row = mysql_fetch_array($users, MYSQL_ASSOC)){
    ?>
    <div id="editUser<?php echo $users_row['id']; ?>" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Заголовок модального окна -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Редактировать модератора <?php echo $users_row['login']; ?></h4>
                </div>
                <!-- Основное содержимое модального окна -->
                <form action="" method="post">
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Город:</label>
                            <select id="selest_district" name="city_id" class="form-control" required>
                                <?php
                                    $city_query  = "SELECT * FROM `city` ORDER BY `url_name`";
                                    $city = mysql_query($city_query);

                                    while($city_row = mysql_fetch_array($city, MYSQL_ASSOC)){    
                                        $id = stripslashes($city_row['id']);
                                    ?>
                                    <option value="<?php echo $city_row['id']; ?>" <?php if ($users_row['city'] ==  $city_row['id']) { echo "selected";} ?>><?php echo $city_row['name']; ?></option>
                                    <?php
                                    }
                                ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Логин:</label>
                            <input type="text" name="name" value="<?php echo $users_row['login']; ?>" class="form-control" required="">
                        </div>

                        <div class="form-group">
                            <label>Пароль:</label>
                            <input type="text" name="password" class="form-control" placeholder="Введите новый пароль">
                        </div>

                        <div class="form-group">
                            <label>ФИО сотрудника:</label>
                            <input type="text" name="fio" value="<?php echo $users_row['fio']; ?>" class="form-control" required="">
                        </div>

                        <input type="hidden" name="id" value="<?php echo $users_row['id']; ?>" class="form-control" required="">
                    </div>
                    <!-- Футер модального окна -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                        <input type="submit" class="btn btn-success" name="edit_moder" value="Сохранить изменения">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <?php } ?>

<?php 
  include 'footer.php';
?>