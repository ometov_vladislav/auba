<?php 
  session_start();
  include '../config.php';
  include '..'.$admin_dir.'functions.php';

  $moderate_city = $_SESSION['city'];

  if ($_SESSION['name'] == "") {
    $page_name = str_replace(".php","",basename($_SERVER['REQUEST_URI']));
    if ($page_name <> "login") {
      header("Location: ".$admin_dir."login.php");
    }
  }

  if (isset($_POST['login_btn'])){
    $login = login($_POST['login'], md5($_POST['pass']));
    if ($login == 1) {
      header("Location: ".$admin_dir);
    }else{
      $msg = "Неверный логин или пароль";
    }
  }

  if (isset($_GET['exit'])){
    logout();
  }

  if (isset($_GET['del'])) {
    del($_GET['del'], $_GET['del_id']);
  }

  if ($moderate_city == 0) {
    $count_adverts = mysql_query("SELECT * FROM `adverts` WHERE `moderate` <> 1 and `moderate` <> 2");
    $count_adverts = mysql_num_rows($count_adverts);
  }else{
    $count_adverts = mysql_query("SELECT * FROM `adverts` WHERE `moderate` <> 1 and `moderate` <> 2 and `city_id` = $moderate_city");
    $count_adverts = mysql_num_rows($count_adverts);
  }
?>

<!DOCTYPE html>
<html>
  <head>
    <title>Bootstrap Admin Theme v3</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- styles -->
    <link href="css/styles.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->


    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js"></script>

    <script src="js/main.js"></script>
  </head>
<body>

  <nav class="navbar navbar-inverse">
    <div class="container-fluid">
      <div class="navbar-header">

        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-main" aria-expanded="false">
          <span class=sr-only>Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="<?php echo $admin_dir; ?>">AuBa Admin</a>
      </div>
      
      <?php if ($_SESSION['name'] <> ""): ?>
      <div class="collapse navbar-collapse" id="navbar-main">
        <ul class="nav navbar-nav">
          <?php if ($_SESSION['role'] == 1): ?>
          <li>
            <a href="regions.php">Регионы</a>
          </li>
          <li>
            <a href="cut_list.php">Категории</a>
          </li>
          <li>
            <a href="moders.php">Модераторы</a>
          </li>
          <li>
            <a href="tarif.php">Тарифы</a>
          </li>
          <?php endif ?>
          <li>
            <a href="ads.php">Объявления</a>
          </li>
          <li>
            <a href="news.php">Новости</a>
          </li>
          <li>
            <a href="articles.php">Статьи</a>
          </li>
        </ul>
        <div class="navbar-right">
          <a href="login.php?exit=1" class="btn btn-labeled btn-danger">
            <span class="btn-label"><i class="glyphicon glyphicon-remove"></i></span>Выход
          </a>
        </div>
      </div>
      <?php endif ?>

    </div>
  </nav>

  <div class="container">
    <div class="row">
      <?php if ($_SESSION['name'] <> ""): ?>
      <div class="col-xs-12 col-sm-4 col-lg-4">
        <div class="panel panel-default">
          <!-- Обычное содержимое панели -->
          <div class="panel-heading">Навигация</div>
          <!-- Групповой список -->
          <div class="list-group">
              <a href="moderate.php" class="list-group-item">Модерация объявлений <span class="badge"><?php echo $count_adverts; ?></span></a>
              <a href="articles.php" class="list-group-item">Добавить статью</a>
              <a href="news.php" class="list-group-item">Добавить новость</a>
            <?php if ($_SESSION['role'] == 1): ?>
              <a href="moders.php" class="list-group-item">Добавить модератора</a>
            <?php endif ?>
          </div>
        </div>

        <?php 
          $page_name = $_SERVER['REQUEST_URI'];
        ?>
        <?php if ($_SESSION['role'] == 1): ?>
          <?php if (strripos($page_name, 'cut_list')): ?>
          <div class="panel panel-success">
            <div class="panel-heading">Добавить категорию</div>
            <div class="panel-body">
              <div class="list-group">
                <form action="" method="post">
                  <div class="form-group">
                    <label>Раздел:</label>
                    <select id="selest_district" name="section_id" class="form-control" required>
                      <?php select_sections(); ?>
                    </select>
                  </div>

                  <div class="form-group">
                    <label>Название категории:</label>
                    <input type="text" name="cut_name" class="form-control" required>
                  </div>

                  <div class="form-group btn-section">
                    <input type="submit" class="btn btn-success" name="add_cut" value="Добавить">
                  </div>
                </form>
              </div>
            </div>
          </div>

          <div class="panel panel-success">
            <!-- Обычное содержимое панели -->
            <div class="panel-heading">Добавить раздел</div>
            <div class="panel-body">
              <div class="list-group">
                <form action="" method="post">
                  <div class="form-group">
                    <label>Название раздела:</label>
                    <input type="text" name="name" class="form-control" required>
                  </div>

                  <div class="form-group btn-section">
                    <input type="submit" class="btn btn-success" name="add_section" value="Добавить">
                  </div>
                </form>
              </div>
            </div>
          </div>
          <?php elseif (strripos($page_name, 'regions')): ?>
          <div class="panel panel-success">
            <!-- Обычное содержимое панели -->
            <div class="panel-heading">Добавить населенный пункт</div>
            <div class="panel-body">
              <div class="list-group">
                <form action="" method="post">
                  <div class="form-group">
                    <label>Город:</label>
                    <select id="selest_district" name="city_id" class="form-control" required>
                      <?php
                        select_city();
                      ?>
                    </select>
                  </div>

                  <div class="form-group">
                    <label>Тип:</label>
                    <select id="selest_district" name="type" class="form-control" required>
                      <option value="1">Район города (микрорайон города)</option>
                      <option value="2" style="background: #fff0b7;">Пригород</option>
                      <option value="3" style="background: #d2f2b6;">Область</option>
                    </select>
                  </div>

                  <div class="form-group">
                    <label>Название населенного пункта:</label>
                    <input type="text" name="name" class="form-control" required>
                  </div>

                  <div class="form-group btn-section">
                    <input type="submit" class="btn btn-success" name="add_locality" value="Добавить">
                  </div>
                </form>
              </div>
            </div>
          </div>

          <div class="panel panel-success">
            <!-- Обычное содержимое панели -->
            <div class="panel-heading">Добавить город</div>
            <div class="panel-body">
              <div class="list-group">
                <form action="" method="post">

                  <div class="form-group">
                    <label>Название города:</label>
                    <input type="text" name="name" class="form-control" required>
                  </div>

                  <div class="form-group btn-section">
                    <input type="submit" class="btn btn-success" name="add_city" value="Добавить">
                  </div>
                </form>
              </div>
            </div>
          </div>
          
          <?php elseif (strripos($page_name, 'moders')): ?>
          <div class="panel panel-success">
            <!-- Обычное содержимое панели -->
            <div class="panel-heading">Добавить модератора</div>
            <div class="panel-body">
              <div class="list-group">
                <form action="" method="post">

                  <div class="form-group">
                    <label>Город:</label>
                    <select id="selest_district" name="city_id" class="form-control" required>
                      <?php
                        select_city();
                      ?>
                    </select>
                  </div>

                  <div class="form-group">
                    <label>Логин</label>
                    <input type="text" name="name" class="form-control" required>
                  </div>

                  <div class="form-group">
                    <label>Пароль</label>
                    <input type="password" name="password" class="form-control" required>
                  </div>

                  <div class="form-group">
                    <label>ФИО сотрудника</label>
                    <input type="text" name="fio" class="form-control" required>
                  </div>

                  <div class="form-group btn-section">
                    <input type="submit" class="btn btn-success" name="add_moder" value="Добавить">
                  </div>
                </form>
              </div>
            </div>
          </div>
          <?php endif ?>

        
        <?php endif ?>
        </div>
      <?php endif ?>

      <?php if ($_SESSION['name'] == ""): ?>
        <div class="col-sm-2"></div>
      <?php endif ?>
      <div class="col-xs-12 col-sm-8 col-lg-8">
        <div class="row">    