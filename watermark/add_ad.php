<?php
  include 'header.php';

  if (isset($_POST['send'])) {
    $next_step = $_POST['step'];
    if ($next_step ==2) {
      $city = $_POST['city'];
      $section = $_POST['section'];
      $category = $_POST['category'];
      $selest_locality = $_POST['selest_locality'];
      $district_name = $_POST['district_name'];
      $street = $_POST['street'];
      $num = $_POST['num'];
      $etazh = $_POST['etazh'];
      $etazhnost = $_POST['etazhnost'];
      $mat = $_POST['mat'];
      $size = $_POST['size'];
      $price = $_POST['price'];
      $srok = $_POST['srok'];
      $exchange = $_POST['exchange'];
      $agent = $_POST['agent'];
      $infra = $_POST['infra'];
      $text = $_POST['text'];
      if ($_FILES['photos']['name'][0] <> "") {
        $photos = $_FILES['photos'];
      }else{
        $photos = 0;
      }
      $phone = $_POST['phone'];
      $contact_name = $_POST['contact_name'];
      $pass = $_POST['pass'];
      
      if ( ($city <> "") or ($section <> "") or ($category <> "") or ($street <> "") or ($num <> "") or ($etazh <> "") or ($etazhnost <> "") or ($mat <> "") or ($size <> "") or ($price <> "") or ($agent <> "") or ($text <> "") or ($phone <> "") or ($contact_name <> "") or ($pass <> "") ) {
        add_advert(strip_tags($city), strip_tags($section), strip_tags($category), strip_tags($selest_locality), strip_tags($district_name), strip_tags($street), strip_tags($num), strip_tags($etazh), strip_tags($etazhnost), strip_tags($mat), strip_tags($size), strip_tags($price), strip_tags($srok), strip_tags($exchange), strip_tags($agent), strip_tags($infra), strip_tags($text), $photos, strip_tags($phone), strip_tags($contact_name), md5(strip_tags($pass)));
      }
    }else{
        //Получаем пост от recaptcha
        $recaptcha = $_POST['g-recaptcha-response'];

        //Сразу проверяем, что он не пустой
        if(!empty($recaptcha)) {
          //Получаем HTTP от recaptcha
          $recaptcha = $_REQUEST['g-recaptcha-response'];
          $secret = '6LfN-lwUAAAAAKzpUNgiuKHuW42ayqv3o3zJYpsX';
          $url = "https://www.google.com/recaptcha/api/siteverify?secret=".$secret ."&response=".$recaptcha."&remoteip=".$_SERVER['REMOTE_ADDR'];

          //Инициализация и настройка запроса
          $curl = curl_init();
          curl_setopt($curl, CURLOPT_URL, $url);
          curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
          curl_setopt($curl, CURLOPT_TIMEOUT, 10);
          curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.16) Gecko/20110319 Firefox/3.6.16");
          //Выполняем запрос и получается ответ от сервера гугл
          $curlData = curl_exec($curl);

          curl_close($curl);  
          //Ответ приходит в виде json строки, декодируем ее
          $curlData = json_decode($curlData, true);

          //Смотрим на результат 
          if($curlData['success']) {
            $next_step = 2;
            $city = $_POST['city'];

            $city_result = mysql_query("select * from `city` WHERE `id`='$city'")or die(mysql_error());
            $city_name = mysql_fetch_array($city_result);
            $city_name = $city_name['name'];

            $category = $_POST['category'];

            $section_id_result = mysql_query("select * from `categories` WHERE `id`='$category'")or die(mysql_error());
            $section_id = mysql_fetch_array($section_id_result);
            $section = $section_id['section_id'];
          }
        }
    }
  }

  $title_page = "Разместить объявление";
?>
  <title><?php echo $site_name." ".$title_delimiter." ".$title_page; ?></title>
    <div id="add_ad_page">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <div class="page-header">
              <h2>Добавить новое объявление в городе <?php echo $city_name; ?></h2>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-2"></div>
          <div class="col-xs-12 col-md-8">
            <form id="add_ad" action="" method="POST" enctype="multipart/form-data">
              <fieldset>
              <?php if ($next_step == 2): ?>
                <input type="hidden" name="step" value="2">
                <input type="hidden" name="city" value="<?php echo $city; ?>">
                <input type="hidden" name="category" value="<?php echo $category; ?>">
                <input type="hidden" name="section" value="<?php echo $section; ?>">
                <div class="form-group">
                  <label for="name">Населенный пункт:</label>

                  <select id="selest_locality" name="selest_locality" class="form-control" required>
                    <?php
                      $type[1][1] = "Районы города";
                      $type[1][2] = "Пригород";
                      $type[1][3] = "Область";
                      $type[2][1] = 'none';
                      $type[2][2] = '#fff0b7';
                      $type[2][3] = '#d2f2b6';

                      $city_query = mysql_query("SELECT * FROM `city` WHERE `id`='$city'");
                      $city_query_row = mysql_fetch_array($city_query);
                    ?>
                      <?php
                        $locality_query  = "SELECT * FROM `locality` WHERE `city_id` = $city  ORDER BY `type`";
                        $locality = mysql_query($locality_query);

                        $i = 1;
                        while($locality_row = mysql_fetch_array($locality, MYSQL_ASSOC)){
                          $optgroup = $locality_row['type'];
                          if ($i == 1) { $last_optgroup = $locality_row['type']; }
                      ?>

                        <?php if (($i == 1) or ($optgroup <> $last_optgroup)): ?>
                          <optgroup label="<?php echo $type[1][$optgroup]; ?>" style="background: <?php echo $type[2][$optgroup]; ?>">
                          <?php $last_optgroup = $optgroup; ?>
                        <?php endif ?>

                          <option value="<?php echo $locality_row['id']; ?>"><?php echo $locality_row['name']; ?></option>

                        <?php if (($i <> 1) & ($optgroup <> $last_optgroup)): ?>
                          </optgroup>
                          <?php $last_optgroup = $last_optgroup + 1; ?>
                        <?php endif ?>

                      <?php 
                          $last_optgroup = $optgroup;
                          $i++;
                        }
                      ?>
                      </optgroup>

                    <option disabled></option>
                    <option value="-">- Указать другой населенный пункт -</option>
                  </select>
                  <script>
                    $("#selest_locality").on('change', function(){
                      if($(this).val() == "-"){
                        $("#district_name").show();
                        $('#district_name_input').prop('required', true);
                      } else {
                        $("#district_name").hide();
                        $('#district_name_input').prop('required', false);
                      }
                    })
                  </script>
                </div>

                <div id="district_name" class="form-group" style="display: none;">
                  <input id="district_name_input" type="text" name="district_name" class="form-control" placeholder="Введите имя населенного пункта">
                </div>
                
                <div class="row">
                  <div class="form-group col-xs-12 col-sm-6 col-md-8 col-lg-9">
                    <label>Улица</label>
                    <input type="text" name="street" class="form-control" required>
                  </div>

                  <div class="form-group col-xs-12 col-sm-6 col-md-4 col-lg-3">
                    <label>№ дома</label>
                    <input type="text" name="num" class="form-control" required>
                  </div>
                </div>

                <hr>

                <div class="row">
                  <div class="form-group col-xs-6 col-sm-4">
                    <label for="etazh">Этаж:</label>
                    <select name="etazh" id="etazh" class="form-control" required>
                      <option value="-">-</option>
                      <option value="0">цокольный</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option><option value="60">60</option><option value="61">61</option><option value="62">62</option><option value="63">63</option><option value="64">64</option><option value="65">65</option><option value="66">66</option><option value="67">67</option><option value="68">68</option><option value="69">69</option><option value="70">70</option><option value="71">71</option><option value="72">72</option><option value="73">73</option><option value="74">74</option><option value="75">75</option><option value="76">76</option><option value="77">77</option><option value="78">78</option><option value="79">79</option><option value="80">80</option><option value="81">81</option><option value="82">82</option><option value="83">83</option><option value="84">84</option><option value="85">85</option><option value="86">86</option><option value="87">87</option><option value="88">88</option><option value="89">89</option><option value="90">90</option><option value="91">91</option><option value="92">92</option><option value="93">93</option><option value="94">94</option><option value="95">95</option><option value="96">96</option><option value="97">97</option><option value="98">98</option><option value="99">99</option>
                    </select>
                  </div>

                  <div class="form-group col-xs-6 col-sm-4">
                    <label for="etazhnost">Этажность:</label>
                    <select name="etazhnost" id="etazhnost" class="form-control" required>
                      <option value="-">-</option>
                      <option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option><option value="60">60</option><option value="61">61</option><option value="62">62</option><option value="63">63</option><option value="64">64</option><option value="65">65</option><option value="66">66</option><option value="67">67</option><option value="68">68</option><option value="69">69</option><option value="70">70</option><option value="71">71</option><option value="72">72</option><option value="73">73</option><option value="74">74</option><option value="75">75</option><option value="76">76</option><option value="77">77</option><option value="78">78</option><option value="79">79</option><option value="80">80</option><option value="81">81</option><option value="82">82</option><option value="83">83</option><option value="84">84</option><option value="85">85</option><option value="86">86</option><option value="87">87</option><option value="88">88</option><option value="89">89</option><option value="90">90</option><option value="91">91</option><option value="92">92</option><option value="93">93</option><option value="94">94</option><option value="95">95</option><option value="96">96</option><option value="97">97</option><option value="98">98</option><option value="99">99</option>
                    </select>
                  </div>

                  <div class="form-group col-xs-12 col-sm-4">
                    <label for="etazhnost">Тип дома:</label>
                    <select name="mat" id="mat" class="form-control" required>
                      <option value="k">Кирпичный</option>
                      <option value="p">Панельный</option>
                      <option value="b">Блочный</option>
                      <option value="m">Монолитный</option>
                      <option value="mk">Монолитно-кирпичный</option>
                      <option value="d">Деревянный</option>
                    </select>
                  </div>
                </div>

                <hr>

                <div class="row">
                  <?php if ($section == 1): ?>
                  <div class="form-group col-xs-12 col-sm-6">
                    <label for="srok">Срок аренды:</label>
                    <select name="srok" id="srok" class="form-control" required>
                      <option value="long">На длительный срок</option>
                      <option value="short">Посуточно</option>
                    </select>

                    <script>
                      $("#srok").on('change', function(){
                        $("#long").hide();
                        $("#short").hide();
                        $("#long_price").hide();
                        $("#short_price").hide();
                        if($(this).val() == "long"){
                          $("#long").show();
                          $("#long_price").show();
                        } else {
                          $("#short").show();
                          $("#short_price").show();
                        }
                      })
                    </script>
                    <span id="long" class="kvm">Укажите цену за месяц</span>
                    <span id="short" class="kvm" style="display: none;">Укажите цену за сутки</span>
                  </div>
                  <?php else: ?>
                  <div class="form-group col-xs-12 col-sm-6">
                    <label>Возможность обмена:</label>
                    <div class="label_deskr">
                      Рассматриваете ли вы возможность обмена недвижимости?
                    </div>
                    <label class="radio_check"><input type="radio" name="exchange" value="1" > Да</label>
                    <label class="radio_check"><input type="radio" name="exchange" value="0" checked > Нет</label>
                  </div>
                  <?php endif ?>

                  <div class="form-group col-xs-12 col-sm-6">
                    <label>Принимаете услуги риэлторов?</label>
                    <div class="label_deskr">
                      Готовы ли вы к сотрудничеству с агентствами недвижимости
                    </div>
                    <label class="radio_check"><input type="radio" name="agent" value="1" checked=""> Да</label>
                    <label class="radio_check"><input type="radio" name="agent" value="0"> Нет</label>
                  </div>
                </div>

                <hr>
                
                <div class="row">
                  <?php if ($section == 1): ?>
                  <div class="form-group col-xs-12 col-sm-6">
                    <label for="size">Общая площадь:</label>
                    <input type="text" name="size" id="size" class="form-control only_num" required>
                  </div>

                  <div class="form-group col-xs-12 col-sm-6">
                    <label for="price">Стоимость:</label>
                    <input type="text" name="price" id="price" class="form-control only_num" required>
                    <?php if ($section == 1): ?>
                    <span id="long_price" class="kvm">За месяц</span>
                    <span id="short_price" class="kvm" style="display: none;">За сутки</span>
                    <?php endif ?>
                  </div>
                  <?php elseif ($section == 2): ?>
                  <div class="form-group col-xs-12 col-sm-6">
                    <label for="size">Общая площадь:</label>
                    <input type="text" name="size" id="size" class="form-control only_num" required>
                    <span class="kvm"></span>
                  </div>

                  <div class="form-group col-xs-12 col-sm-6">
                    <label for="price">Стоимость:</label>
                    <input type="text" name="price" id="price" class="form-control only_num" required>
                    <span class="price_kvm"></span>
                  </div>

                  <script>
                    $('#size').bind('keyup keydown paste',function(e) {
                      var $size=$('#size');
                      var $price_no_space = $('#price').val().replace(/\s+/g,'');

                      setTimeout(function(){
                        $('.kvm').text($size.val() + " кв.м");
                        $price_kvm = $price_no_space / $size.val();
                        $('.price_kvm').text($price_kvm.toFixed(0) + " руб./кв.м");
                      },0);
                    });

                    $('#price').bind('keyup keydown paste',function(e) {
                      var $price = $(this);
                      var $price_no_space = $price.val().replace(/\s+/g,'');
                      var $size = $('#size');

                      setTimeout(function(){
                        $price_kvm = $price_no_space / $size.val();
                        $('.price_kvm').text($price_kvm.toFixed(0) + " руб./кв.м");
                      },0);
                    });
                  </script>
                  <?php endif ?>
                  <script type="text/javascript">
                    $('.only_num').bind("change keyup input click", function() {
                      if (this.value.match(/[^0-9]/g)) {
                        this.value = this.value.replace(/[^0-9]/g, '');
                      }
                    });
                  </script>
                </div>

                <hr>

                <div class="form-group">
                  <label for="infra">Инфраструктура</label>
                  <input type="text" name="infra" id="infra" class="form-control" placeholder="Перечислите через запятую">
                </div>

                <hr>

                <div class="form-group">
                  <label for="text">Текст объявления</label>
                  <textarea name="text" id="text" class="form-control" rows="8" style="resize: none;" required></textarea>
                </div>
                
                <hr>

                <div class="form-group">
                  <label for="photos">Фотографии</label>
                  <div class="label_deskr">
                    Вы можете загрузить несколько фотографий
                  </div>
                  <input type="file" name="photos[]" id="photos" multiple accept="image/*">
                </div>

                <hr>

                <div class="row">                  
                  <div class="form-group col-xs-12 col-sm-6">
                    <label for="phone">Номер телефона:</label>
                    <input type="text" name="phone" id="phone" class="form-control" placeholder="+7 (___) ___-____" required>
                  </div>
                  <script type="text/javascript" src="/js/input_mask.js"></script>
                  <script>
                    $(document).ready(function() {
                      $("#phone").mask("+7 (999) 999-9999");
                    });
                  </script>

                  <div class="form-group col-xs-12 col-sm-6">
                    <label for="contact_name">Контактное лицо:</label>
                    <input type="text" name="contact_name" id="contact_name" class="form-control" placeholder="Например, имя и отчество собственника" required>
                  </div>
                </div>

                <hr>

                <div class="row">
                  <div class="form-group col-xs-12 col-sm-6">
                    <label for="pass">Пароль</label>
                    <div class="label_deskr">
                      Придумайте пароль для управления объявлением
                    </div>
                    <input type="password" name="pass" id="pass" class="form-control" required>
                  </div>
                </div>

                <hr>

              <?php else: ?>
                <form>
                <div class="form-group">
                  <label for="name">Выберите регион:</label>

                  <select class="form-control" name="city">
                    <?php
                    $city_query  = "SELECT * FROM `city` ORDER BY `id`";
                    $city = mysql_query($city_query);

                    while($city_row = mysql_fetch_array($city, MYSQL_ASSOC)){    
                        $id = stripslashes($city_row['id']);   
                    ?>
                    <option value="<?php echo $city_row['id']; ?>"><?php echo $city_row['name']; ?></option>
                    <?php } ?>
                  </select>
                </div>

                <hr>

                <div class="form-group" name="cutegory">
                  <label>Категория объявления:</label>

                  <select id="selest_district" class="form-control" name="category">
                    <?php
                    $sections_query  = "SELECT * FROM `sections` ORDER BY `id`";
                    $sections = mysql_query($sections_query);

                    while($sections_row = mysql_fetch_array($sections, MYSQL_ASSOC)){    
                        $id = stripslashes($sections_row['id']);   
                    ?>
                    <optgroup label="<?php echo $sections_row['name']; ?>">
                      <?php
                      $categories_query  = "SELECT * FROM `categories` WHERE `section_id` = $id ORDER BY `ordinal`";
                      $categories = mysql_query($categories_query);

                      while($categories_row = mysql_fetch_array($categories, MYSQL_ASSOC)){ 
                      ?>
                      <option value="<?php echo $categories_row['id']; ?>"><?php echo $categories_row['cut_name'];; ?></option>
                      <?php } ?>
                    </optgroup>
                    <?php } ?>
                  </select>
                </div>

                <hr>

                <div class="form-group">
                  <label for="name">Подтвердите, что вы не робот:</label>
                  <div class="g-recaptcha" data-sitekey="6LfN-lwUAAAAALhjJ3bxjAV_ncSRHoA57VkkcxB_"></div>
                </div>
                <script src='https://www.google.com/recaptcha/api.js'></script>
                <script>
                  document.getElementById('add_ad').onsubmit = function () {
                    if (!grecaptcha.getResponse()) {
                      alert('Вы не заполнили поле Я не робот!');
                      return false;
                    }
                  }
                </script>

                <hr>
              <?php endif ?>

              </fieldset>
              <div class="text-center">
                <?php if (isset($next_step)): ?>
                <p class="deskr_btn">
                  После добавления объявления оно получит статус «Ожидает проверки», в случае необходимости вам позвонит наш оператор для подтверждения указанных данных.
                </p>
                <button class="btn btn-success btn-lg" type="submit" name="send">Разместить объявление <i class="glyphicon glyphicon-plus"></i></button>
                <?php else: ?>
                <button class="btn btn-primary btn-lg" type="submit" name="send">Продолжить <i class="glyphicon glyphicon-chevron-right"></i></button>
                <?php endif ?>
              </div>

            </form>
            
          </div>
        </div>
      </div>

    </div>

<?php
  include 'footer.php';
?>