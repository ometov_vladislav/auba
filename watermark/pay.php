<?php 

include 'header.php';

if(!empty($_POST["categories"]) && !empty($_POST["tarif"]) && !empty($_POST["mail"])){ // если был пост
    
    $name = trim(htmlspecialchars(strip_tags(base64_encode(urlencode(select_cut($_POST["categories"])))))); // принимаем параметры с формы
    $email = trim(htmlspecialchars(strip_tags(base64_encode(urlencode($_POST["tarif"]))))); // принимаем параметры с формы
    $phone = trim(htmlspecialchars(strip_tags(base64_encode(urlencode($_POST["mail"]))))); // принимаем параметры с формы

    // echo urldecode(base64_decode($name));

    $mrh_login = "auba"; // идентификатор магазина
    $mrh_pass1 = "tzB7HhxZ2VjZ9D0NpWz5"; // пароль #1
    
    $max_id = mysql_query("SELECT * FROM `code` ORDER BY id DESC LIMIT 1");
    $max_id_row = mysql_fetch_array($max_id);

    $inv_id = $max_id_row[0]+1; //получаем номер заказа с файла count.txt

    $tarif_price = mysql_query("SELECT * FROM `tarif` ORDER BY `id`");
    $tarif_price_row = mysql_fetch_array($tarif_price);
    
    $inv_desc = $tarif_price_row['name']; // описание заказа

    $cuts_list = explode(",", urldecode(base64_decode($name)));
    $out_summ = count($cuts_list) * $tarif_price_row['price']; // Получаем цену тарифа

    // $IsTest = 0;
    
    $shp_item = generate_code(); // тип товара
    if (find_code($shp_item) <> 0) {
        do {
            $shp_item = generate_code();
        } while (find_code($shp_item) <> 0);
    }
    
    $in_curr = ""; // предлагаемая валюта платежа
    
    $culture = "ru"; // язык
    
    $encoding = "utf-8"; // кодировка
    
    $crc  = md5("$mrh_login:$out_summ:$inv_id:$mrh_pass1:Shp_item=$shp_item:shp_mulo=$email:shp_names=$name:shp_phone=$phone"); // формирование подписи
    

    
    // Перенаправляем пользователя на страницу оплаты
    Header("Location: http://auth.robokassa.ru/Merchant/Index.aspx?MrchLogin=$mrh_login&OutSum=$out_summ&InvId=$inv_id&IncCurrLabel=$in_curr".
          "&Desc=$inv_desc&SignatureValue=$crc&Shp_item=$shp_item". //&IsTest=$IsTest
          "&Culture=$culture&Encoding=$encoding&shp_mulo=$email&shp_names=$name&shp_phone=$phone");
      
} 

$title_page = "Оплата";
?>
<title><?php echo $site_name." ".$title_delimiter." ".$title_page; ?></title>

<div class="container">
    <div class="col-xs-12">
        <form action="" method="POST" class="by_code">

            <h4>Выберите интересующие вас категории</h4>
            <div class="form-group">
                <div class="checkbox_group_scroll">
                    <div class="row">
                    <?php
                    $sections_query  = "SELECT * FROM `sections` ORDER BY `id`";
                    $sections = mysql_query($sections_query);

                    while($sections_row = mysql_fetch_array($sections, MYSQL_ASSOC)){    
                        $id = stripslashes($sections_row['id']);   
                    ?>
                    <div class="col-xs-12 col-sm-6">
                      <label><?php echo $sections_row['name']; ?></label>
                      <?php
                      $categories_query  = "SELECT * FROM `categories` WHERE `section_id` = $id ORDER BY `ordinal`";
                      $categories = mysql_query($categories_query);

                      while($categories_row = mysql_fetch_array($categories, MYSQL_ASSOC)){ 
                      ?>
                        <div class="checkbox">
                          <label><input type="checkbox" name="categories[]" value="<?php echo $categories_row['id']; ?>"><?php echo $categories_row['cut_name'];; ?></label>
                        </div>
                      <?php } ?>
                    </div>
                    <?php } ?>
                    </div>
                </div>
            </div>
            
            <h4>Выберите подходящий вам тариф</h4>
            <div class="form-group">
                <label>Тариф</label>
                <select id="tarif" class="form-control" name="tarif">
                  <?php
                    $tarifs = mysql_query("SELECT * FROM `tarif` ORDER BY `id`");

                    while($tarifs_row = mysql_fetch_array($tarifs, MYSQL_ASSOC)){  
                  ?>
                    <option value="<?php echo $tarifs_row['id']; ?>" data-price="<?php echo $tarifs_row['price']; ?>"><?php echo $tarifs_row['name']; ?></option>
                  <?php } ?>
                </select>
            </div>

            <h4>Укажите e-mail, для получения кода</h4>
            <div class="form-group">
                <label for="">Ваш email</label>
                <input type="text" class="form-control" placeholder="Укажите email, чтобы мы выслали на него ваш код доступа" name="mail">
            </div>

            <div class="text-center">
                <div id="price"></div>
            </div>

            <div class="form-group text-center">
                <button type="submit" class="btn btn-success btn-lg">Оплатить</button>
            </div>
        </form>
    </div>
</div>

<script>
    var count = 0;
    $(function() {
        select_tarif_price = $("#tarif").find(':selected').data('price')
        $('#tarif').change(function(){
            select_tarif_price = $(this).find(':selected').data('price');
            displayCount();
        });

        count = $('input[type=checkbox]:checked').length;
        // displayCount();

        $('input[type=checkbox]').bind('click' , function(e, a) {   
            if (this.checked) {
                 count += a ? -1 : 1;
            } else {
                 count += a ? 1 : -1;
            }
            displayCount();
        });
    });
    function displayCount() {
        $('#price').text(select_tarif_price * count + " руб");
    }
</script>

<?php include 'footer.php'; ?>

