<?php
  include 'header.php';
?>
    <div id="articles_news_page">
	    <div class="container">
			
			<ol class="breadcrumb">
	        	<li><a href="/">Главная</a></li>
	        	<li><a href="/articles.php">Статьи</a></li>
	        	<li class="active">Название статьи</li>
	        </ol>

	        <div class="row">
	          <div class="col-xs-12">
	            <div class="page-header">
	              <h2>Название статьи</h2>
	            </div>
	          </div>
	        </div>
	        <div class="row">
            	<div class="col-xs-12">
					<div class="post">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corrupti ipsa quos saepe nulla voluptatum laudantium veritatis voluptatem voluptas reprehenderit officiis vel architecto ipsum, expedita autem quis sequi tempora odio corporis quia, ad aliquid excepturi veniam nemo. Cupiditate accusamus, aut, modi, non provident quos quam illo dolorem ex repellendus, aliquid dicta quis? Dolor consequatur minus ab nihil, aliquid corporis ullam officiis cum est quam assumenda necessitatibus eos suscipit, ut excepturi consectetur fuga optio unde sit? Inventore dignissimos corporis ullam quae temporibus eligendi cupiditate laudantium soluta expedita provident porro asperiores beatae debitis nisi reprehenderit ad, odio maxime accusamus, magni in aut ipsum eaque incidunt. Culpa, eum, labore, quidem fuga praesentium distinctio error quia id nam vero incidunt sint. Iure magni, amet odio.</p>
					</div>
				</div>
          	</div>
     	</div>
    </div>

    <div id="lust_news_list" class="grey_bg">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <div class="page-header">
              <h2>Последние статьи</h2>
            </div>
          </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-md-3 col-lg-3">
              <a href="/articles_page.php">
                <div class="thumbnail">
                    <img data-src="holder.js/300x200" alt="300x200" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIzMDAiIGhlaWdodD0iMjAwIj48cmVjdCB3aWR0aD0iMzAwIiBoZWlnaHQ9IjIwMCIgZmlsbD0iI2VlZSIvPjx0ZXh0IHRleHQtYW5jaG9yPSJtaWRkbGUiIHg9IjE1MCIgeT0iMTAwIiBzdHlsZT0iZmlsbDojYWFhO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1zaXplOjE5cHg7Zm9udC1mYW1pbHk6QXJpYWwsSGVsdmV0aWNhLHNhbnMtc2VyaWY7ZG9taW5hbnQtYmFzZWxpbmU6Y2VudHJhbCI+MzAweDIwMDwvdGV4dD48L3N2Zz4=" style="width: 300px; height: 200px;">
                    <div class="caption">
                      <h3>Название статьи</h3>
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque voluptatum quae iusto necessitatibus aliquam eaque?</p>
                    </div>
                </div>
                </a>
            </div>

            <div class="col-sm-6 col-md-3 col-lg-3">
              <a href="/articles_page.php">
                <div class="thumbnail">
                    <img data-src="holder.js/300x200" alt="300x200" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIzMDAiIGhlaWdodD0iMjAwIj48cmVjdCB3aWR0aD0iMzAwIiBoZWlnaHQ9IjIwMCIgZmlsbD0iI2VlZSIvPjx0ZXh0IHRleHQtYW5jaG9yPSJtaWRkbGUiIHg9IjE1MCIgeT0iMTAwIiBzdHlsZT0iZmlsbDojYWFhO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1zaXplOjE5cHg7Zm9udC1mYW1pbHk6QXJpYWwsSGVsdmV0aWNhLHNhbnMtc2VyaWY7ZG9taW5hbnQtYmFzZWxpbmU6Y2VudHJhbCI+MzAweDIwMDwvdGV4dD48L3N2Zz4=" style="width: 300px; height: 200px;">
                    <div class="caption">
                      <h3>Название статьи</h3>
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque voluptatum quae iusto necessitatibus aliquam eaque?</p>
                    </div>
                </div>
                </a>
            </div>

            <div class="col-sm-6 col-md-3 col-lg-3">
              <a href="/articles_page.php">
                <div class="thumbnail">
                    <img data-src="holder.js/300x200" alt="300x200" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIzMDAiIGhlaWdodD0iMjAwIj48cmVjdCB3aWR0aD0iMzAwIiBoZWlnaHQ9IjIwMCIgZmlsbD0iI2VlZSIvPjx0ZXh0IHRleHQtYW5jaG9yPSJtaWRkbGUiIHg9IjE1MCIgeT0iMTAwIiBzdHlsZT0iZmlsbDojYWFhO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1zaXplOjE5cHg7Zm9udC1mYW1pbHk6QXJpYWwsSGVsdmV0aWNhLHNhbnMtc2VyaWY7ZG9taW5hbnQtYmFzZWxpbmU6Y2VudHJhbCI+MzAweDIwMDwvdGV4dD48L3N2Zz4=" style="width: 300px; height: 200px;">
                    <div class="caption">
                      <h3>Название статьи</h3>
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque voluptatum quae iusto necessitatibus aliquam eaque?</p>
                    </div>
                </div>
                </a>
            </div>

            <div class="col-sm-6 col-md-3 col-lg-3">
              <a href="/articles_page.php">
                <div class="thumbnail">
                    <img data-src="holder.js/300x200" alt="300x200" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIzMDAiIGhlaWdodD0iMjAwIj48cmVjdCB3aWR0aD0iMzAwIiBoZWlnaHQ9IjIwMCIgZmlsbD0iI2VlZSIvPjx0ZXh0IHRleHQtYW5jaG9yPSJtaWRkbGUiIHg9IjE1MCIgeT0iMTAwIiBzdHlsZT0iZmlsbDojYWFhO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1zaXplOjE5cHg7Zm9udC1mYW1pbHk6QXJpYWwsSGVsdmV0aWNhLHNhbnMtc2VyaWY7ZG9taW5hbnQtYmFzZWxpbmU6Y2VudHJhbCI+MzAweDIwMDwvdGV4dD48L3N2Zz4=" style="width: 300px; height: 200px;">
                    <div class="caption">
                      <h3>Название статьи</h3>
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque voluptatum quae iusto necessitatibus aliquam eaque?</p>
                    </div>
                </div>
                </a>
            </div>

          </div>
      </div>
    </div>
<?php
  include 'footer.php';
?>