<?php
  include 'header.php';

  $city_url_name = $_GET['city'];
  $city_result = mysql_query("select * from `city` WHERE `url_name`='$select_city'")or die(mysql_error());
  $city_row = mysql_fetch_array($city_result);
  $city_name = $city_row['name'];
  $city_id = $city_row['id'];

  if ($city_name == "") {
    header("Location: /");
  }

  $title_page = $city_name;
?>
  <title><?php echo $site_name." ".$title_delimiter." ".$title_page; ?></title>
    <div id="cut_list">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <div class="page-header">
              <h2>Выберите категорию для поиска недвижимости от собственников</h2>
            </div>
          </div>

          <?php
          $sections_query  = "SELECT * FROM `sections` ORDER BY `id`";
          $sections = mysql_query($sections_query);

          while($sections_row = mysql_fetch_array($sections, MYSQL_ASSOC)){    
              $id = stripslashes($sections_row['id']);  
              $section_name = $sections_row['name']; 
          ?>

          <div class="col-xs-12 col-sm-6">
            <div id="section<?php echo $id; ?>" class="visible-xs visible-sm show_hide_cut_list"><?php echo $section_name; ?> <span class="glyphicon glyphicon-chevron-down"></span></div>
            <div class="list-group section section<?php echo $id; ?>">
              <?php
              $categories_query  = "SELECT * FROM `categories` WHERE `section_id` = $id  ORDER BY `ordinal`";
              $categories = mysql_query($categories_query);

              while($categories_row = mysql_fetch_array($categories, MYSQL_ASSOC)){    
                  $id = stripslashes($categories_row['id']); 

                  $count_adverts_cut_query  = "SELECT * FROM `adverts` WHERE `city_id`='$city_id' AND `category_id`='$id' AND `moderate`=1";
                  $count_adverts_cut = mysql_num_rows(mysql_query($count_adverts_cut_query));          
              ?>
                <a href="/<?php echo $select_city."/".$categories_row['url_name']; ?>" class="list-group-item">
                  <h4 class="list-group-item-heading"><?php echo $categories_row['cut_name']; ?>
                  <?php if ($count_adverts_cut > 0): ?>
                    <span class="badge pull-right"><?php echo $count_adverts_cut; ?></span>
                  <?php endif ?>
                </h4>
                </a>
              <?php } ?>
              
            </div>
            <script type="text/javascript">
              $("#section<?php echo $sections_row['id']; ?>").click(function(){   
                $(".section<?php echo $sections_row['id']; ?>").toggle();
                $(this).children("span").toggleClass('glyphicon-chevron-down');
                $(this).children("span").toggleClass('glyphicon-chevron-up');
              });
            </script>
          </div>

          <?php } ?>
        </div>
      </div>
    </div>

    <div id="lust_ad_list">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <div class="page-header">
              <h2>Последние добавленные объявления</h2>
            </div>
          </div>
        </div>
      </div>

      <?php 
        $limit = 7; //СКОЛЬКО ЗАПИСЕЙ ПОДГРУЖАТЬ

        if ($advert_row['srok'] == "long") {
          $long = 1;
        } else{
          $short = 1;
        }

        $exchange = $advert_row['exchange'];

        printf('
        <input type="hidden" id="city_id" value="'.$city_id.'">
        <input type="hidden" id="limit_ajax" value="'.$limit.'">');
      ?>
      <script type="text/javascript" src="/ajax/js/load_ads_city.js"></script>

      <div class="ads_list">
        <div class="container">
          <div class="row">
            <div id="ads_list" class="col-xs-12">
              <?php
                $monthes = array(
                  1 => 'Января', 2 => 'Февраля', 3 => 'Марта', 4 => 'Апреля',
                  5 => 'Мая', 6 => 'Июня', 7 => 'Июля', 8 => 'Августа',
                  9 => 'Сентября', 10 => 'Октября', 11 => 'Ноября', 12 => 'Декабря'
                );
                
                $adverts_query  = "SELECT * FROM `adverts` WHERE `city_id`='$city_id' AND `moderate`=1 ORDER BY `id` DESC LIMIT $limit";

                $adverts = mysql_query($adverts_query) or die (mysql_error());
                if (mysql_num_rows($adverts)>0) {
                  while($adverts_row = mysql_fetch_array($adverts, MYSQL_ASSOC)){
                    $cut_id = $adverts_row['category_id'];

                    $cut_result = mysql_query("select * from `categories` WHERE `id`='$cut_id'")or die(mysql_error());
                    $cut_row = mysql_fetch_array($cut_result);
                    $cut_url_name = $cut_row['url_name'];

                    $locality_id = $adverts_row['selest_locality'];
                    $advert_id = $adverts_row['id'];
                    $locality_result = mysql_query("select * from `locality` WHERE `id`='$locality_id' and `city_id`='$city_id'")or die(mysql_error());
                    $locality_row = mysql_fetch_array($locality_result);
                    $locality = $locality_row['name'];

                    $adres = $city_name.", ".$locality.", ".$adverts_row['street'].", ".$adverts_row['num'];

                    $ad_photo_result = mysql_query("SELECT url FROM `photos` WHERE `ad_id` = $advert_id ORDER BY id DESC LIMIT 1")or die(mysql_error());
                    $ad_photo_name = mysql_fetch_array($ad_photo_result);
                    $ad_photo = $ad_photo_name['url'];

                    $ad_photo_count_result = mysql_query("SELECT url FROM `photos` WHERE `ad_id` = $advert_id ORDER BY id")or die(mysql_error());
                    $ad_photo_count = mysql_num_rows($ad_photo_count_result);

                    $date = $adverts_row['date'];
                    $date_withdraw = spliti (" ", $date, 2);

                    $current_time = date('j ') . date('n');

                    if ($date == $current_time) {
                      $date_type = 1;
                    }elseif ($date == date('j n', time()-24*3600)) {
                      $date_type = 2;
                    }else{
                      $date_type = 3;
                    }
              ?>
                <div class="ad "><!-- selected -->
                  <div class="row">
                    <?php if ($ad_photo_count > 0): ?>
                      <div class="col-xs-12 col-sm-2">
                        <a href="/<?php echo $_GET['city']."/".$cut_url_name."/ad".$adverts_row['id']; ?>" target="_blank">
                          <div class="photo" style="background-image: url(../img/addadverts/<?php echo $ad_photo; ?>);"><div class="count"><?php echo $ad_photo_count; ?> фото</div></div>
                        </a>
                      </div>
                    <?php endif ?>
                    <div class="col-xs-12 <?php if ($ad_photo_count > 0): ?>col-sm-7 col-md-7<?php else: ?>col-sm-9 col-md-9<?php endif ?>">
                      <div class="deskr">
                        <div class="name"><a href="/<?php echo $_GET['city']."/".$cut_url_name."/ad".$adverts_row['id']; ?>" target="_blank"><?php echo $adres; ?>, собственник</a></div>
                        <div class="text">
                          <?php if (strlen($adverts_row['text']) <= 300) {
                            echo $adverts_row['text'];
                          } else{
                            echo substr($adverts_row['text'], 0, strrpos(substr($adverts_row['text'], 0, 300), ' '))." ...";
                          } ?>
                        </div>
                        <div class="min_info">
                          <?php if ($date_type == 1): ?>
                            <div class="date today">Сегодня</div>
                          <?php elseif ($date_type == 2): ?>
                            <div class="date yesterday">Вчера</div>
                          <?php elseif ($date_type == 3): ?>
                            <div class="date"><?php echo $date_withdraw[0]." ".$monthes[$date_withdraw[1]]; ?></div>
                          <?php endif ?>
                          <div class="district"><?php echo $locality; ?></div>
                          <div class="size"><?php echo $adverts_row['size']; ?> кв. м</div>
                        </div>
                      </div>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-3">
                      <?php if ($adverts_row['section_id'] == 1): ?>
                        <?php 
                          if ($adverts_row['srok'] == "long") {
                            $srok = "/месяц";
                          }else{
                            $srok = "/сутки";
                          }
                        ?>
                      <?php endif ?>
                      <div class="price"><?php echo number_format($adverts_row['price'], 0, '.', ' '); ?> руб.<?php echo $srok; ?></div>
                      <?php if ($adverts_row['section_id'] == 2): ?>
                      <div class="kvm_price"><?php echo number_format(round($adverts_row['price']/$adverts_row['size']), 0, '.', ' '); ?> тыс. руб./м²</div>
                      <?php endif ?>
                    </div>
                  </div>
                </div>
              <?php
                  }
                }else{
              ?>
                <div class="alert alert-info" role="alert">К сожаления объявлений нет!</div>
              <?php
                }
              ?>

            </div>
          </div>
        </div>
      </div>
      <div class="text-center">
        <button type="button" id="ads_list-load" class="btn btn-primary btn-lg">Загрузить еще</button>
      </div>
    </div>
<?php
  include 'footer.php';
?>