    <?php if (($_SERVER['REQUEST_URI']=="") or ($_SERVER['REQUEST_URI']=="/") or ($_SERVER['REQUEST_URI']=="/index.php")): ?>
    <?php else: ?>
    <nav class="navbar navbar-inverse">
      <div class="container-fluid">
        <div class="navbar-header">
          <a class="navbar-brand visible-xs" href="/">AuBa</a>
        </div>

        <div class="collapse navbar-collapse" id="navbar-main">
          <ul class="nav navbar-nav">
            <li>
              <a href="/cut_list.php">Аренда</a>
            </li>
            <li>
              <a href="/cut_list.php">Продажа</a>
            </li>
            <li>
              <a href="/news.php">Новости</a>
            </li>
            <li>
              <a href="/articles.php">Статьи</a>
            </li>
            <li class="active">
              <a href="/add_ad.php">Разместить объявление </a>
            </li>
          </ul>

          <p class="navbar-text navbar-right">© <a href="/" class="navbar-link">AuBa Company</a> - 2018</p>
        </div>

      </div>
    </nav>
    <?php endif ?>

    
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/js/bootstrap.min.js"></script>
    
    <!-- AD PAGE -->
    <script src="/slick/slick.js" type="text/javascript" charset="utf-8"></script>
    <script>
        $(".vertical").slick({
            dots: false,
            infinite: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            adaptiveHeight: true
        });
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.js"></script>
    <script type="text/javascript">
      $(".show_hide_block").click(function(){   
        $(".hide_block").toggle();
      });

      $('#city_list').modal({
        show: 'false',
        backdrop: 'static',
        keyboard: false
      });

      $('#editAdvert').modal({
        show: 'false'
      });

      $( "#search_mobile" ).click(function() {
        $( "#header_search" ).toggle();
      });
      $( "#filtr" ).click(function() {
        $( ".filtr" ).toggle();
      });
    </script>
    <!-- END -->
  </body>
</html>